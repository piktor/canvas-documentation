const path = require('path')
const axios = require("axios")

exports.createPages = ({ boundActionCreators, graphql }) => {
  const { createPage } = boundActionCreators

  const multisiteTemplate = path.resolve('src/templates/multiSiteMarkDown/components/rightSideContent/index.js')
  const mdDocs = graphql(`
  {
    allFile(filter: { extension:{ eq : "md" }})
{
  edges {
    node {
      id,
      relativePath
      extension
    }
  }
}
swapi {
  allSpecies {
    name
  }
}
  }
  `).then(res => {
    if (res.errors) {
      return Promise.reject(res.errors)
    }
    console.log("swapi", res.data.swapi.allSpecies);
    
    res.data.allFile.edges.forEach(({ node }) => { 
      if(node.relativePath.includes("canvasDocs")){
        let path = "/" + node.relativePath;
        path = path.replace("/index.md","");
            createPage({
              path: path,
              component: multisiteTemplate
            })
      }  
    })
  })
  return Promise.all([mdDocs]);
}

