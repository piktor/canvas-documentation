const { hot } = require("react-hot-loader/root")

// prefer default export if available
const preferDefault = m => m && m.default || m


exports.components = {
  "component---src-templates-multi-site-mark-down-components-right-side-content-index-js": hot(preferDefault(require("/Users/nixon/projects/piktor-repo/canvas-documentation/canvas-documentation/src/templates/multiSiteMarkDown/components/rightSideContent/index.js"))),
  "component---cache-dev-404-page-js": hot(preferDefault(require("/Users/nixon/projects/piktor-repo/canvas-documentation/canvas-documentation/.cache/dev-404-page.js"))),
  "component---src-pages-404-js": hot(preferDefault(require("/Users/nixon/projects/piktor-repo/canvas-documentation/canvas-documentation/src/pages/404.js"))),
  "component---src-pages-index-js": hot(preferDefault(require("/Users/nixon/projects/piktor-repo/canvas-documentation/canvas-documentation/src/pages/index.js")))
}

