module.exports = {
  siteMetadata: {
    title: 'Dev Center',
  },
  plugins: [
    'gatsby-plugin-react-helmet',
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: 'Dev Center - API First',
        short_name: 'Dev Center',
        start_url: '/',
        background_color: '#663399',
        theme_color: '#663399',
        display: 'minimal-ui',
        icon: 'src/images/favicon.png', // This path is relative to the root of the site.
      },
    },
    {
      resolve: `gatsby-transformer-json`,
      options: {
        typeName: `Journey`
      },
    },
    `gatsby-transformer-json`,
    // {
    //   resolve: `gatsby-source-filesystem`,
    //   options: {
    //     name: 'devJourney',
    //     path: `./src/pages/canvasDocs/securitydocs`,
    //   }
    // },
    'gatsby-plugin-catch-links',
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        path: `${__dirname}/src/pages`,
        name: 'pages'
      }
    },
    'gatsby-plugin-offline',
    'gatsby-plugin-sass',
    'gatsby-plugin-slug',
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-transformer-remark`,
      options: {
        plugins: [
          {
            resolve: `gatsby-remark-images`,
            options: {
              // It's important to specify the maxWidth (in pixels) of
              // the content container as this plugin uses this as the
              // base for generating different widths of each image.
              maxWidth: 590,
            },
          },
          {
            resolve: `gatsby-remark-prismjs`,
            options: {
              classPrefix: "language-",
              inlineCodeMarker: null,
              aliases: {},
              showLineNumbers: false,
              noInlineHighlight: false,
            },
          },
          {
            resolve: "gatsby-remark-custom-blocks",
            options: {
              blocks: {
                notes: {
                  classes: "notes",
                  title: "optional",
                },
                iLink: {
                  classes: "iLink",
                  title: "optional",
                },
                external: {
                  classes: "external",
                  title: "optional",
                },
                mdImage: {
                  classes: "mdImage",
                  title: "optional",
                },
              },
            },
          },
          {
            resolve: "gatsby-source-graphql",
            options: {
              // This type will contain remote schema Query type
              typeName: "SWAPI",
              // This is field under which it's accessible
              fieldName: "swapi",
              // Url to query from
              url: "https://api.graphcms.com/simple/v1/swapi",
            },
          }
        ],
      },
    }
  ],
}
