import React, { Component } from "react";
import ExpandHeader from "../../components/header/expandHeader";
import Header from "../../components/header/SimpleHeader";
import Layout from "../../components/layout";
import Footer from "../../components/footer";
import DevJourneyNavBar from '../../components/devJourneyComponent/components/devJourneySteps/components/devJourneyNavBar';
import collapse_icon from '../../images/icon_collapse.svg';
import expand_icon from '../../images/icon_expand.svg';
import "./index.css";

export default class markDown extends Component {
  state = {
    toggleNav: false
  };

  toggleNavVal = () => {
    let self = this;
    self.setState({
      toggleNav: !this.state.toggleNav
    });
  };
  // componentDidMount() {
  //   let inIframe = !!window.frameElement;
  //   this.setState({ inIframe });
  //   console.log("__________AM I IN AN IFRAME_______?????" + inIframe);
  // }


  toogleClickEvent = () => {
    if (document.getElementById('devJourneyNavb')) {
      document.getElementById('devJourneyNavb').classList.toggle('collapseJourneyEvent');
      // document.getElementById('collapse-btn').classList.toggle('display-none-devBtn');
      document.getElementById('devJourneyNavb') && document.getElementById('devJourneyNavb').classList.remove('.collapseJourneyEvent'); 
    }
    if (document.getElementsByClassName('devJourneyNav').length > 0){
      document.getElementsByClassName('devJourneyNav')[0].classList.toggle('nav-expanded');
    }
      document.getElementById('devJourneyNavbarr') && document.getElementById('devJourneyNavbarr').classList.toggle('collapseJourney');
      // document.getElementById('devJourneyNavb') && document.getElementById('devJourneyNavb').classList.toggle('collapseJourney');
      document.getElementById('devJourneyRight-div') && document.getElementById('devJourneyRight-div').classList.toggle('devJourneyRi');

    if (window.location.pathname.indexOf(("/sites/") > -1)) {
      document.getElementById('collapse-btn').classList.toggle('rotate-btn');
    }
    this.setState({
      expand: !this.state.expand
    })
    // document.getElementById('collapse-btn').classList.toggle('display-none-devBtn');
    // document.getElementById('devJourneyNavb').classList.remove('.collapseJourneyEvent');
    // document.getElementById('devJourneyNavb').classList.toggle('collapseJourneyEvent');
    // document.getElementById('devJourney-tour') && document.getElementById('devJourney-tour').classList.toggle('devJourney-tour-posin')
  }

  render() {
    //console.log("window:");
    //console.log(!window? undefined : window);
    //console.log('localStorage:')
    //console.log(window? window.localStorage.getItem('JOURNEY_VALUE') : window);
   
    return (
      <Layout>
        {!this.props.inIframe && <Header />}
        <section className={!this.props.inIframe ? "blogPage-bg multisite-markdown" : "multisite-markdown" }>
        {(!this.props.inIframe) && (
          <aside className="devJourneyNav">
            <div className="devJourneyNavbarr collapseJourney" id="devJourneyNavbarr" style={(typeof window !== 'undefined' && window.localStorage.getItem('JOURNEY_VALUE')) ? { display: "block" } : { display: "none" }}>
              <DevJourneyNavBar />
            </div>
        </aside>
        )}
        {(!this.props.inIframe) && (
          <div className="collapse-btn-event toolsdevJourney" title="Dev Journey" id="collapse-btn-event" onClick={this.toogleClickEvent} style={(typeof window !== 'undefined' && window.localStorage.getItem('JOURNEY_VALUE')) ? { display: "block" } : { display: "none" }}>
            <img src={expand_icon} alt="expand-icon" />
          </div>
          )}
          {(!this.props.inIframe) && (
          <aside className="devJourneyNavEvent devJourneyNavEvent-lr" id="devJourneyNavb" style={(typeof window !== 'undefined' && window.localStorage.getItem('JOURNEY_VALUE')) ? { display: "block" } : { display: "none" }}>
            <DevJourneyNavBar />
          </aside>
          )}
          <div className="pg-title">Canvas Documentation</div>
          <div
            className="max-width-blogpage"
            style={this.props.inIframe ? { margin: "0 auto" } : {}}
          >
            <div
              className={
                (this.state.toggleNav
                  ? "mobileNavOpen mobileHeight"
                  : "mobileNavClosed") + " security-markdown mobileNav"
              }
            >
              {this.props.children}
            </div>
          </div>
        </section>
        {!this.props.inIframe && <Footer />}
      </Layout>
    );
  }
}
