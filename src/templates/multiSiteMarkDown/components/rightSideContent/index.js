import React, { Component } from "react";
// import PropTypes from "prop-types";
// import { connect } from "react-redux";
import { graphql } from "gatsby";
import Link from "gatsby-link";
import _ from "lodash";
import star from "../../../../images/icon-favorites.svg";
import starHover from "../../../../images/icon-favorites-hover.svg";
import navList from "../../../../json/navBarSearch.json";
import SEO from "../../../../components/seo";
import MarkDown from "../../index.js";
import store from "../../../../state/createStore";
import LeftSideNavbar from "../leftSideNavbar";

import "./index.css";

export default class RightSideContent extends Component {
  constructor(props) {
    super(props);
    // this.starMouseEnter = this.starMouseEnter.bind(this);
    // this.starMouseLeave = this.starMouseLeave.bind(this);
    // this.starClicked = this.starClicked.bind(this);
    this.state = {
      navList: navList,
      starActive: false,
      rightSideMarkdown: {}
    };
  }
  componentWillMount() {
    let path = this.props.location.pathname.slice(1);
    let temp = path;
    let lastChar = temp.slice(-1);
    if(lastChar !== "/") {
      let rightSideMarkdown = _.find(
        this.props.data.allMarkdownRemark.edges,
        function(item) {
          return item.node.fields.slug === "/" + path + "/";
        }
      );
      this.setState({
        rightSideMarkdown
      });
    }
    else if(lastChar === "/") {
      let rightSideMarkdown = _.find(
        this.props.data.allMarkdownRemark.edges,
        function(item) {
          return item.node.fields.slug === "/" + path;
        }
      );
      this.setState({
        rightSideMarkdown
      });
    }
  }
  componentDidMount() {
    store.dispatch({
      type: `PATH`,
      data: this.state.rightSideMarkdown.node.fields.slug
    });
    let inIframe = !!window.frameElement;
    this.setState({ inIframe });    
    var mdImgClass = document.querySelectorAll(".mdImage a");
    for (var i = 0; i < mdImgClass.length; i++) {
      mdImgClass[i].removeEventListener("click", this.mdImgClick);
      mdImgClass[i].addEventListener("click", this.mdImgClick);
    }
    if(document.querySelector('.mdImage') !== null ) {
      var mdimg = document.querySelectorAll('.mdImage');
      for (var i=0; i<mdimg.length; i++){
        let a = mdimg[i].getElementsByTagName('a');
        a[0].setAttribute('target', '_self');
        a[0].setAttribute('href', '#');
      }
    }
  }

  componentDidUpdate() {
    var mdImgClass = document.querySelectorAll(".mdImage a");
    for (var i = 0; i < mdImgClass.length; i++) {
      mdImgClass[i].removeEventListener("click", this.mdImgClick);
      mdImgClass[i].addEventListener("click", this.mdImgClick);
    }
    if(document.querySelector('.mdImage') !== null ) {
      var mdimg = document.querySelectorAll('.mdImage');
      for (var i=0; i<mdimg.length; i++){
        let a = mdimg[i].getElementsByTagName('a');
        a[0].setAttribute('target', '_self');
        a[0].setAttribute('href', '#');
      }
    }
    if(document.querySelector('.mdImage-overlay') !== null ) {
      var mdImgClass = document.querySelectorAll(".mdImage .gatsby-resp-image-link");
      for (var i = 0; i < mdImgClass.length; i++) {
        mdImgClass[i].removeEventListener("click", this.zoomOut);
        mdImgClass[i].addEventListener("click", this.zoomOut);
      }
    }
  }

  componentWillUnmount() {
    var mdImgClass = document.getElementsByClassName("mdImage");
    for (var i = 0; i < mdImgClass.length; i++) {
      mdImgClass[i].removeEventListener("click", e => this.mdImgClick(e));
    }
    if(document.querySelector('.mdImage-overlay') !== null ) {
      var mdImgClass = document.querySelectorAll(".mdImage .gatsby-resp-image-link");
      for (var i = 0; i < mdImgClass.length; i++) {
        mdImgClass[i].removeEventListener("click", e => this.zoomOut(e));
      }
    }
  }

  mdImgClick = event => {
    var imgLink = event.currentTarget;
    imgLink.classList.add("mdImage-overlay");
    var imgWrapper = imgLink.querySelectorAll('.mdImage .gatsby-resp-image-wrapper');
    imgWrapper[0].classList.add("mdImage-zoom");
  };

  zoomOut = event => {
    var imgLink = document.querySelectorAll('.mdImage .gatsby-resp-image-link');
    for (var i = 0; i < imgLink.length; i++) {
      imgLink[i].classList.remove("mdImage-overlay");
    }
    var imgWrapper = document.querySelectorAll('.mdImage .gatsby-resp-image-wrapper');
    for (var i = 0; i < imgWrapper.length; i++) {
      imgWrapper[i].classList.remove("mdImage-zoom");
    }
  };

  // starMouseLeave() {
  //   if (!this.state.starActive) {
  //     let element = this.refs.star;
  //     element.src = star;
  //   }
  // }
  // starMouseEnter() {
  //   if (!this.state.starActive) {
  //     let element = this.refs.star;
  //     element.src = starHover;
  //   }
  // }
  // starClicked() {
  //   let self = this;
  //   let element = this.refs.star;
  //   if (this.state.starActive) {
  //     element.src = star;
  //   } else {
  //     element.src = starFilled;
  //   }

  //   self.setState({ starActive: !self.state.starActive });
  // }

  renderLinkName(list, index, flag, navItems) {
    let path = "";
    for (let i = 0; i <= index; i++) {
      path = path + (list[i] + "/");
    }
    if (navItems) {
      var returnData = _.find(navItems, function(item) {
        if (
          item.node.fields.slug.replace("_index/", "") ===
          "/sites/linked/" + path
        ) {
          return item;
        }
      });
      if (returnData) {
        if (flag === "name") {
          return returnData.node.frontmatter.menuTitle;
        } else {
          return returnData.node.fields.slug;
        }
      }
    }
  }

  renderBreadcrum(fields, navItems) {
    let splitSlug = fields.slug.split("/");
    splitSlug.shift();
    splitSlug.shift();
    splitSlug.shift();
    splitSlug.pop();
    return (
      <ol className="breadcrumbb">
        <div className="breadcrumb-flex">
          {splitSlug.map(
            (item, index) =>
              index > 0 && (
                <li className="breadcrumb-li breadcrumb-item" key={index}>
                  <Link
                    className="breadcrumb-li text-style-1"
                    to={this.renderLinkName(splitSlug, index, "link", navItems)}
                  >
                    {this.renderLinkName(splitSlug, index, "name", navItems)}
                  </Link>
                </li>
              )
          )}
        </div>
        {!this.state.inIframe && (
          <li
            className="breadcrumb-img-cont"
            //onMouseEnter={() => this.starMouseEnter()}
            //onMouseLeave={() => this.starMouseLeave()}
            // onClick={() => this.starClicked()}
          >
            {/* <img className="breadcrumb-img" src={star} ref="star" alt="star" /> */}
          </li>
        )}
      </ol>
    );
  }

  getNavItems() {
    if(this.state.rightSideMarkdown !== undefined) {
    const directoryName = this.state.rightSideMarkdown.node.fields.slug.split(
      "/"
    )[2];
    const items = this.props.data.allMarkdownRemark.edges.filter(item => {
      if (
        directoryName === item.node.fields.slug.split("/")[2] &&
        item.node.frontmatter.draft === true
      ) {
        if (!item.node.children) {
          return item;
        } else {
          item.node.children = [];
          return item;
        }
      }
      return null;
    });

    const comparingItems = items;

    const itemsWithChildren = items.map(item => {
      comparingItems.forEach(subItem => {
        let check = subItem.node.fields.slug.split("/");
        if (check.length > 6) {
          check.pop();
          check.pop();
          let slugCheck = check.join("/");
          let slugValue = slugCheck.concat("/");
          if (slugValue === item.node.fields.slug) {
            if (!item.node.children) {
              item.node.children = [];
            }
            item.node.children.push(subItem);
          }
        }
      });
      return item;
    });

    const parentItems = itemsWithChildren.filter(item => {
      let t = item.node.fields.slug.split("/");
      if (t.length === 6) {
        return item;
      }
      return null;
    });
    
    return {
      items,
      parentItems
    };
  }}

  render() {
    const allMarkdownRemark = this.getNavItems();
    return (
      <MarkDown inIframe={this.state.inIframe}>
        <LeftSideNavbar
          inIframe={this.state.inIframe}
          allMarkdownRemark={allMarkdownRemark.items}
          navList={allMarkdownRemark.items}
        />
        <main className="security-markdown-right-side">
          <SEO
            title={this.state.rightSideMarkdown.node.frontmatter.title}
            pathname={this.state.rightSideMarkdown.node.fields.slug}
          />
          {this.renderBreadcrum(
            this.state.rightSideMarkdown.node.fields,
            allMarkdownRemark.items
          )}
          <div
            className="markdown-body paddingRight-45"
            dangerouslySetInnerHTML={{
              __html: this.state.rightSideMarkdown.node.html
            }}
          />
        </main>
      </MarkDown>
    );
  }
}

export const postQuery = graphql`
  query BlogPostByFilePath {
    allMarkdownRemark(
      filter: { fields: { slug: { regex: "/^/canvasDoc/" } } }
      sort: { fields: [frontmatter___order], order: ASC }
    ) {
      edges {
        node {
          id
          html
          fields {
            slug
          }
          frontmatter {
            title
            menuTitle
            draft
            tags
          }
        }
      }
    }
  }
`;
