import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import Link from "gatsby-link";
import search from "../../../../images/icon-search@2x.png";
import searchClear from "../../../../images/icon-clear.svg";
import "./index.css";
import arrowDown from "../../../../images/arrow-down-not-coloured.svg";
import arrowDownColor from "../../../../images/arrow-down-coloured.svg";
import dot from "../../../../images/bulletin-dots.svg";
import dotColor from "../../../../images/bulletin-dots-coloured.svg";
import collapse_icon from "../../../../images/icon_collapse.svg";
import expand_icon from "../../../../images/icon_expand.svg";
import DevJourneyLeft from "../../../../components/devJourneyComponent/components/devJourneySteps/components/devJourneyNavBar";

class LeftSideNavbar extends Component {
  constructor() {
    super();
    this.handleToggle = this.handleToggle.bind(this);
    this.searchList = this.searchList.bind(this);
    this.clickedOnSearch = this.clickedOnSearch.bind(this);
    this.onBlurSearch = this.onBlurSearch.bind(this);
    this.clearSearchBar = this.clearSearchBar.bind(this);
    this.mouseHover = this.mouseHover.bind(this);
    this.mouseLeave = this.mouseLeave.bind(this);

    this.mouseOver = this.mouseOver.bind(this);
    this.mouseOut = this.mouseOut.bind(this);

    this.state = {
      inSearchMode: false,
      searchText: "",
      searchBtnClicked: false,
      onCancelBtn: false,
      pathname: "",
      fitered: []
    };
  }

  componentDidMount() {
    this.reCreateNavImage();
    if (this.state.searchText.length > 0) {
      this.setState({ inSearchMode: true });
    }
  }

  mouseOver() {
    document.querySelector(".cross-icon").classList.remove("search-iconn-out");
    document.querySelector(".cross-icon").classList.add("search-iconn-hover");
  }

  mouseOut() {
    document
      .querySelector(".cross-icon")
      .classList.remove("search-iconn-hover");
    document.querySelector(".cross-icon").classList.add("search-iconn-out");
  }

  mouseHover(reference, childLength, level) {
    let element = this.refs[reference];
    if (childLength === "notValid") {
      element.src = dotColor;
    } else {
      if (level === 4 || level === 3) {
        element.src = dotColor;
      } else {
        element.src = arrowDownColor;
      }
    }
  }

  mouseLeave(reference, childLength, level, active) {
    let element = this.refs[reference];
    if (childLength === "notValid") {
      if (!active) element.src = dot;
    } else {
      if (level === 4 || level === 3) {
        if (!active) element.src = dot;
      } else {
        if (!active) element.src = arrowDown;
      }
    }
  }

  reCreateNavImage() {
    if (this.props.storedActiveNavImage.length > 0) {
      this.props.storedActiveNavImage.forEach(element => {
        document
          .getElementsByClassName(element)[0]
          .classList.add("arrow-down-image");
        document
          .getElementsByClassName(element)[1]
          .classList.add("opened-toggle");
        document.getElementById(element).children[0].classList.remove("closed");
        document.getElementById(element).children[0].classList.add("opend");
      });
    }
  }

  searchList(data) {
    if (data.length > 0) {
      this.setState({ searchText: data, inSearchMode: true });
      // localStorage.setItem("searchText", data);
    } else {
      this.setState({ searchText: data, inSearchMode: false });
      // localStorage.setItem("searchText", data);
    }
  }
  clickedOnSearch() {
    this.setState({ searchBtnClicked: true });
  }

  onBlurSearch() {
    this.setState({ searchBtnClicked: false });
  }
  clearSearchBar() {
    this.setState({
      searchText: "",
      inSearchMode: false,
      searchBtnClicked: true
    });
    // localStorage.setItem("searchText", "");
  }

  handleToggle(toggleId) {
    let self = this;
    var getId = document.getElementsByClassName(toggleId)[0].className;
    let isCollepse = getId.includes("arrow-down-image");
    if (isCollepse) {
      document
        .getElementsByClassName(toggleId)[0]
        .classList.remove("arrow-down-image");
      document
        .getElementsByClassName(toggleId)[1]
        .classList.remove("opened-toggle");
      document.getElementById(toggleId).children[0].classList.add("closed");
      document.getElementById(toggleId).children[0].classList.remove("opend");

      //TEMP : remove from stored image --- Shift this code to redux later
      let storedActiveNavImage = self.props.storedActiveNavImage;
      let findIndex = storedActiveNavImage.indexOf(toggleId);
      storedActiveNavImage.splice(findIndex, 1);
      self.props.changeImage(storedActiveNavImage);
    } else {
      document
        .getElementsByClassName(toggleId)[0]
        .classList.add("arrow-down-image");
      document
        .getElementsByClassName(toggleId)[1]
        .classList.add("opened-toggle");
      document.getElementById(toggleId).children[0].classList.remove("closed");
      document.getElementById(toggleId).children[0].classList.add("opend");

      //TEMP : add to stored image --- Shift this code to redux later
      let storedActiveNavImage = self.props.storedActiveNavImage;
      storedActiveNavImage.push(toggleId);
      self.setState({ storedActiveNavImage });
      self.props.changeImage(self.props.storedActiveNavImage);
      localStorage.setItem(
        "storedImageNav",
        JSON.stringify(storedActiveNavImage)
      );
    }
  }

  renderListHorizontal(item) {
    const childText = item.node.children
      ? item.node.children.length
      : "notValid";
    const isActive = this.props.rootPath === item.node.fields.slug;

    const showArrow = () => (isActive ? arrowDownColor : arrowDown);
    const showDot = () => (isActive ? dotColor : dot);

    const src = item.node.children ? showArrow() : showDot();
    const path = item.node.fields.slug.split("/");
    const className = item.node.children
      ? `${path} arrow-right-image`
      : "dot-image";

    const onClick = () => {
      if (item.node.children) {
        this.handleToggle(path);
      }
    };

    return (
      <div
        className="list-horizontal"
        onMouseEnter={() => {
          this.mouseHover(item.node.fields.slug, childText, 1);
        }}
        onMouseLeave={() => {
          const isActive = this.props.rootPath === item.node.fields.slug;
          this.mouseLeave(item.node.fields.slug, childText, 1, isActive);
        }}
        onClick={onClick}
      >
        <div className="arrow-dot">
          <img
            src={src}
            alt=""
            ref={item.node.fields.slug}
            className={className}
          />
        </div>
        <Link
          to={item.node.fields.slug.slice(0, -1)}
          activeStyle={{ color: "#e20074" }}
          className={path}
        >
          {item.node.frontmatter.menuTitle}
        </Link>
      </div>
    );
  }

  renderTreeChildren(edges, ulClass = "closed") {
    console.log("NAAAAV=====>", edges)
    return (
      <ul className={ulClass}>
        {edges.map((item, index) => {
          const path = item.node.fields.slug.split("/");

          return (
            <li key={index} className="parent">
              {this.renderListHorizontal(item)}
              {item.node.children && item.node.children.length && (
                <div id={path}>
                  {this.renderTreeChildren(item.node.children)}
                </div>
              )}
            </li>
          );
        })}
      </ul>
    );
  }

  renderTreeList() {
    return (
      <div className="ul-classes components">
        {this.renderTreeChildren(this.props.allMarkdownRemark, "ul-border")}
      </div>
    );
  }

  renderSearchList() {
    const word = this.state.searchText.toLowerCase();
    const fitered = this.props.navList.filter(item => {
      const result = item.node.frontmatter.menuTitle
        .toLowerCase()
        .includes(word);
      if (result) {
        return item;
      }
      return null;
    });

    return (
      <div className="searchList">
        <div className="ul-classes components">
          <ul className="ul-border">
            {fitered.map((item, index) => (
              <li className="parent" key={index}>
                <div className="list-horizontal">
                  <Link
                    className="searchLink"
                    to={item.node.fields.slug.slice(0, -1)}
                    activeStyle={{ color: "#e20074" }}
                  >
                    {item.node.frontmatter.menuTitle}
                  </Link>
                </div>
              </li>
            ))}
            {fitered.length < 1 && this.state.inSearchMode && (
              <li className="not-found">
                <a href="#/">No Results Found!</a>
              </li>
            )}
          </ul>
        </div>
      </div>
    );
  }

  renderNav() {
    return (
      <div className="sidebar-nav">
        <nav id="sidebar">
          {/* {!this.props.inIframe && (
            <div
              className="collapse-btn-event"
              title="Dev Journey"
              id="collapse-btn-event"
              onClick={this.toogleClickEvent}
              style={
                typeof window !== "undefined" &&
                window.localStorage.getItem("JOURNEY_VALUE")
                  ? { display: "block" }
                  : { display: "none" }
              }
            >
              <img
                src={this.state.expand === true ? collapse_icon : expand_icon}
                alt="expand-icon"
              />
            </div>
          )} */}
          <div
            className={
              "sidebar-search" +
              (this.state.inSearchMode || this.state.searchBtnClicked
                ? " bg-white"
                : "")
            }
          >
            <input
              type="text"
              className="search-textt header-search"
              placeholder="Search"
              value={this.state.searchText}
              onBlur={() => this.onBlurSearch()}
              onFocus={() => this.clickedOnSearch()}
              onChange={event => this.searchList(event.target.value)}
            />
            {!this.state.inSearchMode && (
              <img alt="search" src={search} className="search-iconn" />
            )}
            {this.state.inSearchMode && (
              <img
                alt="search"
                src={searchClear}
                onClick={() => this.clearSearchBar()}
                onMouseEnter={() => this.mouseOver()}
                onMouseLeave={() => this.mouseOut()}
                className="search-iconn cross-icon cursor-pointer"
              />
            )}
          </div>
          <div>
            <div
              className={
                !this.state.inSearchMode ? "displayList" : "notDisplayList"
              }
            >
              {this.renderTreeList()}
            </div>
            <div
              className={
                this.state.inSearchMode ? "displayList" : "notDisplayList"
              }
            >
              {this.renderSearchList()}
            </div>
          </div>
        </nav>
      </div>
    );
  }

  toogleClickEvent = () => {
    document
      .getElementById("devJourneyNavb")
      .classList.toggle("collapseJourneyEvent");
    // document
    //   .getElementById("collapse-btn")
    //   .classList.toggle("display-none-devBtn");
    document
      .getElementById("devJourneyNavb")
      .classList.remove(".collapseJourneyEvent");
      if (document.getElementsByClassName('devJourneyNav').length > 0){
        document.getElementsByClassName('devJourneyNav')[0].classList.toggle('nav-expanded');
      }
    document
      .getElementById("collapse-btn-event")
      .classList.toggle("btn-positn-api");
    this.setState({
      expand: !this.state.expand
    });
  };

  render() {
    return (
      <div className="toolsNDoc">
        <aside className={this.props.inIframe? "security-markdown-leftnav frame":"security-markdown-leftnav"}>{this.renderNav()}</aside>
        {/* <aside id="devJourneyNavb" className="devJourneyNavEvent">
          <DevJourneyLeft />
        </aside> */}
      </div>
    );
  }
}

LeftSideNavbar.propTypes = {
  storedActiveNavImage: PropTypes.array.isRequired,
  changeImage: PropTypes.func.isRequired,
  rootPath: PropTypes.string.isRequired
};

const mapStateToProps = ({ storedActiveNavImage, rootPath }) => {
  return { storedActiveNavImage, rootPath };
};

const mapDispatchToProps = dispatch => {
  return {
    changeImage: dataValue =>
      dispatch({
        type: `NAVBARIMAGE`,
        data: dataValue
      })
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LeftSideNavbar);
