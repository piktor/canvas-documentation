import React, { Component } from 'react'
import NavBlock from './components/navBlock';
import Search from './components/search';
import "./index.css";

export default class ToolsAndDocsRight extends Component {

  render() {
    return (
      <section className="toolsAndDocsRight">
        <article className="search-tag max-width">
          <Search sectionName="design" />
        </article>
        <article className="card-portion max-width">
          <div className="card-group">
            <NavBlock tools={this.props.toolsData} docs={this.props.documentsData}/>
          </div>
        </article>
      </section>
    )
  }
}
