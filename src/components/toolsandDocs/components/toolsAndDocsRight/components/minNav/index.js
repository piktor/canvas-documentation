import React from 'react';
import { Link } from 'gatsby';
import filter from '../../../../../../images/icon-filter@3x.png';
import './index.css';

const MinMinNav = () => (
  <div>
    <div className="min-nav">
      <div className="type">
        <div>
          <Link to="/" className="min-nav-li left-li active">
            Popular
          </Link>
        </div>
        <div title="coming soon">
          <Link title="coming soon" to="/" className="min-nav-li left-li">
            Recent
          </Link>
        </div>
        <div title="coming soon">
          <Link title="coming soon" to="#/" className="min-nav-li left-li">
            All
          </Link>
        </div>
      </div>

      <div className="filterSort">
        <div title="coming soon" className="filter-li right-li">
          <Link title="coming soon" to="/" className="min-nav-li">
            <img className="filter" src={filter} alt="filter" />
            Filter
          </Link>
        </div>
        {/* <div title="coming soon" className="sort-li right-li">
          <Link href="#/" title="coming soon" to="/" onClick={e => e.preventDefault()} className="min-nav-li">
            Sort
            <img className="sort" src={sort} alt="sort" />
          </Link>
        </div> */}
      </div>
    </div>
  </div>
);

export default MinMinNav;
