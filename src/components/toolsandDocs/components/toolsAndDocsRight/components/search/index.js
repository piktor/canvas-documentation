import React, { Component } from 'react';
import './index.css';

export default class Search extends Component {
  constructor() {
    super();
    this.state = {
      sectionName:"design",
      sections: {
        design: 'Design',
        develop: 'Development',
        testDeploy: 'Testing and Deployment',
        manage: 'Manage'
      }
    };
  }

  componentDidMount(){
    let sectionName = window.location.search.split("?section=")[1];
    if(sectionName === "design" || sectionName === "develop" || sectionName === "testDeploy" || sectionName === "manage"){
      this.setState({
        sectionName
      })
    }
  }
  componentDidUpdate(prevProps){
    let sectionName = window.location.search.split("?section=")[1];
    if(sectionName !== this.state.sectionName){
      if(sectionName === "design" || sectionName === "develop" || sectionName === "testDeploy" || sectionName === "manage"){
        this.setState({
          sectionName
        })
      }
    }
  }

  render() {
    return (
      <div className="search-Nav">
        <div className="search-design">{this.state.sections[this.state.sectionName]}</div>
      </div>
    );
  }
}
