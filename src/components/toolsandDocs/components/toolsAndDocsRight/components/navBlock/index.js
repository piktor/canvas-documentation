import React, { Component } from "react";
import Cards from "../cards";
import MinNav from "../minNav";
import Document from "../document";
import "./index.css";

export default class MainNav extends Component {
  constructor() {
    super();
    this.state = {
      tabName: "documents"
    };
    this.changeTab = this.changeTab.bind(this);
  }

  changeTab(name) {
    this.setState({ tabName: name });
  }

  render() {
    const toolsData = this.props.tools;
    const docsData = this.props.docs;
    return (
      <div>
        <div>
          <div className="nav-block">
            <div
              className={
                "navigation-block " +
                (this.state.tabName === "documents" ? "active" : "inactive")
              }
              onClick={() => this.changeTab("documents")}
            >
              <div className="navigation-block-text">
                <span className="nav-text">DOCUMENTS</span>
                <div className="nav-bar-count flex flex-center flex-align-center">
                  <span className="nav-count-text">{docsData.length}</span>
                </div>
              </div>
            </div>
            <div
              className={
                "navigation-block navigation-block-documents disable-cursor " +
                (this.state.tabName === "tools" ? "active" : "inactive")
              }
              onClick={() => this.changeTab("tools")}
            >
              <div className="navigation-block-text">
                <span className="nav-text">TOOLS</span>
                <div className="nav-bar-count flex flex-center flex-align-center">
                  <span className="nav-count-text">{toolsData.length}</span>
                </div>
              </div>
              <div className='nav-bg'></div>
            </div>
            <div className="space-navbar" />
          </div>
        </div>
        <MinNav />
        {this.state.tabName === "tools" && <Cards list={toolsData} />}
        {this.state.tabName === "documents" && <Document list={docsData} />}
        {/* {this.state.tabName === 'documents' && <Documents />}  */}
      </div>
    );
  }
}
