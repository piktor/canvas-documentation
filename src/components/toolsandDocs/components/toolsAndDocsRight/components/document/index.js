import React, { Component } from "react";
import "./index.css";
import { navigate } from "gatsby";
import icon from "../../../../../../images/icon-templates.svg";
import arrow from "../../../../../../images/arrow-documents.svg";

export default class Templates extends Component {
  
  disableNav(e){   // function disable nav
    e.preventDefault();
    return false
 
 }
  navigateTo(isExternal, target){
    if(!isExternal){
      navigate(target);
    }else{
      window.open(target, '_blank');
    }
  }
 
  render() {
    return (
      <div className="template-div">
        {this.props.list.length > 0 &&
          this.props.list.map((item, index) => (
            <a href="#/" onClick={() => this.navigateTo(item.node.frontmatter.external, item.node.frontmatter.link)}>
              <div className="tempdiv" key={index}>
                <div className="icondiv">
                  <img src={icon} alt="template-icon" />
                </div>
                <div className="textdiv">
                  <span>
                    <h5 className="temp-title">{item.node.frontmatter.title}</h5>
                  </span>
                  <h6
                    className="temp-text"
                    dangerouslySetInnerHTML={{ __html: item.node.html }}
                  />
                </div>
                <span className="arrowdiv">
                  <img src={arrow} alt="arrow" />
                </span>
              </div>
            </a>
          ))}

            {this.props.list.length === 0 &&(
              <div className="result-not-found">No Results Found !!!</div>
            )}
      </div>
    );
  }
}
