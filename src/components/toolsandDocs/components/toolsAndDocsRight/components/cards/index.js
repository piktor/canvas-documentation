import React, { Component } from "react";
import black from "../../../../../../images/6B3552.svg";
import brown from "../../../../../../images/AB4657.svg";
import orange from "../../../../../../images/FA814D.svg";
import grey from "../../../../../../images/767F96.svg";
import light_brown from "../../../../../../images/C6A79D.svg";
import white from "../../../../../../images/ffffff.svg";
import extIcon from "../../../../../../images/icon-external-tools.svg";
import ModalFrame from "../../../../../../components/modalFrame";
import "./index.css";

export default class Cards extends Component {
  constructor() {
    super();
    this.state = {
      colors: [black, brown, orange, grey, light_brown],
      modalIsOpen: false,
      modalTitle: "",
      modalLink: "",
      modalImage: ""
    };
    this.modalClick = this.modalClick.bind(this);
    this.handleModalState = this.handleModalState.bind(this);
  }

  modalClick(frontmatter) {
    this.setState(prevState => {
      return {
        modalIsOpen: true,
        modalTitle: frontmatter.title,
        modalLink: frontmatter.link,
        modalImage: frontmatter.image
      };
    });
    //this.setState({modalIsOpen: true});
  }

  handleModalState(isModalOpen) {
    this.setState(prevState => {
      return { modalIsOpen: isModalOpen };
    });
  }

  /* eslint-disable global-require */
  /* eslint-disable import/no-dynamic-require */
  render() {
    return (
      <div>
        <ModalFrame
          showModal={this.state.modalIsOpen}
          name={this.state.modalTitle}
          modalUrl={this.state.modalLink}
          imagePath="tools/"
          image={this.state.modalImage}
          changeModalState={this.handleModalState}
        />
        <div className="card-row">
          <div className="card-wrapper">
            {this.props.list.length > 0 &&
              this.props.list.map((item, index) => (
                <span key={index}>
                  {!item.node.frontmatter.external && !item.node.frontmatter.launchModal && (
                    <div className="card-link" key={index}>
                    <div className="card-container">
                      <div className="card-card">
                        <div className="card-img-holder">
                          <div
                            style={{
                              backgroundImage: item.node.frontmatter.image
                                ? "url(" + white + ")"
                                : "url(" +
                                  this.state.colors[
                                    Math.floor(
                                      Math.random() * this.state.colors.length
                                    )
                                  ] +
                                  ")"
                            }}
                            className={((item.node.frontmatter.image)&&(item.node.frontmatter.image.toLowerCase().includes("hexagon-")))?"card-no-back":"card-back"}
                          >
                            <div>
                              {item.node.frontmatter.image && (
                                <div className={(item.node.frontmatter.image.toLowerCase().includes("hexagon-"))?"hex-card-img":"card-img"}>
                                  <img
                                  src={require("../../../../../../pages/tools/" +
                                    item.node.frontmatter.image)}
                                  alt="Star"
                                  />
                                </div>
                                
                              )}
                              {!item.node.frontmatter.image && (
                                <p className="customLogo">
                                  {item.node.frontmatter.title[0]}
                                </p>
                              )}
                            </div>
                          </div>
                          {item.node.frontmatter.external &&
                            (!item.node.frontmatter.launchModal) &&
                          (
                            <a
                              href={item.node.frontmatter.link}
                              target="_blank"
                              className="card-external-link"
                              rel="noopener noreferrer"
                            >
                              <img src={extIcon} alt="ext icon" />
                            </a>
                          )}
  
                            {item.node.frontmatter.external &&
                            (item.node.frontmatter.launchModal) &&
                            (
                                <a
                                    onClick={() =>
                                        this.modalClick(item.node.frontmatter)
                                    }
                                    href="#/"
                                    target="_blank"
                                    className="card-external-link"
                                    rel="noopener noreferrer"
                                >
                                    <img src={extIcon} alt="ext icon" />
                                </a>
                            )}
  
  
                        </div>
                        <div className="card-body">
                          <h5 className="card-title">
                            {item.node.frontmatter.title}
                          </h5>
                          <p
                            className="card-text"
                            dangerouslySetInnerHTML={{ __html: item.node.html }}
                          />
                          {!item.node.frontmatter.external &&
                            !item.node.frontmatter.launchModal && (
                              <a
                                href={item.node.frontmatter.link}
                                className="card-see-more"
                              >
                                <span className="seemore">Go To Link</span>
                              </a>
                            )}
                          {item.node.frontmatter.launchModal && (
                              <a
                                onClick={() =>
                                  this.modalClick(item.node.frontmatter)
                                }
                                href="#/"
                                rel="noopener noreferrer"
                                className="card-see-more"
                              >
                                <span className="seemore">Go To Link</span>
                              </a>
                            )}
  
                            {(item.node.frontmatter.external) &&
                              (!item.node.frontmatter.launchModal) &&
                              (
                                <a href={item.node.frontmatter.link} rel="noopener noreferrer" target="_blank" className="card-see-more">
                                    <span className="seemore">Go To Link</span>
                                </a>
                            )}
                          </div>
                      </div>
                    </div>
                  </div>
                  )}

                  {item.node.frontmatter.launchModal && (
                    <a onClick={() => this.modalClick(item.node.frontmatter)} href="#/" rel="noopener noreferrer">
                      <div className="card-link" key={index}>
                  <div className="card-container">
                    <div className="card-card">
                      <div className="card-img-holder">
                        <div
                          style={{
                            backgroundImage: item.node.frontmatter.image
                              ? "url(" + white + ")"
                              : "url(" +
                                this.state.colors[
                                  Math.floor(
                                    Math.random() * this.state.colors.length
                                  )
                                ] +
                                ")"
                          }}
                          className={((item.node.frontmatter.image)&&(item.node.frontmatter.image.toLowerCase().includes("hexagon-")))?"card-no-back":"card-back"}
                        >
                          <div>
                            {item.node.frontmatter.image && (
                              <div className={(item.node.frontmatter.image.toLowerCase().includes("hexagon-"))?"hex-card-img":"card-img"}>
                                <img
                                src={require("../../../../../../pages/tools/" +
                                  item.node.frontmatter.image)}
                                alt="Star" 
                                />
                              </div>
                              
                            )}
                            {!item.node.frontmatter.image && (
                              <p className="customLogo">
                                {item.node.frontmatter.title[0]}
                              </p>
                            )}
                          </div>
                        </div>
                        {item.node.frontmatter.external &&
                          (!item.node.frontmatter.launchModal) &&
                        (
                          <a
                            href={item.node.frontmatter.link}
                            target="_blank"
                            className="card-external-link"
                            rel="noopener noreferrer"
                          >
                            <img src={extIcon} alt="ext icon" />
                          </a>
                        )}

                          {item.node.frontmatter.external &&
                          (item.node.frontmatter.launchModal) &&
                          (
                              <a
                                  onClick={() =>
                                      this.modalClick(item.node.frontmatter)
                                  }
                                  href="#/"
                                  target="_blank"
                                  className="card-external-link"
                                  rel="noopener noreferrer"
                              >
                                  <img src={extIcon} alt="ext icon" />
                              </a>
                          )}


                      </div>
                      <div className="card-body">
                        <h5 className="card-title">
                          {item.node.frontmatter.title}
                        </h5>
                        <p
                          className="card-text"
                          dangerouslySetInnerHTML={{ __html: item.node.html }}
                        />
                        {!item.node.frontmatter.external &&
                          !item.node.frontmatter.launchModal && (
                            <a
                              href={item.node.frontmatter.link}
                              className="card-see-more"
                            >
                              <span className="seemore">Go To Link</span>
                            </a>
                          )}
                        {item.node.frontmatter.launchModal && (
                            <a
                              onClick={() =>
                                this.modalClick(item.node.frontmatter)
                              }
                              href="#/"
                              rel="noopener noreferrer"
                              className="card-see-more"
                            >
                              <span className="seemore">Go To Link</span>
                            </a>
                          )}

                          {(item.node.frontmatter.external) &&
                            (!item.node.frontmatter.launchModal) &&
                            (
                              <a href={item.node.frontmatter.link} rel="noopener noreferrer" target="_blank" className="card-see-more">
                                  <span className="seemore">Go To Link</span>
                              </a>
                          )}
                        </div>
                    </div>
                  </div>
                </div>
                    </a>
                  )}

                  {(item.node.frontmatter.external) && (!item.node.frontmatter.launchModal) &&(
                    <a href={item.node.frontmatter.link} rel="noopener noreferrer" target="_blank">
                      <div className="card-link" key={index}>
                        <div className="card-container">
                          <div className="card-card">
                            <div className="card-img-holder">
                              <div
                                style={{
                                  backgroundImage: item.node.frontmatter.image
                                    ? "url(" + white + ")"
                                    : "url(" +
                                      this.state.colors[
                                        Math.floor(
                                          Math.random() * this.state.colors.length
                                        )
                                      ] +
                                      ")"
                                }}
                                className={((item.node.frontmatter.image)&&(item.node.frontmatter.image.toLowerCase().includes("hexagon-")))?"card-no-back":"card-back"}
                              >
                                <div>
                                  {item.node.frontmatter.image && (
                                    <div className={(item.node.frontmatter.image.toLowerCase().includes("hexagon-"))?"hex-card-img":"card-img"}>
                                      <img
                                      src={require("../../../../../../pages/tools/" +
                                        item.node.frontmatter.image)}
                                      alt="Star" 
                                      />
                                    </div>
                                    
                                  )}
                                  {!item.node.frontmatter.image && (
                                    <p className="customLogo">
                                      {item.node.frontmatter.title[0]}
                                    </p>
                                  )}
                                </div>
                              </div>
                              {item.node.frontmatter.external &&
                                (!item.node.frontmatter.launchModal) &&
                              (
                                <a
                                  href={item.node.frontmatter.link}
                                  target="_blank"
                                  className="card-external-link"
                                  rel="noopener noreferrer"
                                >
                                  <img src={extIcon} alt="ext icon" />
                                </a>
                              )}

                                {item.node.frontmatter.external &&
                                (item.node.frontmatter.launchModal) &&
                                (
                                    <a
                                        onClick={() =>
                                            this.modalClick(item.node.frontmatter)
                                        }
                                        href="#/"
                                        target="_blank"
                                        className="card-external-link"
                                        rel="noopener noreferrer"
                                    >
                                        <img src={extIcon} alt="ext icon" />
                                    </a>
                                )}


                            </div>
                            <div className="card-body">
                              <h5 className="card-title">
                                {item.node.frontmatter.title}
                              </h5>
                              <p
                                className="card-text"
                                dangerouslySetInnerHTML={{ __html: item.node.html }}
                              />
                              {!item.node.frontmatter.external &&
                                !item.node.frontmatter.launchModal && (
                                  <a
                                    href={item.node.frontmatter.link}
                                    className="card-see-more"
                                  >
                                    <span className="seemore">Go To Link</span>
                                  </a>
                                )}
                              {item.node.frontmatter.launchModal && (
                                  <a
                                    onClick={() =>
                                      this.modalClick(item.node.frontmatter)
                                    }
                                    href="#/"
                                    rel="noopener noreferrer"
                                    className="card-see-more"
                                  >
                                    <span className="seemore">Go To Link</span>
                                  </a>
                                )}

                                {(item.node.frontmatter.external) &&
                                  (!item.node.frontmatter.launchModal) &&
                                  (
                                    <a href={item.node.frontmatter.link} rel="noopener noreferrer" target="_blank" className="card-see-more">
                                        <span className="seemore">Go To Link</span>
                                    </a>
                                )}
                              </div>
                          </div>
                        </div>
                      </div>
                    </a>
                  )}
                </span>
              ))}
          </div>
        </div>
        {this.props.list.length === 0 &&(
              <div className="result-not-found">No Results Found !!!</div>
            )}
      </div>
    );
  }
  /* eslint-enable import/no-dynamic-require */
  /* eslint-enable global-require */
}


