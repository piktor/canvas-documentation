import React, { Component } from "react";
import arrowDown from "../../../../images/arrow-down-not-coloured.svg";
import arrowDownColor from "../../../../images/arrow-down-coloured.svg";
import collapse_icon from '../../../../images/icon_collapse.svg';
import expand_icon from '../../../../images/icon_expand.svg';
import DevJourneyLeft from '../../../devJourneyComponent/components/devJourneySteps/components/devJourneyNavBar';
import "./index.css";

export default class ToolsAndDocsLeft extends Component {
  constructor() {
    super();
    this.state = {
      expand: false,
      activeLink: ""
    }
    this.handleMouseEnter = this.handleMouseEnter.bind(this);
    this.handleMouseLeave = this.handleMouseLeave.bind(this);
  }

  componentDidMount() {
    this.setState({
      activeLink: this.props.activeLink
    })
    if (this.state.activeLink !== "") this.changeFilter(this.state.activeNavLink);
  }
  componentWillReceiveProps(nextProps) {
    let self = this;
    if (this.props !== nextProps) {
      self.props = nextProps;
      self.setState({
        activeLink: self.props.activeLink
      })
    }
  }

  handleMouseEnter(className, activeLink) {
    if (!(this.state.activeLink === activeLink)) {
      document.getElementsByClassName(className)[0].src = arrowDownColor;
    }
  }

  handleMouseLeave(className, activeLink) {
    if (!(this.state.activeLink === activeLink)) {
      document.getElementsByClassName(className)[0].src = arrowDown;
    }
  }

  toogleClickEvent = () => {
    document.getElementById('devJourneyNavb').classList.toggle('collapseJourneyEvent');
    // document.getElementById('collapse-btn').classList.toggle('display-none-devBtn');
    document.getElementById('devJourneyNavb').classList.remove('.collapseJourneyEvent');

    if (document.getElementsByClassName('devJourneyNav').length > 0){
      document.getElementsByClassName('devJourneyNav')[0].classList.toggle('nav-expanded');
    }
    document.getElementById('collapse-btn-event').classList.toggle('btn-positn')
    this.setState({
      expand: !this.state.expand
    })
  }

  render() {
    return (
      <div className="toolsNDoc">
        <section className="categoryList">
          <ul>
            <li className="catTitle">
              <span>categories</span>
            </li>
            <li className={(((this.state.activeLink === "all") ? "active " : "") + "links")} onClick={() => this.props.callbackFilter("all")} onMouseEnter={() => this.handleMouseEnter(("linkArrow01"), "all")} onMouseLeave={() => this.handleMouseLeave(("linkArrow01"), "all")}>
              <div>ALL</div>
              <div className="arrowRight">
                <img src={((this.state.activeLink === "all") ? arrowDownColor : arrowDown)} alt="arrow" className={("linkArrow01")} />
              </div>
            </li>
            {this.props.categories.map((item, index) => (
              <li className={(((this.state.activeLink === item) ? "active " : "") + "links")} key={index} onClick={() => this.props.callbackFilter(item)} onMouseEnter={() => this.handleMouseEnter(("linkArrow" + index), item)} onMouseLeave={() => this.handleMouseLeave(("linkArrow" + index), item)}>
                <div>{item}</div>
                <div className="arrowRight">
                  <img src={((this.state.activeLink === item) ? arrowDownColor : arrowDown)} alt="arrow" className={("linkArrow" + index)} />
                </div>
              </li>
            ))}
          </ul>
        </section>
        {/* <aside className="devJourneyNavEvent" id="devJourneyNavb">
          <DevJourneyLeft />
        </aside> */}
      </div>
    );
  }
}
