import React, { Component } from "react";
import ExpandHeader from "../header/expandHeader";
import ToolsAndDocLeft from "./components/toolsAndDocsLeft";
import ToolsAndDocRight from "./components/toolsAndDocsRight";
import NavHeader from "../header/navHeader";
import Footer from "../footer";
import _ from "lodash";
import "./index.css";
import star from "../../images/icon-favorites.svg";
import starHover from "../../images/icon-favorites-hover.svg";
import collapse_icon from '../../images/icon_collapse.svg';
import expand_icon from '../../images/icon_expand.svg';
import DevJourneyNavBar from '../devJourneyComponent/components/devJourneySteps/components/devJourneyNavBar';  

export default class ToolAndDocs extends Component {
  constructor() {
    super();
    // this.starMouseEnter = this.starMouseEnter.bind(this);
    // this.starMouseLeave = this.starMouseLeave.bind(this);
    // this.starClicked = this.starClicked.bind(this);
    this.state = {
      tagName: "",
      firstSplit: "",
      filtredData: [],
      toolsData: [],
      documentsData: [],
      starActive: false,
      activeNavLink: "all",
      getStageData: [],
      expand: false
    };
    this.changeFilter = this.changeFilter.bind(this);
  }

  componentDidMount() {
    let firstSplit = window.location.search.split("?section=")[1];
    this.setState({ firstSplit });
    let self = this;
    if (firstSplit.includes("&tag=")) {
      let tagNameVal = firstSplit.split("&tag=")[1];
      let stageValComp1 = firstSplit.split("&tag=")[0];
      tagNameVal = tagNameVal.replace(/%20/g, " ");
      let getStageData1 = _.filter(self.props.data, function (item) {
        if (
          item.node.frontmatter.stage !== undefined &&
          item.node.frontmatter.stage.includes(stageValComp1)
        ) {
          return item;
        }
      });
      self.setState(
        { getStageData: getStageData1, tagName: tagNameVal },
        () => {
          self.getCatogoryFilter();
          self.getAllToolsData();
          self.getAllDocumentsData();
        }
      );
    } else {
      let stageValComp2 = firstSplit;
      let getStageData2 = _.filter(self.props.data, function (item) {
        if (
          item.node.frontmatter.stage !== undefined &&
          item.node.frontmatter.stage.includes(stageValComp2)
        ) {
          return item;
        }
      });
      self.setState({ getStageData: getStageData2, tagName: "" }, () => {
        self.getCatogoryFilter();
        self.getAllToolsData();
        self.getAllDocumentsData();
      });
    }
  }

  componentDidUpdate(prevProps) {
    let firstSplit = window.location.search.split("?section=")[1];
    let self = this;
    if (this.state.firstSplit !== firstSplit && this.state.tagName === "") {
      let stageValComp11 = firstSplit;
      let getStageData11 = _.filter(self.props.data, function (item) {
        if (
          item.node.frontmatter.stage !== undefined &&
          item.node.frontmatter.stage.includes(stageValComp11)
        ) {
          return item;
        }
      });
      self.setState(
        { getStageData: getStageData11, firstSplit: firstSplit },
        () => {
          self.getCatogoryFilter();
          self.getAllToolsData();
          self.getAllDocumentsData();
        }
      );
    }
    if (this.state.firstSplit !== firstSplit && this.state.tagName !== "") {
      if (firstSplit.includes("&tag=")) {
        let tagNameVal = firstSplit.split("&tag=")[1];
        let stageValComp12 = firstSplit.split("&tag=")[0];
        tagNameVal = tagNameVal.replace(/%20/g, " ");
        let getStageData12 = _.filter(self.props.data, function (item) {
          if (
            item.node.frontmatter.stage !== undefined &&
            item.node.frontmatter.stage.includes(stageValComp12)
          ) {
            return item;
          }
        });
        self.setState(
          { getStageData: getStageData12, tagName: tagNameVal },
          () => {
            self.getCatogoryFilter();
            self.getAllToolsData();
            self.getAllDocumentsData();
            self.changeFilter(self.changeFilter(self.state.activeNavLink));
          }
        );
      } else {
        let stageValComp13 = firstSplit;
        let getStageData13 = _.filter(self.props.data, function (item) {
          if (
            item.node.frontmatter.stage !== undefined &&
            item.node.frontmatter.stage.includes(stageValComp13)
          ) {
            return item;
          }
        });
        self.setState({ getStageData: getStageData13, tagName: "" }, () => {
          self.getCatogoryFilter();
          self.getAllToolsData();
          self.getAllDocumentsData();
          self.changeFilter(self.changeFilter(self.state.activeNavLink));
        });
      }
    }
  }

  getCatogoryFilter() {
    let returnFilterArray = this.state.getStageData;
    let getAllCatogery = _.flatten(
      _.map(returnFilterArray, function (item) {
        if (item.node.frontmatter.categories !== null) {
          return item.node.frontmatter.categories.split("||");
        } else {
          return;
        }
      })
    );
    let uniqe = _.uniq(getAllCatogery);
    let compact = _.compact(uniqe);
    let filtredData = _.orderBy(compact, [item => item.toLowerCase()], ["asc"]);
    this.setState({
      filtredData
    });
  }

  changeFilter(category) {
    let self = this;
    function filterDataByCategoryAndClass(getToolsAndDocsData, classification) {
      const filterTools = _.filter(getToolsAndDocsData, data => {
        if (data.node.frontmatter.categories !== null) {
          return (
            data.node.frontmatter.categories.split("||").indexOf(category) !==
            -1 && data.node.frontmatter.classification === classification
          );
        }

      });

      return filterTools;
    }

    if (category !== undefined && category !== "" && category !== "all") {
      const filterTools = filterDataByCategoryAndClass(
        this.state.getStageData,
        "tools"
      );
      const orderedTools = _.orderBy(filterTools, "node.frontmatter.title");
      const filterDocs = filterDataByCategoryAndClass(
        this.state.getStageData,
        "documents"
      );
      const orderedDocs = _.orderBy(filterDocs, "node.frontmatter.title");

      if (self.state.tagName.length > 0) {
        var toolsDataFilter = _.filter(orderedTools, function (item) {
          let tagArray = item.node.frontmatter.tag.split("||");
          if (tagArray.indexOf(self.state.tagName) > -1) {
            return item;
          }
        });
        var toolsDataDocs = _.filter(orderedDocs, function (item) {
          let tagArray = item.node.frontmatter.tag.split("||");
          if (tagArray.indexOf(self.state.tagName) > -1) {
            return item;
          }
        });
        self.setState({
          toolsData: toolsDataFilter,
          documentsData: toolsDataDocs,
          activeNavLink: category
        });
      } else {
        self.setState({
          toolsData: orderedTools,
          documentsData: orderedDocs,
          activeNavLink: category
        });
      }
    }
    if (category === "all") {
      this.getAllToolsData();
      this.getAllDocumentsData();
      self.setState({
        activeNavLink: "all"
      });
    }
  }

  getAllToolsData() {
    let returnToolsData = this.state.getStageData;

    var filToolData = _.filter(returnToolsData, {
      node: { frontmatter: { classification: "tools" } }
    });
    if (this.state.tagName.length > 0) {
      let self = this;
      var toolsData = _.filter(filToolData, function (item) {
        let tagArray = item.node.frontmatter.tag.split("||");
        if (tagArray.indexOf(self.state.tagName) > -1) {
          return item;
        }
      });
    } else {
      toolsData = filToolData;
    }
    this.setState({
      toolsData
    });
  }

  getAllDocumentsData() {
    let returnDocsData = [];
    returnDocsData = this.state.getStageData;
    let filDocumentsData = _.filter(returnDocsData, {
      node: { frontmatter: { classification: "documents" } }
    });
    if (this.state.tagName.length > 0) {
      let self = this;
      var documentsData = _.filter(filDocumentsData, function (item) {
        let tagArray = item.node.frontmatter.tag.split("||");
        if (tagArray.indexOf(self.state.tagName) > -1) {
          return item;
        }
      });
    } else {
      documentsData = filDocumentsData;
    }
    this.setState({
      documentsData
    });
  }

  // starMouseLeave() {
  //   if (!this.state.starActive) {
  //     let element = this.refs.star;
  //     element.src = star;
  //   }
  // }
  // starMouseEnter() {
  //   if (!this.state.starActive) {
  //     let element = this.refs.star;
  //     element.src = starHover;
  //   }
  // }
  // starClicked() {
  //   let self = this;
  //   let element = this.refs.star;
  //   if (this.state.starActive) {
  //     element.src = star;
  //   } else {
  //     element.src = starFilled;
  //   }

  //   self.setState({ starActive: !self.state.starActive });
  // }

  // toogleClick = () => {
  //   document.getElementById('devJourneyNavb') && document.getElementById('devJourneyNavb').classList.toggle('collapseJourneyEvent');
  //   // document.getElementById('collapse-btn').classList.toggle('display-none-devBtn');
  //   document.getElementById('devJourneyNavb') && document.getElementById('devJourneyNavb').classList.remove('.collapseJourneyEvent');

  //   document.getElementById('devJourneyNavbarr') && document.getElementById('devJourneyNavbarr').classList.toggle('collapseJourney');
  //   // document.getElementById('devJourneyNavb') && document.getElementById('devJourneyNavb').classList.toggle('collapseJourney');
  //   document.getElementById('devJourneyRight-div') && document.getElementById('devJourneyRight-div').classList.toggle('devJourneyRi');
  //   var staticModal = document.getElementsByClassName('Modal-static');
  //   if (staticModal[0]) {
  //     staticModal[0].classList.toggle('modal-collapsed');
  //   }
  //   if (window.location.pathname.indexOf(("/devJourney/") > -1)) {
  //     document.getElementById('collapse-btn').classList.toggle('rotate-btn');
  //   }
  //   this.setState({
  //     expand: !this.state.expand
  //   })
  // }

  toogleClickEvent = () => {

    if (document.getElementById('devJourneyNavb')) {
      document.getElementById('devJourneyNavb').classList.toggle('collapseJourneyEvent');
      // // document.getElementById('collapse-btn').classList.toggle('display-none-devBtn');
      document.getElementById('devJourneyNavb') && document.getElementById('devJourneyNavb').classList.remove('.collapseJourneyEvent');
      
    }

      if (document.getElementsByClassName('devJourneyNav').length > 0){
        document.getElementsByClassName('devJourneyNav')[0].classList.toggle('nav-expanded');
      }
      document.getElementById('devJourneyNavbarr') && document.getElementById('devJourneyNavbarr').classList.toggle('collapseJourney');
      // document.getElementById('devJourneyNavb') && document.getElementById('devJourneyNavb').classList.toggle('collapseJourney');
      document.getElementById('devJourneyRight-div') && document.getElementById('devJourneyRight-div').classList.toggle('devJourneyRi');
    
    if (window.location.pathname.indexOf(("/toolsdocs") > -1)) {
      document.getElementById('collapse-btn').classList.toggle('rotate-btn');
    }
    this.setState({
      expand: !this.state.expand
    })

    // document.getElementById('collapse-btn').classList.toggle('display-none-devBtn');
    // document.getElementById('devJourneyNavb') && document.getElementById('devJourneyNavb').classList.remove('.collapseJourneyEvent');
    // document.getElementById('devJourneyNavb') && document.getElementById('devJourneyNavb').classList.toggle('collapseJourneyEvent');
    // document.getElementById('devJourney-tour') && document.getElementById('devJourney-tour').classList.toggle('devJourney-tour-posin');
    // this.setState({
    //   expand: !this.state.expand
    // })
  }

  render() {
    return (
      <div className="toolsAndDocs">
        <ExpandHeader />
        <NavHeader />
        <section className="tnd-bg">
        <aside className="devJourneyNav">
          <div className="devJourneyNavbarr collapseJourney" id="devJourneyNavbarr" style={(typeof window !== 'undefined' && window.localStorage.getItem('JOURNEY_VALUE')) ? { display: "block" } : { display: "none" }}>
            <DevJourneyNavBar />
          </div>
        </aside>
        <div className="collapse-btn-event toolsdevJourney" title="Dev Journey" id="collapse-btn-event" onClick={this.toogleClickEvent} style={(typeof window !== 'undefined' && window.localStorage.getItem('JOURNEY_VALUE')) ? { display: "block" } : { display: "none" }}>
          <img src={expand_icon} alt="expand-icon" />
        </div>
        <aside className="devJourneyNavEvent devJourneyNavEvent-lr" id="devJourneyNavb" style={(typeof window !== 'undefined' && window.localStorage.getItem('JOURNEY_VALUE')) ? { display: "block" } : { display: "none" }} >
          <DevJourneyNavBar />
        </aside>
        <div className="toolsandDocs max-width-tools">
          <aside className="toolsandDocs-left">
            <ToolsAndDocLeft
              categories={this.state.filtredData}
              callbackFilter={this.changeFilter}
              activeLink={this.state.activeNavLink}
            />
          </aside>
          <main className="toolsandDocs-right">
            <section
              className="breadcrumb-img-cont-2"
            //onMouseEnter={() => this.starMouseEnter()}
            //onMouseLeave={() => this.starMouseLeave()}
            // onClick={() => this.starClicked()}
            >
              <img
                //className="breadcrumb-img-2"
                //src={star}
                //ref="star"
                //alt="star"
                //title="Coming Soon"
              />
              </section>
              <ToolsAndDocRight
                toolsData={this.state.toolsData}
                documentsData={this.state.documentsData}
              />
            </main>
          </div>
        </section>
        {/* <UpperFooter /> */}
        <Footer />
      </div>
    );
  }
}
