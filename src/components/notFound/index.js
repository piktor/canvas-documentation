import React, { Component } from "react";
import Header from "../header/expandHeader";
import src from "../../images/404-img.gif";
import { Link } from "gatsby";
import './index.css';

export default class NotFound extends Component {
  render() {
    return (
      <div>
          <Header/>
          <section className="notFoundDiv">
            <article className="nf-404-div">
                <h1 className="nf-404-title">Oops!</h1>
                <h2 className="nf-404-subtitle">We can't seem to find the page you're looking for.</h2>
                <h6 className="nf-404-sub">Error 404</h6>
                <p  className="nf-404-cont">
                    Why don't you try going...
                    <ul>
                        <li><Link to="/"  className="nf-404-link">Home</Link></li>
                        <li><Link to="/devJourney/" className="nf-404-link">Dev Journey</Link></li>
                        <li><Link to="/toolsdocs/" className="nf-404-link">Tools &amp; Docs</Link></li>
                        <li><Link to="/eventsList/" className="nf-404-link">Events</Link></li>
                    </ul>
                </p>
            </article>
            <article><img src={src} alt="404-img" className="nf-404-img"/></article>
          </section>
        </div>
    );
  }
}
