import React, { Component } from "react";
import SEO from "../../components/seo";
import Header from "../header/expandHeader";
import DevJourneySteps from "./components/devJourneySteps";
import DevJourneyLanding from "./components/devJourneyLanding";
import queryString from "query-string";

import "./index.css";

export default class DevJourneyComponent extends Component {
  constructor() {
    super();
    this.state = {
      landingPage: true,
      search: ""
    };
  }

  componentDidMount() {
    let self = this;
    let search = window.location.search;
    if (search !== "") {
      let query = queryString.parse(search);
      let artifactName = query.artifactName;
      var devJson = this.props.devJourneyJson.edges.filter((item)=>{
        return item.node.name.toLowerCase() == artifactName.toLowerCase();
      });
      var devJourneyJson = devJson[0].node;
      window.localStorage.setItem("devJourneyJson", JSON.stringify(devJourneyJson));
      self.setState({
        landingPage: false,
        search: search,
        // pass only the current journey info
        devJourneyJson: devJourneyJson
      });
    } else {
      self.setState({
        landingPage: true,
        search: ""
      });
    }
  }

  componentDidUpdate() {
    let self = this;
    let search = window.location.search;
    if (search !== this.state.search) {
      if (search !== "") {
        let query = queryString.parse(search);
        let artifactName = query.artifactName;
        var devJson = this.props.devJourneyJson.edges.filter((item)=>{
          return item.node.name.toLowerCase() == artifactName.toLowerCase();
        });
        var devJourneyJson = devJson[0].node;
        window.localStorage.setItem("devJourneyJson", JSON.stringify(devJourneyJson));
        self.setState({
          landingPage: false,
          search: search,
          // pass only the current journey info
          devJourneyJson: devJourneyJson
        });
      } else {
        self.setState({
          landingPage: true,
          search: ""
        });
      }
    }
  }

  render() {
    return (
      <div className="devJourneyComponent">
        <SEO title="Dev Journey" />
        <Header />
        {this.state.landingPage && <DevJourneyLanding />}
        {!this.state.landingPage && (
          <DevJourneySteps
            devJourneyStep={this.props.devJourneyDetails}
            staticpageDet={this.props.staticPageDetails}
            linkedPageDet={this.props.linkedPageDetails}
            devJourneyJson={this.state.devJourneyJson}
          />
        )}
      </div>
    );
  }
}
