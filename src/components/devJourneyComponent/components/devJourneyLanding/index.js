import React, { Component } from 'react';
import Link from "gatsby-link";
import "./index.css";
import DevJourneyCards from "../../../../pages/devJourney/devJourneyCards.json";

export default class DevJourneyLanding extends Component {
  constructor(){
    super();
    this.cards=this.cards.bind(this);
    this.subcategory=this.subcategory.bind(this);
    this.handleDisabledClick=this.handleDisabledClick.bind(this);
    this.showSubcategory=this.showSubcategory.bind(this);
  }

  showSubcategory() {
    document.getElementById('djSubCards').style.display = "flex";
  }

  handleDisabledClick(e) {
    e.preventDefault();
  }

  subcategory() {
    debugger
    if(typeof window !== 'undefined' && !window.localStorage.getItem('JOURNEY_VALUE')) {
      return DevJourneyCards.map((item) => {
        if(item.subcategory!=="none") {
          let s = item.subcategory;
          return s.map((i) => (
              <Link to={item.link + i.path} className="subCat">
                <h5 className="djSubcategory-title">{i.name}</h5>
              </Link>
          ))
        }
      });
    }
    else if(typeof window !== 'undefined' && window.localStorage.getItem('JOURNEY_VALUE')) {
      return DevJourneyCards.map((item) => {
        if(item.subcategory!=="none") {
        let s = item.subcategory;
        return s.map((i) => (
          <Link to={item.link+window.localStorage.getItem('JOURNEY_VALUE')} className="subCat">
            <h5 className="djSubcategory-title">{i.name}</h5>
          </Link>
        ))
        }
      });
    }
  }

  cards() {
    var dj = DevJourneyCards.map((item, index) => {
      return (
        <span key={index}>
        {typeof window !== 'undefined' && !window.localStorage.getItem('JOURNEY_VALUE') && item.subcategory==="none" && (
        <Link to={item.link + item.path}  onClick={(item.link==="disabled")? this.handleDisabledClick : null} className={(item.link==="disabled")?"disabledCard":"activeCard"}>
        <div className="devJourneyLandingLowerCard">
          <img src={require('../../../../images/' + item.icon)} alt="card-icon" className={(item.link==="disabled")?"devJourneyLandingLowerCard-icon disabledCardOpacity":"devJourneyLandingLowerCard-icon"}/>
          <h5 className={(item.link==="disabled")?"devJourneyLandingLowerCard-title disabledCardOpacity":"devJourneyLandingLowerCard-title"}>{item.name}</h5>
        </div>
        </Link>
        )}
        {typeof window !== 'undefined' && window.localStorage.getItem('JOURNEY_VALUE') && item.subcategory==="none" && (
          <Link to={item.link+window.localStorage.getItem('JOURNEY_VALUE')}  onClick={(item.link==="disabled")? this.handleDisabledClick : null} className={(item.link==="disabled")?"disabledCard":"activeCard"}>
          <div className="devJourneyLandingLowerCard">
            <img src={require('../../../../images/' + item.icon)} alt="card-icon" className={(item.link==="disabled")?"devJourneyLandingLowerCard-icon disabledCardOpacity":"devJourneyLandingLowerCard-icon"}/>
            <h5 className={(item.link==="disabled")?"devJourneyLandingLowerCard-title disabledCardOpacity":"devJourneyLandingLowerCard-title"}>{item.name}</h5>
          </div>
          </Link>
        )}
        {/* Dev Journey items with sub-category */}
        {item.subcategory!=="none" && (
          <span onClick={(item.link==="disabled")? this.handleDisabledClick : this.showSubcategory} className={(item.link==="disabled")?"disabledCard":"activeCard"}>
            <div className="devJourneyLandingLowerCard">
              <img src={require('../../../../images/' + item.icon)} alt="card-icon" className={(item.link==="disabled")?"devJourneyLandingLowerCard-icon disabledCardOpacity":"devJourneyLandingLowerCard-icon"}/>
              <h5 className={(item.link==="disabled")?"devJourneyLandingLowerCard-title disabledCardOpacity":"devJourneyLandingLowerCard-title"}>{item.name}</h5>
            </div>
          </span>
        )}
        </span>
      );
    });
    return dj;
  }

  render() {
    return (
      <section className="devJourneyLanding">
        <article className="devJourneyLandingArticle">
          <div className="djanihide">
            <h2 className="devJourneyLandingUpperHeading-small">WELCOME TO YOUR</h2>
            <h1 className="devJourneyLandingUpperHeading-big">DEVELOPER JOURNEY</h1>
            <p className="devJourneyLandingUpperContent">Developer Journey is designed to assist you to get your tasks done proficiently through the right processes.</p>
          </div>
          <div className="djanishow">
            <h2 className="devJourneyLandingLowerHeading">What would you like to build today?</h2>
            <div className="devJourneyLandingLowerCardSection">{this.cards()}</div>
            <div className="djSubCards" id="djSubCards">{this.subcategory()}</div>
          </div>
        </article>
      </section>
    )
  }
}
