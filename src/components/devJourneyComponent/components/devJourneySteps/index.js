import React, { Component } from "react";
import DevJourneyNavBar from "./components/devJourneyNavBar";
import DevJourneyRight from "./components/devJourneyRight";
import { Link } from "gatsby";
import "./index.css";
import _ from "lodash";
import queryString from "query-string";


export default class DevJourneySteps extends Component {
  constructor() {
    super();
    this.state = {
      setProcessOrder: "",
      stageName: "",
      processOrder: 0,
      nextStage: "",
      previousStage: "",
      path: "",
      backpath: "",
      artifactName: "",
      hideNext: false,
      hideBack: true
    };
  }

  componentDidMount() {
    let search = window.location.search;
    let query = queryString.parse(search);
    query.processOrder = query.processOrder? query.processOrder : '0';

    this.setState({
      setProcessOrder: query.processOrder,
      artifactName: query.artifactName
    });
    let nextValues = this.getNextStages(query);
    if (parseInt(query.processOrder) + 1 <= nextValues.limit) {
      this.setState({
        processOrder: parseInt(query.processOrder) + 1,
        nextStage: nextValues.nextStage,
        path: nextValues.path
      });
    } else {
      this.setState({ processOrder: null });
    }
    let previousValues = this.getBackStages(query);
    if (parseInt(query.processOrder) - 1 <= previousValues.limit) {
      this.setState({
        processOrder: parseInt(query.processOrder) + 1,
        previousStage: previousValues.previousStage,
        backpath: previousValues.backpath
      });
    } else {
      this.setState({ processOrder: null });
    }
  }

  componentDidUpdate() {
    let search = window.location.search;
    let query = queryString.parse(search);
    query.processOrder = query.processOrder? query.processOrder : '0';

    if (query.processOrder !== this.state.setProcessOrder) {
      this.setState({
        setProcessOrder: query.processOrder,
        artifactName: query.artifactName
      });
      let nextValues = this.getNextStages(query);
        if (parseInt(query.processOrder) + 1 <= nextValues.limit) {
          this.setState({
            processOrder: parseInt(query.processOrder) + 1,
            nextStage: nextValues.nextStage,
            path: nextValues.path
          });
        } else {
          this.setState({ processOrder: null });
        }
        let getNextPathVal = localStorage.getItem("nextstepVal");
        if (getNextPathVal) {
          if (parseInt(query.processOrder) > parseInt(this.state.setProcessOrder)) {
            localStorage.setItem("nextstepVal", query.processOrder);
          }
        } else {
          localStorage.setItem("nextstepVal", query.processOrder);
        }
      
      let previousValues = this.getBackStages(query);
      if (previousValues) {
        if (parseInt(query.processOrder) - 1 <= nextValues.limit) {
          this.setState({
            processOrder: parseInt(query.processOrder) + 1,
            previousStage: nextValues.previousStage,
            backpath: previousValues.backpath
          });
        } else {
          this.setState({ processOrder: null });
        }
        let getPreviousPathVal = localStorage.getItem("previousstepVal");
        if (getPreviousPathVal) {
          if (parseInt(query.processOrder) > parseInt(this.state.setProcessOrder)) {
            localStorage.setItem("previousstepVal", query.processOrder);
          }
        } else {
          localStorage.setItem("previousstepVal", query.processOrder);
        }
      }
    }
  }

  getNextStages(data) {
    let self = this;
    let obj = {};
    if (data.processOrder) {
      var DevJson = this.props.devJourneyJson;
      let journeyIndex = DevJson.buildartifacts.map(function(e) { return e.artifactName.toLowerCase(); }).indexOf(data.artifactName.toLowerCase());
      let devJson = DevJson.buildartifacts[journeyIndex].stages;
      var processes = devJson[devJson.length - 1].processes;
      obj.limit = parseInt(processes[processes.length - 1].processOrder) + 1;
      let nextStage;
      devJson.map((stage)=>{
        stage.processes.map((item)=>{
          if (item.processOrder == parseInt(data.processOrder) + 1){
            nextStage = item
          } else if (item.processOrder == parseInt(data.processOrder)) {
            this.state.stageName = item.ProcessTitle
          }
        })
      })
      self.setState({
        hideNext: false
      });
      if (data.processOrder < obj.limit - 1){
        obj["nextStage"] = nextStage.ProcessTitle;
        obj["path"] = nextStage.path;
      } else {
        self.setState({
          hideNext: true
        });
      }
      return obj;
    } else {
      return;
    }
  }

  getBackStages(data) {
    let self = this;
    let obj = {};
    if (data.processOrder) {
      var DevJson = this.props.devJourneyJson;
      let journeyIndex = DevJson.buildartifacts.map(function(e) { return e.artifactName.toLowerCase(); }).indexOf(data.artifactName.toLowerCase());
      let devJson = DevJson.buildartifacts[journeyIndex].stages;
      var processes = devJson[devJson.length - 1].processes;
      obj.limit = parseInt(processes[processes.length - 1].processOrder) + 1;
      
      let previousStage;
      devJson.map((stage)=>{
        stage.processes.map((item)=>{
          if (item.processOrder == parseInt(data.processOrder) - 1){
            previousStage = item
          }  else if (item.processOrder == parseInt(data.processOrder)) {
            this.state.stageName = item.ProcessTitle
          }
        })
      })

      self.setState({
        hideBack: false
      });

      
      if (data.processOrder >= 1){
        obj["previousStage"] = previousStage.ProcessTitle;
        obj["backpath"] = previousStage.path;
      } else {
          self.setState({
            hideBack: true
        });
      }
      return obj;
    } else {
      return;
    }
  }

  disableNav(e) {
    // function disable nav
    e.preventDefault();
    return false;
  }

  render() {
    if (this.props.devJourneyJson){
      return (
        <section className="devJourney-bg">
          <ol className="breadcrumbb">
            <div className="breadcrumb-flex">
              <li className="breadcrumb-li breadcrumb-item">
              {typeof window !== 'undefined' && window.localStorage.getItem('JOURNEY_VALUE') && (
                <Link to="/devJourney" className="breadcrumb-li text-style-1" >
                  DEV JOURNEY
                </Link>
              )}
              {typeof window !== 'undefined' && !window.localStorage.getItem('JOURNEY_VALUE') && (
                <span className="breadcrumb-li text-style-1" >
                DEV JOURNEY
              </span>
            )}
              </li>
              <li className="breadcrumb-li breadcrumb-item">
                <a
                  href="#/"
                  onClick={e => this.disableNav(e)}
                  className="breadcrumb-li text-style-1"
                >
                  {this.props.devJourneyJson.buildartifacts[0].artifactTitle}
                </a>
              </li>
              <li className="breadcrumb-li breadcrumb-item">
                <a
                  href="#/"
                  onClick={e => this.disableNav(e)}
                  className="breadcrumb-li text-style-1"
                >
                  {this.state.stageName}
                </a>
              </li>
              <li className="breadcrumb-li breadcrumb-item">
                <a
                  href="#/"
                  onClick={e => this.disableNav(e)}
                  className="breadcrumb-li text-style-1"
                >
                  {/* {this.state.pageDetails.heading} */}
                </a>
              </li>
            </div>
            <li
              className="breadcrumb-img-cont"
              // onMouseEnter={() => this.starMouseEnter()}
              // onMouseLeave={() => this.starMouseLeave()}
              // onClick={() => this.starClicked()}
            />
          </ol>
  
          <div className="devJourney max-width-devjourney">
            <aside className="devJourneyNav">
              <div className="devJourneyNavbarr" id="devJourneyNavbarr">
                <DevJourneyNavBar devJourneyJson={this.props.devJourneyJson} />
              </div>
            </aside>
            <article className="devJourneyContent">
              <div className="devJourneyRight-div" id="devJourneyRight-div">
                <DevJourneyRight
                  devJourneySteps={this.props.devJourneyStep}
                  staticPage={this.props.staticpageDet}
                  linkedPage={this.props.linkedPageDet}
                  devJourneyJson={this.props.devJourneyJson}
                />
              </div>
            </article>
          </div>
        </section>
      );
    } else {
      return (<div></div>)
    }
    
  }
}
