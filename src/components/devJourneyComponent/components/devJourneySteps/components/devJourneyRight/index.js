import React, { Component } from "react";
import queryString from "query-string";
import _ from "lodash";
import "./index.css";
import Link from "gatsby-link";
import arrow from "../../../../../../images/icon_arrow-devJourney.svg";
import ModalFrame from "../../../modalComponent";
import StaticBody from "../../../../../../templates/staticPageMarkdown/components/staticPageBody";
import MultiSite from "../../../../../../templates/multiSiteMarkDown/components/rightSideContent";

export default class DevJourneyRight extends Component {
  constructor() {
    super();
    this.state = {
      //pageDetails: {},
      //setRefId: "",
      stageName: "",
      processOrder: 0,
      nextStage: "",
      previousStage: "",
      path: "",
      backpath: "",
      artifactName: "",
      hideNext: false,
      hideBack: true,
      modalIsOpen: false,
      staticMarkdown: {}
    };
    this.handleAnimation = this.handleAnimation.bind(this);
    this.handleBackAnimation = this.handleBackAnimation.bind(this);
    this.modalClick = this.modalClick.bind(this);
    this.handleModalState = this.handleModalState.bind(this);
    this.linkclick = this.linkclick.bind(this);
    this.staticMarkDown = "";
    this.pathslug = "";
    this.journeyEnd = this.journeyEnd.bind(this);
    this.closeModal = this.closeModal.bind(this);
  }

  createDesc(desc) {
    return { __html: desc };
  }

  componentDidMount() {
    document.addEventListener("keydown", this._handleKeyDown);
    let search = window.location.search;
    let query = queryString.parse(search);
    query.processOrder = query.processOrder? query.processOrder : '0';

    //let pageDetails = journeyDetails[query.refId];
    this.setState({
      //pageDetails,
      setProcessOrder: query.processOrder,
      stageName: query.stageName,
      artifactName: query.artifactName
    });
    let nextValues = this.getNextStages(query);
    if (parseInt(query.processOrder) + 1 <= nextValues.limit) {
      this.setState({
        processOrder: parseInt(query.processOrder) + 1,
        nextStage: nextValues.nextStage,
        path: nextValues.path
      });
    } else {
      this.setState({ processOrder: null });
    }
    let previousValues = this.getBackStages(query);
    if (parseInt(query.processOrder) - 1 <= previousValues.limit) {
      this.setState({
        processOrder: parseInt(query.processOrder) + 1,
        previousStage: previousValues.previousStage,
        backpath: previousValues.backpath
      });
    } else {
      this.setState({ processOrder: null });
    }

    if((document.location.pathname.indexOf("/devJourney"))>-1) {
      document.getElementById('main-html').classList.add('no-scroll');
    }
    else{
      document.getElementById('main-html').classList.remove('no-scroll');
    }
    if(this.modalIsOpen === true){
      document.querySelector(".devJourneyNavbr").addEventListener("click", this.closeModal);
    }
    if(document.querySelector('.external') !== null ) {
      var anchors = document.querySelectorAll('.external');
      for (var i=0; i<anchors.length; i++){
        let a = anchors[i].getElementsByTagName('a');
        a[0].setAttribute('target', '_blank');
      }
    }
  }

  componentDidUpdate() {
    var linkClass = document.querySelectorAll(".iLink a");
    for (var i = 0; i < linkClass.length; i++) {
      linkClass[i].removeEventListener("click", this.linkclick);
      linkClass[i].addEventListener("click", this.linkclick);
    }
    let search = window.location.search;
    let query = queryString.parse(search);
    query.processOrder = query.processOrder? query.processOrder : '0';

    if (query.processOrder !== this.state.setProcessOrder && query.processOrder!== undefined) {
      //let pageDetails = journeyDetails[query.refId];
      this.setState({
        //pageDetails,
        setProcessOrder: query.processOrder,
        //stageName: query.stageName,
        artifactName: query.artifactName
      });
      let nextValues = this.getNextStages(query);
      if (parseInt(query.processOrder) + 1 <= nextValues.limit) {
        this.setState({
          processOrder: parseInt(query.processOrder) + 1,
          nextStage: nextValues.nextStage,
          path: nextValues.path
        });
      } else {
        this.setState({ processOrder: null });
      }
      let getNextPathVal = localStorage.getItem("nextstepVal");
      if (getNextPathVal) {
        if (parseInt(query.processOrder) > parseInt(this.state.setProcessOrder)) {
          localStorage.setItem("nextstepVal", query.processOrder);
        }
      } else {
        localStorage.setItem("nextstepVal", query.processOrder);
      }
      
      let previousValues = this.getBackStages(query);
      if (previousValues) {
        if (parseInt(query.processOrder) - 1 <= nextValues.limit) {
          this.setState({
            processOrder: parseInt(query.processOrder) + 1,
            previousStage: nextValues.previousStage,
            backpath: previousValues.backpath
          });
        } else {
          this.setState({ processOrder: null });
        }
        let getPreviousPathVal = localStorage.getItem("previousstepVal");
        if (getPreviousPathVal) {
          if (parseInt(query.processOrder) > parseInt(this.state.setProcessOrder)) {
            localStorage.setItem("previousstepVal", query.processOrder);
          }
        } else {
          localStorage.setItem("previousstepVal", query.processOrder);
        }
      }
    }
    if(this.modalIsOpen === true){
      document.querySelector(".devJourneyNavbr").addEventListener("click", this.closeModal);
    }
    if(document.querySelector('.external') !== null ) {
      var anchors = document.querySelectorAll('.external');
      for (var i=0; i<anchors.length; i++){
        let a = anchors[i].getElementsByTagName('a');
        for (var k=0; k<a.length; k++){
          a[k].setAttribute('target', '_blank');
        }
      }
    }
  }

  componentWillUnmount() {
    if((document.location.pathname.indexOf("/devJourney"))>-1) {
      document.getElementById('main-html').classList.add('no-scroll');
    }
    else{
      document.getElementById('main-html').classList.remove('no-scroll');
    }
    
    var linkClass = document.getElementsByClassName("iLink");
    for (var i = 0; i < linkClass.length; i++) {
      linkClass[i].removeEventListener("click", e => this.linkclick(e));
    }
    document.removeEventListener("keydown", this._handleKeyDown);
  }

  linkclick = event => {
    console.log(event.target);
    // var ilink = document.getElementsByClassName("iLink");
    var mpath = event.target.getAttribute("href");
    event.target.href = "#";
    event.stopPropagation();
    var self = this;
    // for (var i = 0; i < ilink.length; i++) {
    //   mpath = ilink[0].getElementsByTagName("a")[0].getAttribute("href");
    //   ilink[0].getElementsByTagName("a")[0].href = "#";
    //   event.stopPropagation();
    // }
    if (mpath !== "#") {
      this.pathslug = mpath;
    }
    let pageList = [];
    if (this.pathslug.startsWith("/sites/linked/")) {
      pageList = this.props.linkedPage;
      this.showInFrame = true;      
    } else {
      pageList = this.props.staticPage;
      this.showInFrame = false;
    }
    let staticMarkdown = _.find(pageList, function(item) {
      return item.node.fields.slug === self.pathslug + "/";
    });
    this.staticMarkDown = staticMarkdown;
    if(document.getElementById('devJourneyNavbarr').classList.contains('collapseJourney')){
      document.getElementsByClassName('devJourneyContent')[0].classList.toggle('djTransform');
    }
    else {
      document.getElementById('devJourneyNavbarr').classList.add('collapseJourney');
      document.getElementById('collapse-btn').classList.add('rotate-btn');
      document.getElementById('devJourneyRight-div').classList.toggle('devJourneyRi');
    }
    this.modalClick();
  };

  modalClick() {
    this.setState({
      modalIsOpen: true
    });
  }

  handleModalState(isModalOpen) {
    this.setState({
      modalIsOpen: isModalOpen
    });
  }

  closeModal() {
    this.setState({
      modalIsOpen: false
    });
  }

  journeyEnd() {
    if (typeof window !== 'undefined') {
      window.localStorage.removeItem('JOURNEY_VALUE');
      window.localStorage.removeItem('devJourneyJson');
    }
  }

  getNextStages(data) {
    window.localStorage.setItem("JOURNEY_VALUE", window.location.search);
    let self = this;
    let obj = {};
    if (data.processOrder) {
      var DevJson = this.props.devJourneyJson;
      let journeyIndex = DevJson.buildartifacts.map(function(e) { return e.artifactName.toLowerCase(); }).indexOf(data.artifactName.toLowerCase());
      let devJson = DevJson.buildartifacts[journeyIndex].stages;
      var processes = devJson[devJson.length - 1].processes;
      obj.limit = parseInt(processes[processes.length - 1].processOrder) + 1;
      let nextStage;
      devJson.map((stage)=>{
        stage.processes.map((item)=>{
          if (item.processOrder == parseInt(data.processOrder) + 1){
            nextStage = item
          }
        })
      })
      self.setState({
        hideNext: false
      });
      if (data.processOrder < obj.limit - 1){
        obj["nextStage"] = nextStage.ProcessTitle;
        obj["path"] = nextStage.path;
      } else {
        self.setState({
          hideNext: true
        });
      }
      return obj;
    } else {
      return;
    }
  }

  getBackStages(data) {
    let self = this;
    let obj = {};
    if (data.processOrder) {
      var DevJson = this.props.devJourneyJson;
      let journeyIndex = DevJson.buildartifacts.map(function(e) { return e.artifactName.toLowerCase(); }).indexOf(data.artifactName.toLowerCase());
      let devJson = DevJson.buildartifacts[journeyIndex].stages;
      var processes = devJson[devJson.length - 1].processes;
      obj.limit = parseInt(processes[processes.length - 1].processOrder) + 1;
      
      let previousStage;
      devJson.map((stage)=>{
        stage.processes.map((item)=>{
          if (item.processOrder == parseInt(data.processOrder) - 1){
            previousStage = item
          }
        })
      })

      self.setState({
        hideBack: false
      });

      
      if (data.processOrder >= 1){
        obj["previousStage"] = previousStage.ProcessTitle;
        obj["backpath"] = previousStage.path;
      } else {
          self.setState({
            hideBack: true
        });
      }
      return obj;
    } else {
      return;
    }
  }

  handleAnimation() {
    document
      .querySelector(".dummy-devJourneyRight")
      .classList.add("dummyDivAnimation");
    setTimeout(
      () =>
        document
          .querySelector(".dummy-devJourneyRight")
          .classList.remove("dummyDivAnimation"),
      505
    );
    document
      .querySelector(".devJourneyScreens")
      .classList.add("devScreeensAnimation");
    setTimeout(
      () =>
        document
          .querySelector(".devJourneyScreens")
          .classList.remove("devScreeensAnimation"),
      505
    );
  }

  handleBackAnimation() {
    document
      .querySelector(".dummy-devJourneyRight")
      .classList.add("dummyDivBackAnimation");
    setTimeout(
      () =>
        document
          .querySelector(".dummy-devJourneyRight")
          .classList.remove("dummyDivBackAnimation"),
      505
    );
    document
      .querySelector(".devJourneyScreens")
      .classList.add("devScreeensAnimation");
    setTimeout(
      () =>
        document
          .querySelector(".devJourneyScreens")
          .classList.remove("devScreeensAnimation"),
      505
    );
  }

  renderSteps() {
    let query = queryString.parse(window.location.search);
    query.processOrder = query.processOrder? query.processOrder : '0';

    let artifactName = query.artifactName;
    let processOrder = query.processOrder;
    return this.props.devJourneySteps.map((item, index) => {
      if (item.node.fields.slug.split('/').indexOf(artifactName) > -1 && processOrder == item.node.frontmatter.processOrder){
        return <div dangerouslySetInnerHTML={{ __html: item.node.html }} key={index}/>;
      } else {
        return null;
      }
    });
  }

  // Add keyboard action to move to next and previous steps
  // press -> to go to next, press <- to go back
  _handleKeyDown = (event) => {
    switch( event.keyCode ) {
        case 37:
            if(this.state.processOrder && !this.state.hideBack) {
            document.getElementsByClassName('backBtn')[0].click() }
            break;
        case 39:
            if(this.state.processOrder && !this.state.hideNext) {
            document.getElementsByClassName('nextBtn')[0].click() }
            break;
        default: 
            break;
    }
  }

  disableNav(e) {
    // function disable nav
    e.preventDefault();
    return false;
  }
  
  render() {
    return (
      <section
        className="devJourneyScreens"
        style={
          this.state.hideNext ? { marginTop: "0" } : { marginTop: "1.4rem" }
        }
      >
        <ModalFrame
          component={
            <div>
              <StaticBody staticData={this.staticMarkDown} />
            </div>
          }
          src={this.pathslug}
          showInFrame={this.showInFrame}
          showModal={this.state.modalIsOpen}
          changeModalState={this.handleModalState}
        />
        <article className="devJourneyComponentRight">
          <div className="dummy-devJourneyRight" />
          {/* {this.state.pageDetails.hasOwnProperty("heading") && ( */}
            <section
              className={
                !(this.state.processOrder && this.state.hideNext)
                  ? "devJourneyRight"
                  : "devJourneyRight devJourneyHeight"
              }
            >
              <div className="devJourneyMd">{this.renderSteps()}</div>

              <div className="devJourneyButtonsDiv">
                {this.state.processOrder && this.state.hideNext && (
                  <div className="allStagesCompleted">
                    This is the end of your Journey!
                    <Link to="/home">
                      <button className="doneBtn" onClick={this.journeyEnd}>Done</button>
                    </Link>
                  </div>
                )}
                {this.state.processOrder && !this.state.hideBack && (
                  <Link
                    to={this.state.backpath}
                    onClick={this.handleBackAnimation}
                    className={
                      this.state.hideNext ? "backBtn backBtnLast" : "backBtn"
                    }
                  >
                    <img src={arrow} alt="arrow" className="backBtnArrow" />{" "}
                    BACK
                  </Link>
                )}
                {this.state.processOrder && this.state.hideBack && <div />}
                {this.state.processOrder && !this.state.hideNext && (
                  <Link
                    to={this.state.path}
                    className="nextBtn"
                    onClick={this.handleAnimation}
                  >
                    NEXT STEP{" "}
                    <img src={arrow} alt="arrow" className="nextBtnArrow" />
                  </Link>
                )}
              </div>
            </section>
          {/* )} */}
        </article>
        {this.state.processOrder && !this.state.hideNext && (
          <div className="underlay screen1" />
        )}
        {this.state.processOrder && !this.state.hideNext && (
          <div className="underlay screen2" />
        )}
      </section>
    );
  }
}
