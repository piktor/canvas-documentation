import React, { Component } from 'react';
import queryString from "query-string";
//import DevJson from "../../../../../../json/devJourney.json";
import stage1 from "../../../../../../images/active-design1.svg";
import stage2 from "../../../../../../images/active-devtest1.svg";
import stage3 from "../../../../../../images/active-deploy1.svg";
import stage4 from '../../../../../../images/icon-test.svg';
import stage5 from "../../../../../../images/active-manage1.svg";
import stage1Disabled from "../../../../../../images/disabledDesign1.svg";
import stage2Disabled from "../../../../../../images/disabledDevtest1.svg";
import stage3Disabled from "../../../../../../images/disabledDeploy1.svg";
import stage4Disabled from "../../../../../../images/icon-test1.svg";
import stage5Disabled from "../../../../../../images/disabledManage1.svg";
import iconCompleted from "../../../../../../images/icon_Completed.svg";
import Completed from "../../../../../../images/icon-completed.svg";
import devPink from '../../../../../../images/dev-pink.svg';
import expand_icon from '../../../../../../images/icon_expand.svg';
import white_arrow from '../../../../../../images/arow_white.svg';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { Link } from "gatsby";
import './index.css';

export default class DevJourneyNavBar extends Component {

  constructor() {
    super();

    this.state = {
      containerIndex: undefined,
      subContainer: undefined,
      stageName: "",
      setProcessOrder: "",
      modal: false,
      expand: true,
      stagesOrder: {
        "design": 1,
        "develop": 2,
        "deploy": 3,
        "test": 4,
        "manage": 5
      },
      nextStep: ""
    }
    this.toggleStages = this.toggleStages.bind(this);
    this.toogleClick = this.toogleClick.bind(this);
    this.selectSubStage = this.selectSubStage.bind(this);
    this.toggle = this.toggle.bind(this);
    this.toggleEnd = this.toggleEnd.bind(this);
    this.getButton = this.getButton.bind(this);
    this.getClassName = this.getClassName.bind(this);
  }

  getButton = () => {
    if (typeof window !== 'undefined') {
      let str = window.location.pathname;
      if (!str.includes('devJourney')) {
        return <Link to={"/devJourney" + window.localStorage.getItem('JOURNEY_VALUE')} className="end-journey-pink">CONTINUE</Link>;
      }
    }
  }

  toggle() {
    this.setState(prevState => ({
      modal: !prevState.modal
    }));
  }
  toggleEnd() {
    this.setState(prevState => ({
      modal: !prevState.modal
    }));
    if (typeof window !== 'undefined') {
      window.localStorage.removeItem('JOURNEY_VALUE');
      window.localStorage.removeItem('devJourneyJson');
    }
    document.querySelector('.devJourneyNavEvent').style.display = "none";
  }

  componentDidMount() {
    let search = window.location.search;
    let query = queryString.parse(search);
    query.processOrder = query.processOrder? query.processOrder : '0';
    
    var DevJson = this.props.devJourneyJson;
    if (DevJson){
      let journeyIndex = DevJson.buildartifacts.map(function(e) { return e.artifactName.toLowerCase(); }).indexOf(query.artifactName.toLowerCase());
      let devJson = DevJson.buildartifacts[journeyIndex].stages;
      const stages = [];
      var stageName = '';
      var processTitle = '';
      var subContainer = undefined;
  
      if (typeof window !== 'undefined' && typeof localStorage !== 'undefined') {
        if (!query.artifactName){
          query = queryString.parse(localStorage.getItem("JOURNEY_VALUE"));
        }
        let artifactName = query.artifactName;
  
        if (artifactName && devJson){       
          const stagesData = this.props.devJourneyJson.buildartifacts[journeyIndex].stages;
          for(var i = 0; i < stagesData.length; i++){
            stages.push(stagesData[i].stageName.toLowerCase());
          };
          this.setState({
            pageDetails: devJson,
            stages: stages,
            journeyIndex: journeyIndex,
            artifactTitle:this.props.devJourneyJson.buildartifacts[journeyIndex].artifactTitle
          })
        }
      }
  
      devJson.map((stage)=>{
        stage.processes.map((item, index)=>{
          if (item.processOrder == parseInt(query.processOrder)) {
            this.state.stageName = stage.stageName;
            stageName = stage.stageName;
            processTitle = item.processTitle;
            subContainer = index;
            this.state.subContainer = index;
          }
        })
      })
  
      if (stages && stageName){
        if (stages.indexOf(stageName.toLowerCase()) > -1 && query.processOrder) {
          this.setState({
            containerIndex: stages.indexOf(stageName.toLowerCase()),
            subContainer: subContainer,
            search: search
          })
        } 
        else {
          // fix navbar animation issue
          let query = queryString.parse(localStorage.getItem("JOURNEY_VALUE"));
          if (stages.indexOf(stageName.toLowerCase()) > -1 && query.processOrder) {
            this.setState({
              containerIndex: stages.indexOf(stageName.toLowerCase()),
              subContainer: subContainer,
              search: search
            })
          }
        }
      }
    }
    
  }

  componentDidUpdate() {
    let query = queryString.parse(localStorage.getItem("JOURNEY_VALUE"));
    query.processOrder = query.processOrder? query.processOrder : '0';
    let search = window.location.search;
    //let queryObj = queryString.parse(search);

    var DevJson = this.props.devJourneyJson;
    if (DevJson){
      let journeyIndex = DevJson.buildartifacts.map(function(e) { return e.artifactName.toLowerCase(); }).indexOf(query.artifactName.toLowerCase());
      let devJson = DevJson.buildartifacts[journeyIndex].stages;
      const stages = [];
      var stageName = '';
      var processTitle = '';
      var subContainer = undefined;
  
      devJson.map((stage)=>{
        stage.processes.map((item, index)=>{
          if (item.processOrder == parseInt(query.processOrder)) {
            this.state.stageName = stage.stageName;
            stageName = stage.stageName;
            processTitle = item.processTitle;
            subContainer = index;
            this.state.subContainer = index;
          }
        })
      })
  
      if (typeof window !== 'undefined' && typeof localStorage !== 'undefined') {
        if (!query.artifactName){
          query = queryString.parse(localStorage.getItem("JOURNEY_VALUE"));
        }
        let artifactName = query.artifactName;
  
        if (artifactName && devJson){
          
          const stagesData = this.props.devJourneyJson.buildartifacts[journeyIndex].stages;
          for(var i = 0; i < stagesData.length; i++){
            stages.push(stagesData[i].stageName.toLowerCase());
          };
        }
      }
  
      if (stageName && stages){
        if ((stageName.toLowerCase() !== stageName.toLowerCase()) || ((query.processOrder !== this.state.setProcessOrder))) {
          let nextStep = localStorage.getItem("nextstepVal");
          if (parseInt(nextStep) < parseInt(query.processOrder)) {
            this.setState({ nextStep: query.processOrder })
          }
          if (stages.indexOf(stageName.toLowerCase()) > -1) {
            this.setState({
              containerIndex: stages.indexOf(stageName.toLowerCase()),
              subContainer: subContainer,
              search: search
            })
          }
          this.setState({ stageName: stageName, setProcessOrder: query.processOrder })
        }
      }
    } 
  }

  toggleStages(index) {
    if (this.state.containerIndex === index) {
      this.setState({ containerIndex: null })
    } else {
      this.setState({ containerIndex: index })
    }
  }

  selectSubStage(index) {
    this.setState({ subContainer: index });
    document.querySelector(".devJourneyScreens") && document.querySelector(".devJourneyScreens").classList.add("fadeInAnimation1");
    setTimeout(() => document.querySelector(".devJourneyScreens").classList.remove("fadeInAnimation1"), 1001);
    var closeBtn = document.getElementsByClassName('closebtn');
    if (closeBtn.length > 0){
      closeBtn[0].click();
    }
  }

  renderStages() {
    if (this.state.containerIndex >= 0){
      return this.state.pageDetails.map((item, index) => {
        let imageTab = null;
        if (this.state.stageName) {
          if (this.state.stagesOrder[item.stageName.toLowerCase()] < this.state.stagesOrder[this.state.stageName]) {
            imageTab = <img src={iconCompleted} alt="" className="stageIconContent" />
          } else {
            if (item.stageName === "Design") {
              imageTab = <img src={(this.state.stageName === (item.stageName.toLowerCase()) ? stage1 : stage1Disabled)} alt="" className="stageIconContent" />
            } else if (item.stageName === "DevTest") {
              imageTab = <img src={(this.state.stageName === (item.stageName.toLowerCase()) ? stage2 : stage2Disabled)} alt="" className="stageIconContent" />
            } else if (item.stageName === "Deploy") {
              imageTab = <img src={(this.state.stageName === (item.stageName.toLowerCase()) ? stage3 : stage3Disabled)} alt="" className="stageIconContent" />
            } else if (item.stageName === "Test") {
              imageTab = <img src={(this.state.stageName === (item.stageName.toLowerCase()) ? stage4 : stage4Disabled)} alt="" className="stageIconContent" />
            } else {
              imageTab = <img src={(this.state.stageName === (item.stageName.toLowerCase()) ? stage5 : stage5Disabled)} alt="" className="stageIconContent" />
            }
          }
        }
  
        return (
          <div className={"stages-container" + (this.state.containerIndex === index || this.state.containerIndex >= index ? " noContent" : "")} key={index}>
            <div className={"stages "} onClick={() => this.toggleStages(index)}>
              <div className="stageIcon" title={item.stageName}>
                {imageTab}
              </div>
              <div className="stageName">{item.stageName}</div>
            </div>
            <div className={"subStageContainer" + (this.state.containerIndex === index ? " show" : "")}>{this.renderSubStages(item.processes, item.stageName)}</div>
          </div>
        )
      })
    } 
    // else {
    //   setTimeout(()=>{
    //     this.renderStages();
    //   },50)
    // }

  }

  disableNav(e) {
    // function disable nav
    e.preventDefault();
    return false;
  }


  getClassName(item, index, stageName) {
    let defaultClassname = 'subStageName ', self = this;
    if (this.state.subContainer === index && stageName && self.state.stageName && stageName.toLowerCase() === this.state.stageName.toLowerCase()) {
      defaultClassname = defaultClassname + ' selected';
    }
    if (self.state.setProcessOrder !== "") {
      if (parseInt(item.processOrder) < parseInt(self.state.setProcessOrder)) {
        defaultClassname = defaultClassname + ' completedsubStageName';
      }
    }
    return defaultClassname;
    // "subStageName" + ((this.state.subContainer === index && stageName && self.state.stageName && stageName.toLowerCase() === this.state.stageName.toLowerCase() ? " selected" : ""))
  }


  renderSubStages(data, stageName) {
    let self = this;
    return data.map((item, index) => {
      let imageTab = null;
      let imageTab2 = null;
      let imagereder = null;
      // imageTab = <img src={completed} alt="" className="stageIconContent" />
      imageTab = <div className={"subStageNumber" + (this.state.subContainer === index && stageName && self.state.stageName && stageName.toLowerCase() === self.state.stageName.toLowerCase() ? " selectedBg" : "")}><div>{index + 1}</div></div>
      imageTab2 = <img className="completed" src={Completed} alt="completed" />
      if (self.state.setProcessOrder !== "") {
        if (parseInt(item.processOrder) < parseInt(self.state.setProcessOrder)) {
          imagereder = imageTab2;

          // document.querySelector('.navBarContainer').classList.add('navBarContainersubStage-magenta');
        } else {
          imagereder = imageTab;
        }
      }
      return (
        <Link key={index} to={item.path} onClick={() => this.selectSubStage(index)}>
          <div className="subStages" key={index}>
            <div className="subStageIcon subStageIcon-width" title={item.ProcessTitle}>
              {imagereder}
            </div>
            {/* {(this.state.nextStep > this.state.setProcessOrder) && (
             <Link onClick={e => this.disableNav(e)}>
             <div className={"substageName" + (this.state.subContainer === index && stageName.toLowerCase() === this.state.stageName.toLowerCase() ? " selected":"")}>{item.ProcessTitle}</div>
           </Link>
          )} */}
            {/* {(this.state.nextStep < this.state.setProcessOrder) && ( */}
            <div className={this.getClassName(item, index, stageName)}>{item.ProcessTitle}</div>
            {/* )} */}
          </div>
        </Link>
      )
    })
  }

  toogleClick = () => {
    if (document.getElementById('devJourneyNavb')) {
      // // document.getElementById('collapse-btn').classList.toggle('display-none-devBtn');
      document.getElementById('devJourneyNavb') && document.getElementById('devJourneyNavb').classList.remove('.collapseJourneyEvent');
      document.getElementById('devJourneyNavb').classList.toggle('collapseJourneyEvent');
    }
      document.getElementById('devJourneyNavbarr') && document.getElementById('devJourneyNavbarr').classList.toggle('collapseJourney');
      if (document.getElementsByClassName('devJourneyNav').length > 0){
        document.getElementsByClassName('devJourneyNav')[0].classList.toggle('nav-expanded');
      }
      if(document.getElementsByClassName('devJourneyContent')[0]) {
        document.getElementsByClassName('devJourneyContent')[0].classList.toggle('djTransform');
      }
      // document.getElementById('devJourneyNavb') && document.getElementById('devJourneyNavb').classList.toggle('collapseJourney');
      // document.getElementById('devJourneyRight-div') && document.getElementById('devJourneyRight-div').classList.toggle('devJourneyRi');
    
    // var staticModal = document.getElementsByClassName('Modal-static');
    // if (staticModal[0]) {
    //   staticModal[0].classList.toggle('modal-collapsed');
    // }
    if (window.location.pathname.indexOf(("/devJourney/") > -1)) {
      document.getElementById('collapse-btn').classList.toggle('rotate-btn');
    }
    this.setState({
      expand: !this.state.expand
    })
  }

    getPageDetails(){
      if (typeof window !== 'undefined' && typeof localStorage !== 'undefined') {
        let search = window.location.search;
        let query = queryString.parse(search);
        if (!query.artifactName){
          query = queryString.parse(localStorage.getItem("JOURNEY_VALUE"));
        }
        let artifactName = query.artifactName;

        if (artifactName && this.props.devJourneyJson){
          let journeyIndex = this.props.devJourneyJson.buildartifacts.map(function(e) { return e.artifactName.toLowerCase(); }).indexOf(artifactName.toLowerCase());
          const stages = [];
          const stagesData = this.props.devJourneyJson.buildartifacts[journeyIndex].stages;
          for(var i = 0; i < stagesData.length; i++){
            stages.push(stagesData[i].stageName.toLowerCase());
          };
          this.setState({
            pageDetails: this.props.devJourneyJson.buildartifacts[journeyIndex].stages,
            stages: stages,
            journeyIndex: journeyIndex,
            artifactTitle:this.props.devJourneyJson.buildartifacts[journeyIndex].artifactTitle
          })
        }
      }
    }

  render() {
    if (this.state.pageDetails){
      return (

        <div className="devJourneyNavbr">
          {(this.state.pageDetails.length !== 0) && (
            <div className="navBarContainer">
              <div className="devSide-minHeight">
                  <div className="devJourney-title">{this.state.artifactTitle}</div>
                <div className="devJourney-steps">
                  <div className="dev-jouney">DEV JOURNEY</div>
                  <div className="collapse-btn" id="collapse-btn" onClick={this.toogleClick}>
                    {/* <img src={(this.state.expand === true) ? collapse_icon : expand_icon} alt="expand-icon" /> */}
                    <img src={devPink} alt="expand-icon" />
                    <div className="white-arrow">
                      <img src={white_arrow} alt="white arrow"/>
                    </div>
                  </div>
                </div>
  
                <div className="stagesLineWrap">
                  <div className="stagesLine"></div>
                  <div className="allStagesWrap">
                    {this.renderStages()}
                  </div>
                </div>
              </div>
  
              <div className="devsidenav-buttons">
                {this.getButton()}
                <div className="end-journey" onClick={this.toggle}>END JOURNEY</div>
              </div>
  
              <div>
                {/* <Button color="danger" onClick={this.toggle}>{this.props.buttonLabel}</Button> */}
                <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
                  <ModalHeader toggle={this.toggle}>Are you sure?</ModalHeader>
                  <ModalBody>
                    This journey will be cleared if you select end journey.
                  </ModalBody>
                  <ModalFooter>
                    <Button className="cancel-journey" onClick={this.toggle}>CANCEL</Button>
                    {(typeof window !== 'undefined') && (window.location.pathname.includes('devJourney')) && (
                      <Link to="/devJourney" className="end-journey-btn" onClick={this.toggleEnd}>END JOURNEY</Link>)}
                    {(typeof window !== 'undefined') && (!window.location.pathname.includes('devJourney')) && (
                      <Link to={window.location.pathname + window.location.search} className="end-journey-btn" onClick={this.toggleEnd}>END JOURNEY</Link>)}
                  </ModalFooter>
                </Modal>
              </div>
  
              {/* <button type="button" class="btn btn-primary end-journey" data-toggle="modal" data-target="#exampleModalCenter">
                END JOURNEY
              </button>
  
  
              <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      ...
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      <button type="button" class="btn btn-primary">Save changes</button>
                    </div>
                  </div>
                </div>
              </div> */}
            </div>
          )}
        </div>
      )
    } 
    else {
      setTimeout(()=>{
        this.getPageDetails();
      },50)
      return <div></div>
    }
    
  }
}
