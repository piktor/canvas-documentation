import React, { Component } from "react";
import "./index.css";
import Modal from "react-modal";
import cross from "../../../../images/icon-clear@2x.png";

Modal.setAppElement(`#___gatsby`);

export default class modalComponent extends Component {
  spanClass = "";
  nameClass = "";

  iFrameTimeout = null;

  constructor(props) {
    super(props);
    // this.state = { renderIFrame: false };
    this.openModal = this.openModal.bind(this);
    this.afterOpenModal = this.afterOpenModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
  }
  // componentDidMount() {
  //   var iframe = document.getElementById("myFrame");
  //   var elmnt = iframe.contentWindow.document.getElementsByTagName("header")[0];
  //   elmnt.style.display = "none";
  // }
  openModal() {
    //this.setState({modalIsOpen: true});
    document.body.style.overflow = "hidden";
    this.props.changeModalState(true);
  }

  afterOpenModal() {
    // references are now sync'd and can be accessed.
    // this.subtitle.style.color = '#f00';
    //worked try css
    //document.body.style.overflow = 'hidden'
    // this.iFrameTimeout = setTimeout(() => {
    //   this.setState({ renderIFrame: true });
    // }, 1500);
  }

  closeModal() {
    //this.setState({modalIsOpen: false, renderIFrame: false});
    // this.setState({ renderIFrame: false });
    this.props.changeModalState(false);
    //navigate back to homepage route
    //this.props.history.push('/');
    // document.body.style.overflow = 'scroll'
    clearTimeout(this.iFrameTimeout);

    if(document.getElementById('collapse-btn').classList.contains('rotate-btn')){
      document.getElementById('collapse-btn').classList.toggle('rotate-btn');
    }
      else {
      }
    document.getElementById('devJourneyNavbarr').classList.remove('collapseJourney');
    // document.getElementById('collapse-btn').classList.remove('rotate-btn');
    document.getElementById('devJourneyRight-div').classList.remove('devJourneyRi');
  }

  render() {
    return (
      <section>
        <Modal
          isOpen={this.props.showModal}
          // onAfterOpen={this.afterOpenModal}
          onRequestClose={this.closeModal}
          className="Modal-static"
          overlayClassName="Overlay-static"
          contentLabel="Dev Journey Internal Component"
        >
        <div className="djModalheader">
          <button className="closebtn" onClick={this.closeModal}>
            <img src={cross} alt="close-button" className="closebtn-img" />
          </button>
        </div>

          {this.props.showInFrame ? (
            <iframe
              id="myFrame"
              src={this.props.src}
              style={{
                width: "100%",
                height: "100%",
                border: "none",
                padding: "1.5rem"
              }}
            />
          ) : (
            this.props.component
          )}
        </Modal>
      </section>
    );
  }
}
