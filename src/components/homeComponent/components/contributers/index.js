import React, { Component } from 'react';
import CardsContributers from "./components/cardContributers";
import './index.css';

export default class Contributers extends Component {
  render() {
    return (
        <section className="contributers">
        <div>
          <div>
            <span className="contributers-users">USERS</span>
            <div className="car-heading">Top Contributors</div>
            <div className="contributers-para-cont">
            <p className="contributers-para">We're proud of the people who contributed to this book. Here's what they
            have to day about the experience.</p>
            </div>   
            <div className="dot1"></div>
            <div className="dot2"></div>
            <div className="dot3"></div>
          </div>

          <div className="max-width">
           <CardsContributers className="contributer-cards"/>
          </div>
        </div>
        <div className="contributers-part2">
        </div>
      </section>
    )
  }
}
