import React, { Component } from 'react'
import { Link } from 'gatsby';
import './index.css';
import json from '../../../../../../../src/json/contributers.json';
import ProImg from '../../../../../../images/rectanglec@2x.png';
import img1 from '../../../../../../images/icon-quotation-mark-orange.svg';
import img2 from '../../../../../../images/icon-quotation-mark-red.svg';
import img3 from '../../../../../../images/icon-quotation-mark-brown.svg';



export default class CardsContributers extends Component{
  constructor(){
    super();
    this.renderImage = this.renderImage.bind(this);
  }

  renderImage(index){
    switch (index) {
      case 0: 
        return(img1);
        case 1: 
        return(img2);
        case 2: 
        return(img3);
      default: 
      return(img1);
    }
  }

  render(){
    return(
      json.map((item, index) => (
        <div className="card" key={index}>
          <div className="card-upper-half">
             <div className="card-imgs">
                 <div className="hexagon">
                  <img src={ProImg} className="hexagon-img" />
                  </div>
                  <div className="small-img">
                      <img src={this.renderImage(index)} alt="simg" />
                  </div>
             </div>
             
              <div className="card-content">
                  <h5>{item.name}</h5>
                  <p>{item.intro}</p>
              </div>
              <div className="card-btn-line"></div>
          </div>
    
          <div className="card-btn">
              <Link to="/" className="btn">{item.post}</Link>
          </div>
        </div>
        ))   
      )
  }
}