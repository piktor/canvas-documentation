import React, { Component } from 'react';
import email from '../../../../images/outline-email-24px.svg';
import slack from '../../../../images/Slack_Icon.png';
import "./index.css";

export default class Connect extends Component {
  render() {
    return (
    <section className="connect">
        <h3 className="connect_title">LET&apos;S TALK</h3>
        <h1 className="connect_subtitle">We work with you, collaboratively. 
Let’s start a new API project together. </h1>
        <h6 className="connect_channel">Get connect to our channel</h6>
        <div className="connect_btn_div">
          <a href="mailto:devcenter@t-mobile.com">
            <button type="button" className="connect_btn">
              <img src={email} alt="email" className="connect_btn_img" />
              <span>Email</span>
            </button>
          </a>
          <a href='https://t-mo.slack.com/messages/CG0E0JL8G/' target="_blank" rel="noopener noreferrer">
            <button rel="noopener noreferrer" type="button" className="connect_btn">
              <img src={slack} alt="slack" className="connect_btn_img" />
              <span>Slack</span>
            </button>
          </a>
        </div>
    </section>
    )
  }
}
