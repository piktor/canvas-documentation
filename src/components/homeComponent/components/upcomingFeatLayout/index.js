import React, { Component } from "react";
import "./index.css";
import ModalFrame from "../../../modalFrame";

export default class UpcomingFeatLayout extends Component {
  constructor() {
    super();
    this.state = {
      modalIsOpen: false,
      modalTitle: "",
      modalLink: "",
      modalImage: ""
    };
    this.modalClick = this.modalClick.bind(this);
    this.handleModalState = this.handleModalState.bind(this);
  }

  componentDidMount() {
    var isIE = false || !!document.documentMode;
    var isEdge = !isIE && !!window.StyleMedia;
    //Handling Clip part view for IE
    if (isIE || isEdge) {
      var cardview = document.getElementsByClassName("card-cont-box");
      var cardcont = document.getElementsByClassName("card-cont");

      for (var i = 0; i < cardview.length; i++) {
        cardview[i].style.filter = "none";
        cardcont[i].style.clipPath = "none";
        cardcont[i].style.boxShadow = "0 2px 4px 0 rgba(0, 0, 0, 0.15)";
      }
    }
  }

  disableNav(e) {
    // function disable nav
    e.preventDefault();
    return false;
  }

  tag(tags) {
    var collectAlltags = tags.split("#");
    collectAlltags.shift();
    return collectAlltags.map((item, i) => {
      return (
        <span key={parseInt(i.toString(), 10)} className="card-tags-span">
          {item}
        </span>
      );
    });
  }

  modalClick(frontmatter) {
    this.setState(prevState => {
      return {
        modalIsOpen: true,
        modalTitle: frontmatter.title,
        modalLink: frontmatter.link,
        modalImage: frontmatter.image
      };
    });
  }

  handleModalState(isModalOpen) {
    this.setState(prevState => {
      return { modalIsOpen: isModalOpen };
    });
  }

  cardContent(item) {
    return (
      <div className="card-content">
        <img
          src={require("../../../../pages/homedocs/upcoming/" +
            item.node.frontmatter.image)}
          alt="icon"
          className="card-icon"
        />
        {/* <div className="card-tags flex">{this.tag(item.node.frontmatter.tag)}</div>  Removed since tags are not needed on the features */}
        <h3 className="card-heading">{item.node.frontmatter.title}</h3>
        <p
          className="card-para"
          dangerouslySetInnerHTML={{ __html: item.node.html }}
        />
        <a href={item.node.frontmatter.link} target="_blank" rel="noopener noreferrer"
          // onClick={e => this.disableNav(e)}
          className="card-view-more position-bottom"
        >
          <span className="view-more-text">View more </span>
        </a>
      </div>
    );
  }

  card() {
    if (this.props.upCmgfeatData.length > 0) {
      return this.props.upCmgfeatData.map((item, index) => {
        return (
          <article className="card-cont-box" key={index}>
            {item.node.frontmatter.launchModal && (
              <div className="card-preventDefault" onClick={e => this.disableNav(e)}>
                 <a
                href="#/"
                onClick={() => this.modalClick(item.node.frontmatter)}
                rel="noopener noreferrer"
                key={parseInt(index.toString(), 10)}
                className="card-cont flex flex-col"
              >
                {this.cardContent(item)}
                
              </a>
              </div>
             
            )}

            {!item.node.frontmatter.launchModal && (
              <a
                href={item.node.frontmatter.link}
                target="_blank" rel="noopener noreferrer"
                key={parseInt(index.toString(), 10)}
                className="card-cont flex flex-col"
              >
                {this.cardContent(item)}
              </a>
            )}
          </article>
        );
      });
    }
  }

  render() {
    return (
      <main className="home-cards-section flex flex-wrap">
        <article className="upcoming-features-div flex flex-col">
          <div className="upcoming-heading-div flex">
            <h2 className="upcoming-heading">Developer Showcase</h2>
          </div>
          <p className="upcoming-content">
            Checkout the many ways developers can leverage our API Platform to
            create innovative experiences.
          </p>
          {/* <div className="card-view-more">
          <span className="view-more-text">Explore all features </span>
          <img src={vmarrow} alt="view-more-arrow" className="vm-arrow" />
        </div> */}
        </article>
        {this.card()}
        <ModalFrame
          showModal={this.state.modalIsOpen}
          name={this.state.modalTitle}
          modalUrl={this.state.modalLink}
          image={this.state.modalImage}
          imagePath="homedocs/upcoming/"
          changeModalState={this.handleModalState}
        />
      </main>
    );
  }
}
