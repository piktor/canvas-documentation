import React from "react";
import "./index.css";
import image from "../../../../images/about-us-rectangle.svg";

const About = () => (
  <section className="aboutUs-bg">
    <div className="homepage_about max-width">
      <div className="about_left">
        <h3 className="about_title">ABOUT</h3>
        <h1 className="about_subtitle">
          API Center for Enablement <br />
        </h1>
        <p className="about_text">
        provides frameworks, tools, best practices, support and training to accelerate API adoption. We've done the heavy lifting by creating a state of art API platform so that developers can focus on creating great customer experiences.
        </p>
      </div>
      <div className="about_right">
        <img src={image} alt="api_logo" className="api-logo-image" />
      </div>
    </div>
  </section>
);

export default About;
