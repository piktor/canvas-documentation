import React from 'react';
import './index.css';
import Link from 'gatsby-link';

const Journey = () => {
    return(
        <article className="journey">
            <div className="max-width">
            <div className="bg-img-lft"></div>
            <div className="content">
            <div className="txt-content">
               <p className="heading">Start Your Development Journey</p>
               <p className="para">Your Custom Walk Through of Design, Develop, Test, Deploy, and Manage Stages</p>
            </div>
            {typeof window !== 'undefined' && !window.localStorage.getItem('JOURNEY_VALUE') && (
            <Link to="/devJourney" className="btnn"><div className="btn-txt-journey">START YOUR JOURNEY</div></Link>
            )}
            {typeof window !== 'undefined' && window.localStorage.getItem('JOURNEY_VALUE') && (
                <Link to={"/devJourney"+window.localStorage.getItem('JOURNEY_VALUE')} className="btnn btnn2"><div className="btn-txt-journey">CONTINUE YOUR JOURNEY</div></Link>
            )}
            </div>
            <div className="bg-img-rgt"></div>
            </div>
        </article>
    )
}
export default Journey;