import React, { Component } from "react";
import Header from "../../components/header";
import SEO from "../../components/seo";
import _ from "lodash";
import UpcomingFeatLayout from "./components/upcomingFeatLayout";
import Carousel from "../../components/carousel";
import Journey from './components/journey';
import About from "./components/about";
import Connect from "./components/connect";
import Footer from "../../components/footer";
import introSteps from "../../json/intro.json";
import { Steps } from "intro.js-react";
import "intro.js/introjs.css";
// import collapse_icon from '../../images/icon_collapse.svg';
import expand_icon from '../../images/icon_expand.svg';
import white_arrow from '../../images/arow_white.svg';
import DevJourneyNavBar from '../devJourneyComponent/components/devJourneySteps/components/devJourneyNavBar';
import takeTour from '../../images/rectangle-copy_2019-04-16/take-tour.png';
// import devJourneyBtn from '../../images/rectangle-copy-2_2019-04-16/devJourneyBtn.png';
import "./index.css";

const stepLength = introSteps.steps.length;

export default class HomeComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filteredBlogs: [],
      filteredEvents: [],
      filterFeatured: [],
      filterUpCmgfeatData: [],
      stepsEnabled: false,
      initialStep: 0,
      steps: introSteps.steps,
      options: {
        skipLabel: 'Skip',
        nextLabel: 'Next',
        prevLabel: 'Back'
      },
      loadIntroModule: false
    }
    this.filterData = this.filterData.bind(this);
  }

  componentDidMount() {
    let self = this;
    var introJs = import('../../../node_modules/intro.js');
    introJs.then(introJs => {
      self.setState({
        loadIntroModule: true
      })
    })
    introJs.catch((error) => console.error(error));
    let getIntroSeenValue = window.localStorage.getItem("introSeen");
    if (getIntroSeenValue) {
      this.setState({ stepsEnabled: false });
    } else {
      this.setState({ stepsEnabled: true });
      window.localStorage.setItem("introSeen", true);
    }
    this.filterData();
  }

  filterData() {
    let getBlogsAndEventsData = this.props.data;
    let filteredBlogs = _.filter(getBlogsAndEventsData, {
      node: { frontmatter: { label: "blog" } }
    });
    let filteredEvents = _.filter(getBlogsAndEventsData, {
      node: { frontmatter: { label: "event" } }
    });
    let filterFeatured = _.filter(getBlogsAndEventsData, {
      node: { frontmatter: { label: "feature" } }
    });
    let filterUpCmgfeatData = _.sortBy(
      _.filter(getBlogsAndEventsData, {
        node: { frontmatter: { label: "upcomming" } }
      }),
      function (item) {
        return item.node.frontmatter.menuTitle;
      }
    );
    this.setState({
      filteredBlogs,
      filteredEvents,
      filterFeatured,
      filterUpCmgfeatData
    });
  }

  enableSite = () => {
    this.setState(() => ({ stepsEnabled: true }));
  };

  onBeforeChange = (nextStepIndex) => {
    if (nextStepIndex === (stepLength - 1)) {
      document.querySelector(".introjs-tooltipbuttons").classList.add("last-tour-tab");
    } else {
      document.querySelector(".introjs-tooltipbuttons").classList.remove("last-tour-tab");
    }
  }
  onComplete = () => { }
  onExit = () => {
    this.setState(() => ({ stepsEnabled: false }));
  };

  toogleClickEvent = () => {
    document.getElementById('devJourneyNavb').classList.toggle('collapseJourneyEvent');
    // document.getElementById('collapse-btn').classList.toggle('display-none-devBtn');
    document.getElementById('devJourneyNavb').classList.remove('.collapseJourneyEvent');

    if (document.getElementsByClassName('devJourneyNav').length > 0){
      document.getElementsByClassName('devJourneyNav')[0].classList.toggle('nav-expanded');
    }
    // document.getElementById('devJourney-tour').classList.toggle('devJourney-tour-posin')
    this.setState({
      expand: !this.state.expand
    })
  }

  render() {
    const { stepsEnabled, steps, initialStep, options } = this.state;
    return (
      <div>
        {(this.state.loadIntroModule) && (
          <Steps
            enabled={stepsEnabled}
            steps={steps}
            initialStep={initialStep}
            onBeforeChange={this.onBeforeChange}
            onComplete={this.onComplete}
            options={options}
            onExit={this.onExit}
          />
        )}
        <div className="dev-tours">
          {/* <div className="collapse-btn-event devJourney-tour" id="devJourney-tour" onClick={this.toogleClickEvent} style={(typeof window !== 'undefined' && window.localStorage.getItem('JOURNEY_VALUE')) ? { display: "block" } : { display: "none" }}>
          <img src={devJourneyBtn} alt="expand-icon" />
          <span class="tooltiptext">Continue your Journey</span>
        </div> */}

          <aside className="devJourneyNav">
            <div className="devJourneyNavbarr collapseJourney" id="devJourneyNavbarr" style={(typeof window !== 'undefined' && window.localStorage.getItem('JOURNEY_VALUE')) ? { display: "block" } : { display: "none" }}>
              <DevJourneyNavBar />
            </div>
          </aside>
          <div className="collapse-btn-event toolsdevJourney" title="Dev Journey" id="collapse-btn-event" onClick={this.toogleClickEvent} style={(typeof window !== 'undefined' && window.localStorage.getItem('JOURNEY_VALUE')) ? { display: "block" } : { display: "none" }}>
            <img src={expand_icon} alt="expand-icon" />
          </div>
          <aside className="devJourneyNavEvent devJourneyNavEvent-lr" id="devJourneyNavb" style={(typeof window !== 'undefined' && window.localStorage.getItem('JOURNEY_VALUE')) ? { display: "block" } : { display: "none" }} >
            <DevJourneyNavBar />
          </aside>

          <div className="takeTour" onClick={() => this.enableSite()}>
            <img src={takeTour} alt="Take Tour" />
            <span className="tooltiptext">Take a Tour</span>
          </div>
        </div>

        {/* <aside id="devJourneyNavb" className="devJourneyNavEvent devJourneyNavHome" >
          <DevJourneyLeft />
        </aside> */}

        <Header featuredData={this.state.filterFeatured} />
        <Journey />
        <SEO title="Dev Center" pathname="home" />
        <div className="bgcolor">
          <section className="max-width">
            <div className="main-section-home">
              <UpcomingFeatLayout
                className="main-card-section-home"
                upCmgfeatData={this.state.filterUpCmgfeatData}
              />
              <Carousel
                className="carousel-section-home"
                featuredData={this.state.filterFeatured}
                blogData={this.state.filteredBlogs}
                eventData={this.state.filteredEvents}
              />
            </div>
          </section>
        </div>
        {/* <Contributers /> */}
        <About />
        <Connect />
        {/* <UpperFooter /> */}
        <Footer />
      </div>
    );
  }
}
