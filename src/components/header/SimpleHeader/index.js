import React, { Component } from "react";
// import Link from "gatsby-link";
import CanvasIcon from "./logo-canvas-white.png";
import "./index.css";
import design from "../../../images/disabledDesign.svg";
import develop from "../../../images/disabledDevtest.svg";
import testDeploy from "../../../images/disabledDeploy.svg";
import manage from "../../../images/disabledManage.svg";
import arrow from "../../../images/disabled-combined-shape.svg";
import activedesign from "../../../images/active-design.svg";
import activedevelop from "../../../images/active-devtest.svg";
import activetestDeploy from "../../../images/active-deploy.svg";
import activemanage from "../../../images/active-manage.svg";
import { withAssetPrefix } from "gatsby";

export default class SimpleHeader extends Component {
  constructor() {
    super();
  }
  render() {
    return (<div className="simple-header-con">
        <div className="simple-header-wrap">
            <div className="sh-logo-con">
                <div className="sh-logo-wrap">
                <img className="canvas-icon" src={CanvasIcon} alt="Ace" />
                </div>
            </div>
            <div className="sh-header-title"></div>
        </div>
    </div>);
  }
}
