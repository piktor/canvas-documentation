import React, { Component } from "react";
import AppHeader from "../header/appHeader";
import Tooltip from "./components/tooltip";
import SimpleSliderCarosel from "../carousel/carosel";
import "./index.css";
import { Link } from "gatsby";
import arrowImg from "../../images/combined-shape.svg";
import design from "../../images/icon-design.svg";
import devtest from "../../images/icon-devtest.svg";
import deploy from "../../images/icon-deploy.svg";
import manage from "../../images/icon-manage.svg";
import disabledDesign from "../../images/disabledDesign.svg";
import disabledManage from "../../images/disabledManage.svg";
import disabledDeploy from "../../images/disabledDeploy.svg";
import disabledDevTest from "../../images/disabledDevtest.svg";
import disabledCombined from "../../images/disabled-combined-shape.svg";
import tooltipData from "../../json/sub-category.json";
import activedesign from "../../images/active-design.svg";
import activedevelop from "../../images/active-devtest.svg";
import activetestDeploy from "../../images/active-deploy.svg";
import activemanage from "../../images/active-manage.svg";
import hoverHexagon from "../../images/Hexagon_hover.svg";
import collapse_icon from '../../images/icon_collapse.svg';
import expand_icon from '../../images/icon_expand.svg';


const list = [
  {
    label: "DESIGN",
    src: design,
    altsrc: disabledDesign,
    activesrc: activedesign,
    link: "design",
    navigateLink: "/toolsdocs?section=design"
  },
  {
    label: "DEVELOP",
    src: devtest,
    altsrc: disabledDevTest,
    activesrc: activedevelop,
    link: "devtest",
    navigateLink: "/toolsdocs?section=develop"
  },
  {
    label: "TEST & DEPLOY",
    src: deploy,
    altsrc: disabledDeploy,
    activesrc: activetestDeploy,
    link: "deploy",
    navigateLink: "/toolsdocs?section=testDeploy"
  },
  {
    label: "MANAGE",
    src: manage,
    altsrc: disabledManage,
    activesrc: activemanage,
    link: "manage",
    navigateLink: "/toolsdocs?section=manage"
  }
];

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      toolTipData: tooltipData[0].links,
      blogData: []
    };
    this.onScroll = this.onScroll.bind(this);
    this.naviMouseEnter = this.naviMouseEnter.bind(this);
    this.naviMouseLeave = this.naviMouseLeave.bind(this);
    this.navStatus = false;
  }

  componentDidMount() {
    window.addEventListener("scroll", e => this.onScroll(e));
  }

  componentWillUnmount() {
    window.removeEventListener("scroll", e => this.onScroll(e));
  }

  onScroll = event => {
    if (
      event.currentTarget.location.pathname === "/home/" ||
      event.currentTarget.location.pathname === "/home" ||
      event.currentTarget.location.pathname === "/"
    ) {
      if (event.currentTarget.pageXOffset > 500) {
        if (event.currentTarget.pageYOffset > 400) {
          document
            .querySelector(".header-navigation")
            .classList.add("sticky-navigation");
          // apply disabled icons in sticky navigation
          document.querySelector(".design").src = disabledDesign;
          document.querySelector(".devtest").src = disabledDevTest;
          document.querySelector(".deploy").src = disabledDeploy;
          document.querySelector(".manage").src = disabledManage;
          document.querySelectorAll(".arrow-img")[0].src = disabledCombined;
          document.querySelectorAll(".arrow-img")[1].src = disabledCombined;
          document.querySelectorAll(".arrow-img")[2].src = disabledCombined;
          this.navStatus = true;
        } else {
          document
            .querySelector(".header-navigation")
            .classList.remove("sticky-navigation");
          // change sticky navigation icons
          document.querySelector(".design").src = activedesign;
          document.querySelector(".devtest").src = activedevelop;
          document.querySelector(".deploy").src = activetestDeploy;
          document.querySelector(".manage").src = activemanage;
          document.querySelectorAll(".arrow-img")[0].src = arrowImg;
          document.querySelectorAll(".arrow-img")[1].src = arrowImg;
          document.querySelectorAll(".arrow-img")[2].src = arrowImg;
          this.navStatus = false;
        }
      } else {
        if (event.currentTarget.pageYOffset > 630) {
          document
            .querySelector(".header-navigation")
            .classList.add("sticky-navigation");
          // apply disabled icons in sticky navigation
          document.querySelector(".design").src = disabledDesign;
          document.querySelector(".devtest").src = disabledDevTest;
          document.querySelector(".deploy").src = disabledDeploy;
          document.querySelector(".manage").src = disabledManage;
          document.querySelectorAll(".arrow-img")[0].src = disabledCombined;
          document.querySelectorAll(".arrow-img")[1].src = disabledCombined;
          document.querySelectorAll(".arrow-img")[2].src = disabledCombined;
          this.navStatus = true;
        } else {
          document
            .querySelector(".header-navigation")
            .classList.remove("sticky-navigation");
          // change sticky navigation icons
          document.querySelector(".design").src = activedesign;
          document.querySelector(".devtest").src = activedevelop;
          document.querySelector(".deploy").src = activetestDeploy;
          document.querySelector(".manage").src = activemanage;
          document.querySelectorAll(".arrow-img")[0].src = arrowImg;
          document.querySelectorAll(".arrow-img")[1].src = arrowImg;
          document.querySelectorAll(".arrow-img")[2].src = arrowImg;
          this.navStatus = false;
        }
      }
    }
  };

  naviMouseEnter(icon) {
    if (this.navStatus) {
      let activeIcon = list.find(function (item) {
        if (item.link === icon) return item.activesrc;
        return null;
      });
      document.getElementsByClassName(icon)[0].src = activeIcon.activesrc;
    } else {
      let activeIcon = list.find(function (item) {
        if (item.link === icon) return item.src;
        return null;
      });
      document.getElementsByClassName(icon)[0].src = activeIcon.src;
    }
  }

  naviMouseLeave(icon) {
    if (this.navStatus) {
      let activeIcon = list.find(function (item) {
        if (item.link === icon) return item.altsrc;
        return null;
      });
      document.getElementsByClassName(icon)[0].src = activeIcon.altsrc;
    } else {
      let activeIcon = list.find(function (item) {
        if (item.link === icon) return item.activesrc;
        return null;
      });
      document.getElementsByClassName(icon)[0].src = activeIcon.activesrc;
    }
  }

  renderList = () =>
    list.map((item, index) => {
      return (
        <Link to={item.navigateLink} className="renderList" key={index}>
          <Tooltip
            data={this.state.toolTipData}
            indexVal={index}
            broadCategory={item.navigateLink}
          />
          <figure
            // onMouseLeave={this.hideTooltip}
            // onMouseEnter={e => this.showTooltip(index)}
            key={index}
          >
            <div className="nav-subactive-background">
              <img
                src={hoverHexagon}
                alt="hoverHexagon"
                className="hoverHexagonOuter"
              />
            </div>
            <div className="nav-active-background">
              <img
                src={hoverHexagon}
                alt="hoverHexagon"
                className="hoverHexagon"
              />
            </div>
            <img
              src={item.activesrc}
              alt="design-icon"
              className={"header-nav-icon " + item.link}
              onMouseEnter={() => this.naviMouseEnter(item.link)}
              onMouseLeave={() => this.naviMouseLeave(item.link)}
            />
            <figcaption
              className="nav-caption"
              onMouseEnter={() => this.naviMouseEnter(item.link)}
              onMouseLeave={() => this.naviMouseLeave(item.link)}
            >
              {item.label}
            </figcaption>
          </figure>
          {index < list.length - 1 && (
            <img src={arrowImg} alt="arrow" className="arrow-img" />
          )}
        </Link>
      );
    });

  showTooltip = index => {
    // Change data inside tooltip
    this.setState({
      toolTipData: tooltipData[index].links
    });

    // Dont show tooltip if nav is sticky
    if (
      document
        .querySelector(".header-navigation")
        .classList.contains("nav-sticky")
    ) {
      return;
    }
    document.querySelector(".api-lifecycle").style.opacity = "0";
    document.querySelector(".header-blog-div").style.opacity = "0";
    const tooltip = document.querySelectorAll(".tooltip-container");
    // tooltip[index].style.display = "flex";
    tooltip[index].style.bottom = "5em";
    tooltip[index].style.visibility = "visible";
  };

  hideTooltip = () => {
    const tooltip = document.querySelectorAll(".tooltip-container");
    tooltip[0].style.visibility = "hidden";
    tooltip[1].style.visibility = "hidden";
    tooltip[2].style.visibility = "hidden";
    tooltip[3].style.visibility = "hidden";
    tooltip[0].style.bottom = "14.5rem";
    tooltip[1].style.bottom = "14.5rem";
    tooltip[2].style.bottom = "14.5rem";
    tooltip[3].style.bottom = "14.5rem";

    document.querySelector(".api-lifecycle").style.opacity = "1";
    document.querySelector(".header-blog-div").style.opacity = "1";
  };

  render() {
    return (
      <section className="home-header-bg">
        <div className="home-header-bg-image">
          <AppHeader />
          <div className="home-header">
            <div className="add-header-top max-width">
              <div className="one-stop-container">
                <h1 className="one-stop">
                  <span className="one-stop-text">Dev Center Provides</span>
                  <span className="one-stop-list-div">
                    <ul className="one-stop-list">
                      <li className="one-stop-list-item">Training</li>
                      <li className="one-stop-list-item">Frameworks</li>
                      <li className="one-stop-list-item">Tools</li>
                      <li className="one-stop-list-item">Best Practices</li>
                      <li className="one-stop-list-item">Support</li>
                    </ul>
                  </span>
                </h1>
              </div>
              
              <h2 className="welcome-to-tmobile">
                to accelerate API adoption{" "}
              </h2>
              <div className="header-blog-div">
                <div className="header-blog-div-overlay" />
                <div className="header-blog-div-content">
                  <SimpleSliderCarosel
                    itemList={this.props.featuredData}
                    infinate="true"
                    arrows="true"
                    dots="false"
                    intervel="1000"
                    componentName="FeatureSliderComponent" />
                </div>
              </div>
              <h4 className="api-lifecycle">Development Lifecycle Stages</h4>
              <nav className="header-navigation forTransform">
                {this.renderList()}
              </nav>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default Header;
