import React, { Component } from "react";
import Link from "gatsby-link";
import "./index.css";

/* eslint-disable consistent-return */
class Tooltip extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: []
    };
  }

  componentWillReceiveProps(nextProps) {
    this.props = nextProps;
    this.setState({
      data: this.props.data
    });
  }

  renderElement = () => {
    const { data } = this.state;
    if (data) {
      return data.map((item, index) => {
        return (
          <Link to={(this.props.broadCategory + "&tag=" + item)} className="tooltip-element" key={index}>
            <p>{item}</p>
          </Link>
        );
      });
    }
  };
  /* eslint-enable consistent-return */

  toolTipStatus = (status, indexVal) => {
    const tooltip = document.querySelectorAll(".tooltip-container");
    if (status) {
      tooltip[indexVal].style.visibility = "visible";
      tooltip[indexVal].style.bottom = "5em";
      document.querySelector(".api-lifecycle").style.opacity = "0";
      document.querySelector(".header-blog-div").style.opacity = "0";
    } else {
      tooltip[indexVal].style.visibility = "hidden";
      tooltip[indexVal].style.bottom = "14.5rem";
      document.querySelector(".api-lifecycle").style.opacity = "1";
      document.querySelector(".header-blog-div").style.opacity = "1";
    }
  };

  render() {
    return (
      <div
        className="tooltip-container"
        onMouseEnter={() => this.toolTipStatus(1, this.props.indexVal)}
        onMouseLeave={() => this.toolTipStatus(0, this.props.indexVal)}
      >
        {this.renderElement()}
        <div className="extraTooltipHeight" />
      </div>
    );
  }
}

export default Tooltip;
