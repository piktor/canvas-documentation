import React, { Component } from "react";
import Link from "gatsby-link";
import "./index.css";
import design from "../../../images/disabledDesign.svg";
import develop from "../../../images/disabledDevtest.svg";
import testDeploy from "../../../images/disabledDeploy.svg";
import manage from "../../../images/disabledManage.svg";
import arrow from "../../../images/disabled-combined-shape.svg";
import activedesign from "../../../images/active-design.svg";
import activedevelop from "../../../images/active-devtest.svg";
import activetestDeploy from "../../../images/active-deploy.svg";
import activemanage from "../../../images/active-manage.svg";

const list = [
  {
    label: "DESIGN",
    src: design,
    link: "design",
    altsrc: activedesign
  },
  {
    label: "DEVELOP",
    src: develop,
    link: "develop",
    altsrc: activedevelop
  },
  {
    label: "TEST & DEPLOY",
    src: testDeploy,
    link: "testDeploy",
    altsrc: activetestDeploy
  },
  {
    label: "MANAGE",
    src: manage,
    link: "manage",
    altsrc: activemanage
  }
];

export default class NavHeader extends Component {
  constructor() {
    super();
    this.state = {
      activePage: "design"
    };
    this.getNavImage = this.getNavImage.bind(this);
    this.navIconMouseEnter = this.navIconMouseEnter.bind(this);
    this.navIconMouseLeave = this.navIconMouseLeave.bind(this);
  }

  componentDidMount() {
    let activePage = window.location.search.split("?section=")[1];
    if (activePage && activePage.includes("&tag=")) {
      activePage = activePage.split("&tag=")[0];
      if (
        activePage === "design" ||
        activePage === "develop" ||
        activePage === "testDeploy" ||
        activePage === "manage"
      ) {
        this.setState({
          activePage
        });
      }
    } else {
      if (
        activePage === "design" ||
        activePage === "develop" ||
        activePage === "testDeploy" ||
        activePage === "manage"
      ) {
        this.setState({
          activePage
        });
      } else {
        activePage = "design";
        this.setState({
          activePage
        });
      }
    }
  }
  componentDidUpdate(prevProps) {
    let activePage = window.location.search.split("?section=")[1];
    if (activePage !== this.state.activePage) {
      if (
        activePage === "design" ||
        activePage === "develop" ||
        activePage === "testDeploy" ||
        activePage === "manage"
      ) {
        this.setState({
          activePage
        });
      }
    }
  }

  getNavImage(icon) {
    if (icon === "design") {
      if (this.state.activePage === "design") return activedesign;
      else return design;
    } else if (icon === "develop") {
      if (this.state.activePage === "develop") return activedevelop;
      else return develop;
    } else if (icon === "testDeploy") {
      if (this.state.activePage === "testDeploy") return activetestDeploy;
      else return testDeploy;
    } else if (icon === "manage") {
      if (this.state.activePage === "manage") return activemanage;
      else return manage;
    }
  }

  navIconMouseEnter(icon) {
    if (icon !== this.state.activePage) {
      let activeIcon = list.find(function(item) {
        if (item.link === icon) return item.altsrc;
        return null;
      });
      document.getElementsByClassName(icon)[0].src = activeIcon.altsrc;
    }
  }

  navIconMouseLeave(icon) {
    if (icon !== this.state.activePage) {
      let activeIcon = list.find(function(item) {
        if (item.link === icon) return item.src;
        return  null;
      });
      document.getElementsByClassName(icon)[0].src = activeIcon.src;
    }
  }

  renderList = () =>
    list.map((item, index) => {
      return (
        <div className="renderList-navHeader" key={index}>
          <Link
            to={"/toolsdocs?section=" + item.link}
            // onMouseEnter={() => this.navIconMouseEnter(item.link)}
            // onMouseLeave={() => this.navIconMouseLeave(item.link)}
          >
            <figure className="figure-navHeader">
              <img
                src={this.getNavImage(item.link)}
                alt="nav-icon"
                className={"navicon-navHeader " + item.link}
              />
              <figcaption>{item.label}</figcaption>
            </figure>
          </Link>
          {index < list.length - 1 && (
            <img src={arrow} alt="arrow" className="arrow-navHeader" />
          )}
        </div>
      );
    });

  render() {
    return <nav className="navHeader">{this.renderList()}</nav>;
  }
}
