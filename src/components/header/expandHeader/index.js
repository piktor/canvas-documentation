import React, { Component } from "react";
import { Link } from "gatsby";
import logo from "../../../images/dev-center@3x.png";
import burger from '../../../images/burger-menu-icon.svg'
import cross from '../../../images/close-icon.svg';
import docsTools from "../../../images/icon-docs-tools.svg";
import journey from "../../../images/icon-devjourney.svg";
import calender from "../../../images/newicon-calendar.svg";
import search from '../../../images/newicon-search.svg';
import accountCircle from '../../../images/account-circle.svg';

import "./index.css";

export default class expandHeader extends Component {

  componentDidMount() {
    if(window.location.href.indexOf("/toolsdocs") > -1) {
      if (document.getElementById('docAndtools-id')){
        document.getElementById('docAndtools-id').classList.add('highlight');
      }
    } else if(window.location.href.indexOf("/devJourney") > -1) {
      if (document.getElementById('devJourney-id')){
        document.getElementById('devJourney-id').classList.add('highlight');
      }
    }
    else if(window.location.href.indexOf("/events") > -1) {
      document.getElementById('events-id').classList.add('highlight');
    }
  }

  componentWillUnmount() {

    if(window.location.href.indexOf("/toolsdocs") > -1) {
      document.getElementById('docAndtools-id').classList.add('highlight');
    } else if(window.location.href.indexOf("/devJourney") > -1) {
      if (document.getElementById('devJourney-id')){
        document.getElementById('devJourney-id').classList.add('highlight');
      }
    }
    else if(window.location.href.indexOf("/events") > -1){
      document.getElementById('events-id').classList.add('highlight');
    }
  }


  render() {
    return (
      <header className="header-navv">
        <div className="to-flex  max-width-header">

          <div className="header-burger" onClick={() => this.props.toggleNavVal()}>
            <img className="burger-img" src={(this.props.toggleNav) ? cross : burger} alt="burger-img" />
          </div>

          <div className="navbar-header">
            <Link className="navbar-brand" to="/home">
              <img className="logo-new" alt="Brand" src={logo} />
            </Link>
          </div>

          <div className="right-flexx header-right displayNone">
            <div className="header-icons">
              <Link to="/toolsdocs?section=design" className="link home-img" id="docAndtools-id">
                <figure className="header-fig">
                  <img alt="home-icon" src={docsTools} className="header-icon" />
                  <figcaption>Docs &amp; Tools</figcaption>
                </figure>
              </Link>
              {typeof window !== 'undefined' && !window.localStorage.getItem('JOURNEY_VALUE') && (
                <Link to={"/devJourney"} className="link feed-icon" id="devJourney-id" >
                  <figure className="header-fig header-fig-journey">
                    <img alt="feed-icon"
                      src={journey}
                    className="header-icon home-icon" />
                    <figcaption>Dev Journey</figcaption>
                  </figure>
                </Link>
              )}
              {typeof window !== 'undefined' && window.localStorage.getItem('JOURNEY_VALUE') && (
                <Link to={"/devJourney"+window.localStorage.getItem('JOURNEY_VALUE')} className="link feed-icon" id="devJourney-id">
                  <figure className="header-fig header-fig-journey">
                  <span><img alt="feed-icon"
                    src={journey}
                  className="header-icon home-icon" /> <strong className="feed-icon-green"></strong></span>
                    <figcaption>Dev Journey</figcaption>
                  </figure>
                </Link>
              )}
              <Link
                to="/events"
                className="link"
                id="events-id"
              >
                <figure className="header-fig">
                  <img alt="calender-icon"
                    src={calender}
                    className="header-icon" />
                  <figcaption>Events</figcaption>
                </figure>
              </Link>
              {/* <Link
                to="/"
                className="link disabledLink search-header"
                title="Coming Soon"
              >
                <img
                  alt="calender-icon"
                  src={search}
                  className="header-icon"
                />
              </Link>
              <Link
                to="/ "
                className="link disabledLink user-pic"
                title="Coming Soon"
              > */}
                {/* <div className="user-details">
                  <div className="user-name">John Smith</div>
                  <div className="user-post">Sr Developer</div>
                </div> */}
                {/* <img
                  alt="calender-icon"
                  src={accountCircle}
                  className="header-icon"
                />
              </Link> */}
            </div>

            {/* <div className="search-rectangle" title="Coming Soon">
              <img alt="search" src={search} className="search-icon" />
              <input
                type="text"
                className="search-text header-search"
                placeholder="Search"
                disabled
              />
            </div>
            <div className="login-btn-hover">
              <div class="login-rectangle" title="Coming Soon">
                <span class="login-text" title="Coming Soon">
                  Login
                </span>
              </div>
            </div> */}
          </div>
        </div>
      </header>
    );
  }
}
