import React, { Component } from 'react'
import "./index.css"

export default class Footer extends Component {
    render() {
        return (
            <footer className="footer">
                <div className="content max-width">
                    <div className="copyright">
                        &copy; 2019 All rights reserved
                    </div>
                </div>
            </footer>
        )
    }
}