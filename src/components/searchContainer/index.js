import React, { Component } from "react";
import * as JsSearch from "js-search";
import "./index.css";
import dummy from "../../images/dummy_icon.png";

class SearchContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      searchResults: [],
      search: null,
      isError: false,
      indexByTitle: false,
      indexByDetail: false,
      termFrequency: true,
      removeStopWords: false,
      searchQuery: "",
      searchTermOnLoadPresent: props.searchTerm ? true : false,
      selectedStrategy: "",
      selectedSanitizer: "",
    }
  }

  /**
   * React lifecycle method that will inject the data into the state.
   */
  static getDerivedStateFromProps(nextProps, prevState) {
    if (prevState.search === null) {
      const { engine } = nextProps
      return {
        indexByTitle: engine.TitleIndex,
        indexByDetail: engine.DetailIndex,
        termFrequency: engine.SearchByTerm,
        selectedSanitizer: engine.searchSanitizer,
        selectedStrategy: engine.indexStrategy,
      }
    }
    return null
  }
  async componentDidMount() {
    await this.rebuildIndex();    
    if(this.state.searchTermOnLoadPresent) {
      this.setState({ searchQuery: this.props.searchTerm })
      this.searchData();
    }
  }

  /**
   * rebuilds the overall index based on the options
   */
  rebuildIndex = () => {
    const {
      selectedStrategy,
      selectedSanitizer,
      removeStopWords,
      termFrequency,
      indexByTitle,
      indexByDetail,
    } = this.state
    const { data } = this.props

    const dataToSearch = new JsSearch.Search("title")

    if (removeStopWords) {
      dataToSearch.tokenizer = new JsSearch.StopWordsTokenizer(
        dataToSearch.tokenizer
      )
    }
    /**
     * defines an indexing strategy for the data
     * read more about it here https://github.com/bvaughn/js-search#configuring-the-index-strategy
     */
    if (selectedStrategy === "All") {
      dataToSearch.indexStrategy = new JsSearch.AllSubstringsIndexStrategy()
    }
    if (selectedStrategy === "Exact match") {
      dataToSearch.indexStrategy = new JsSearch.ExactWordIndexStrategy()
    }
    if (selectedStrategy === "Prefix match") {
      dataToSearch.indexStrategy = new JsSearch.PrefixIndexStrategy()
    }

    /**
     * defines the sanitizer for the search
     * to prevent some of the words from being excluded
     */
    selectedSanitizer === "Case Sensitive"
      ? (dataToSearch.sanitizer = new JsSearch.CaseSensitiveSanitizer())
      : (dataToSearch.sanitizer = new JsSearch.LowerCaseSanitizer())
    termFrequency === true
      ? (dataToSearch.searchIndex = new JsSearch.TfIdfSearchIndex("title"))
      : (dataToSearch.searchIndex = new JsSearch.UnorderedSearchIndex())

    // sets the index attribute for the data
    if (indexByTitle) {
      dataToSearch.addIndex("title");
      dataToSearch.addIndex("blogTitle");
      dataToSearch.addIndex("menuTitle");
    }
    // sets the index attribute for the data
    // if (indexByDetail) {
    //   dataToSearch.addIndex("html")
    // }

    dataToSearch.addDocuments(data) // adds the data to be searched

    this.setState({ search: dataToSearch, isLoading: false })
  }
  /**
   * handles the input change and perfom a search with js-search
   * in which the results will be added to the state
   */
  searchData = () => {
    const searchTerm = document.getElementById('Search').value;
    const { search } = this.state;
    const queryResult = search.search(searchTerm);
    this.setState({ searchQuery: searchTerm, searchResults: queryResult })
  }

  handleSubmit = e => {
    e.preventDefault()
  }
  render() {
    const { searchResults, searchQuery } = this.state
    const { data } = this.props
    const queryResults = searchQuery === "" ? data : searchResults
    return (
        <main className="search-container">
         <div className="search-suggestion-box">
            <section className="search-navigation">
                  <div className="filter-column">
                    <div className="content-type">Content-type</div>           
                    <div className="content-type">Technology</div>
                    <div className="content-type">Frame-work</div>
                    <div className="content-type">Life Cycle</div>
                </div>                        
            </section>
            <section  className="search-suggestion-list">
                <div className="search-bar-menu">
                    <div className="search-res-info"><span>Showing results for</span><span> {queryResults.length} Results found</span></div>
                    <div className="fake-search-div">
                        <form onSubmit={this.handleSubmit}>
                            <input 
                                className="input-search"
                                id="Search"
                                value={searchQuery}
                                onChange={this.searchData}
                                placeholder="Search here......."
                                autoComplete="off"/>
                            <input className="dummy-search-icon" type="submit" />
                        </form>     
                    </div>
                </div>
                {queryResults.map((item, index) => {
                    return (
                        <div className="suggestion-row" key={index}>
                            <div className="suggestion-icon-flex">
                                <img src={dummy} className="img-icon"/>
                            </div>
                            <div className="sugstion-discription-box">
                                <div className="suggestion-header">
                                    {(item.blog) &&
                                        (item.blogTitle)
                                    }
                                    {(item.event || item.classification === "tools") &&
                                        (item.menuTitle)
                                    }
                                    {(item.classification === "documents") &&
                                        (item.title)
                                    }
                                    {(!item.blog && !item.event && item.classification!=="tools" && item.classification!=="documents") &&
                                        (item.menuTitle)
                                    }     
                                </div>
                                <div className="sugstion-discription">
                                    <p>
                                        Eiusmod pariatur irure consequat eu pariatur in mollit enim. Laborum ullamco mollit exercitation fugiat laborum. Reprehenderit mollit excepteur dolor eu qui pariatur tempor pariatur.
                                    </p>
                                </div>
                            </div>
                        </div>
                    )
                })}
                    
                  </section>
              </div>
        </main>
    )
  }
}
export default SearchContainer;