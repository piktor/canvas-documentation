import React, { Component } from 'react';
import './index.css';

export default class TryItOutCarouselComponent extends Component {
  render() {
    return (
      <div className="right-side-carousul-component">
        <div className="carousel-element">
          <div className="caroselTextWrapper">{this.props.item}</div>
        </div>
      </div>
    );
  }
}
