import React, { Component } from 'react';
import { Link } from "gatsby";
import './index.css';

export default class FeatureSliderComponent extends Component {
  render() {
    return (
      <div className="header-blog-inner-content">
         
                    <div className="header-blog-icon">
                      <div className="header-blog-icon-img">
                        <span className="blog-btn-txt capitalize">
                          {this.props.item.node.frontmatter.label}
                        </span>
                      </div>
                    </div>
                    <h5 className="rest-api">
                      {this.props.item.node.frontmatter.title}
                    </h5>
                    <p
                      className="header-blog-para"
                      dangerouslySetInnerHTML={{
                        __html: this.props.item.node.html
                      }}
                    />
                    <Link
                      to={this.props.item.node.frontmatter.link}
                      className="header-read-more"
                    >
                    <span>
                    Read More
                    </span>
                    </Link>
      </div>
    )
  }
}
