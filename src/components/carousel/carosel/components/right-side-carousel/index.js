import React, { Component } from "react";
import { Link } from "gatsby";
import "./index.css";

export default class ContributerSliderComponent extends Component {
  render() {
    return (
      <div>
        {this.props.item.node.frontmatter.draft===false && (
          <a href={this.props.item.link} target="_blank" rel="noopener noreferrer">
          <div className="right-side-carousul-component clipPolygon">
            <div className="carosal-content">
              <div className="right-carosal-img">
                <img
                  className="car-img"
                  alt="img"
                  src={require(`../../../../../pages/homedocs/blogs/${
                    this.props.item.node.frontmatter.image
                  }`)}
                />
                <div className="gradient" />
              </div>
              <Link
                className="blog-btnn"
                to={this.props.item.node.frontmatter.link}
              >
                {this.props.item.node.frontmatter.label}
              </Link>
              <div className="right-title">
                {this.props.item.node.frontmatter.title}
              </div>
            </div>
          </div>
        </a>
        )}
      </div>
    );
  }
}
