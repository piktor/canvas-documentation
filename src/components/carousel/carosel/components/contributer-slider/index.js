import React, { Component } from 'react';
import contributor from '../../../../../images/oval@3x.png';
import './index.css';

export default class ContributerSliderComponent extends Component {
  render() {
    return (
      <div className="contributorSlider">
        <div className="contributorSliderContent">
          <img className="contributor-img" src={contributor} alt="contributor" />
          <div className="right-content">
            <div className="contributor-profile">
              <div className="contributor-name">{this.props.item.name}</div>
              <div className="contributor-post">{this.props.item.post}</div>
            </div>
            <div className="contributor-desc">{this.props.item.intro}</div>
            <div className="cont-readmore-txt">Read more</div>
          </div>
        </div>
      </div>
    );
  }
}
