import React, { Component } from "react";
import Slider from "react-slick";
import ContributerSliderComponent from "./components/contributer-slider";
import RightSideCarouselComponent from "./components/right-side-carousel";
import TryItOutCarouselComponent from "./components/try-itout-carosel";
import FeatureSliderComponent from "./components/feature-carousel";

import "./index.css";

export default class SimpleSliderCarosel extends Component {
  constructor() {
    super();
    this.state = {
      settings: {}
    };
  }

  componentDidMount() {
    const settings = {
      arrows: this.props.arrows !== "false",
      dots: this.props.dots !== "false",
      pauseOnHover: true,
      infinite: this.props.infinate !== "false",
      speed: (this.props.intervel)? parseInt(this.props.intervel): 500,
      autoplay: true,
      fade: false,
      variableWidth: false,
      slidesToShow: 1,
      slidesToScroll: 1
    };
    this.setState({ settings });
  }

  render() {
    if (this.props.itemList && this.props.itemList.length > 0) {
      return (
        <div>
          <Slider {...this.state.settings}>
            {this.props.itemList.map((i, index) => {
              if (this.props.componentName === "ContributerSliderComponent") {
                return (
                  <div key={parseInt(index, 10)}>
                    <ContributerSliderComponent item={i} />
                  </div>
                );
              }
              if (this.props.componentName === "RightSideCarouselComponent") {
                return (
                  <div key={parseInt(index, 10)}>
                    <RightSideCarouselComponent item={i} />
                  </div>
                );
              }
              if (this.props.componentName === "TryItOutCarouselComponent") {
                return (
                  <div key={parseInt(index, 10)}>
                    <TryItOutCarouselComponent item={i} />
                  </div>
                );
              }
              if (this.props.componentName === "FeatureSliderComponent") {
                return (
                  <div key={parseInt(index, 10)}>
                    <FeatureSliderComponent item={i} />
                  </div>
                );
              }
            })}
          </Slider>
        </div>
      );
      /* eslint-disable consistent-return */
      /* eslint-enable array-callback-return */
    }
    return <div />;
  }
}