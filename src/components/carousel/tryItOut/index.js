import React, { Component } from "react";
import SimpleSliderCarosel from "../carosel";
import List from "../../../json/try-it-out.json";
import arrow from "../../../images/baseline-arrow_forward_ios-24px.svg";
import showMoreArrow from "../../../images/icon-arrow-down.svg";
import "./index.css";

let count = 0;

export default class TryITOut extends Component {
  /* eslint-disable no-plusplus */
  toggleShow = () => {
    document.querySelector(".show-more-contents").classList.toggle("visible");
    document
      .querySelector(".try-out-show__heading")
      .classList.toggle("visible");
      

    if (count % 2 === 0) {
      document.querySelector(".try-out-show-container").style.transform =
        "translateY(-7em)";
      document.querySelector(".show-button").innerHTML =
        "Show less <img src=" + showMoreArrow + ' class="showLessArrow" />';
        document.querySelector('.translaterequiredClass').style.transform = 'translateY(-7em)';
    } else {
      document.querySelector(".try-out-show-container").style.transform =
        "translateY(0px)";
        document.querySelector('.translaterequiredClass').style.transform = 'translateY(0px)';
      document.querySelector(".show-button").innerHTML =
        "Show more <img src=" + showMoreArrow + ' class="showMoreArrow" />';
    }
    count++;
  };
  /* eslint-enable no-plusplus */

  renderList = () => {
    return List.design.links.map((item, index) => {
      return (
        <div className="show-more-contents__element" key={index}>
          <img alt="arrow" src={arrow} />
          <div>{item}</div>
        </div>
      );
    });
  };

  render() {
    return (
      <div className="rightSideCarousel">
        <div className="TryITOutSlider">
          <div className="try-out-show__heading">try this Out</div>
          <SimpleSliderCarosel
            itemList={List.design.description}
            infinate="true"
            arrows="false"
            dots="true"
            componentName="TryItOutCarouselComponent"
          />
          <div className="try-out-show-container">
            {/* <div className="try-out-show__heading">try this Out</div> */}
            <div className="show-more-contents">{this.renderList()}</div>
            <div
              onClick={() => this.toggleShow()}
              onKeyDown={() => this.toggleShow()}
              className="show-button"
            >
              <span className="carosal-show">Show more</span>
              <img
                src={showMoreArrow}
                alt="show more arrow"
                className="showMoreArrow"
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
