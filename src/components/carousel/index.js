import React, { Component } from 'react';
import RightSideCarousel from "./right-side-carousel";
import Event from "./event";
import "./index.css";


export default class Carousel extends Component {
  render() {
    return (
      <aside className="carousal-group display-none">
      {/* <TryItOutCarousel sectionName="home" /> */}
      <div className="translaterequiredClass">
        <RightSideCarousel blogData = {this.props.blogData}/>
        <Event classList="right-event" events = {this.props.eventData}/>
      </div>
    </aside>
    )
  }
}
