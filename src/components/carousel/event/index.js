import React, { Component } from "react";
import { Link } from "gatsby";
import moment from "moment";
import PinkCale from "../../../images/icon-calendar-magenta.svg";
import cale from "../../../images/icon-calendar.svg";
import location from "../../../images/icon-map-pointer.svg";
import noEvent from "../../../images/events-empty-data-set.png";
import Slider from "react-slick";
import "./index.css";

export default class Event extends Component {
  constructor(props) {
    super(props);
    this.getDate = this.getDate.bind(this);
  }

  getDate(date){
    var d = moment(date);
    return d.format("LL");
  }

  renderSlider(items) {
    return items.map((item, index) => {
      return (
        <article className="sub-event" key={index}>
          <Link to={(item.node.frontmatter.link)?item.node.frontmatter.link: item.node.frontmatter.eventPath}>
            <img src={PinkCale} alt="Calc" className="event-pink-cal" />
          </Link>
          <Link to={(item.node.frontmatter.link)?item.node.frontmatter.link: item.node.frontmatter.eventPath} className="event-btn">
            <span className="event-btn-txt uppercase">
              EVENT
            </span>
          </Link>
          <Link to={(item.node.frontmatter.link)?item.node.frontmatter.link: item.node.frontmatter.eventPath}>
            <article className="padding-bottom">
              <div className="event-api">{item.node.frontmatter.title}</div>
              <div>
                <div className="flex flex-align-center margin-top">
                  <img src={cale} alt="Calander" className="event-cal" />
                  <span className="january-21-to-25-20">
                    {this.getDate(item.node.frontmatter.date)}
                  </span>
                </div>
                <div className="flex flex-align-center margin-top">
                  <img
                    src={location}
                    alt="location"
                    className="event-location"
                  />
                  <span className="hyatt">
                    {item.node.frontmatter.location}
                  </span>
                </div>
              </div>
            </article>
          </Link>
        </article>
      );
    });
  }
  render() {
    return (     
        <section className="event-box-parent">
        <div className="event-box">
         {this.props.events.length > 0 && (   
           <span>      
            <Slider
              dots={true}
              infinite={true}
              speed={500}
              slidesToShow={1}
              slidesToScroll={1}
              autoplay={true}
              arrows={false}
            >
              {this.renderSlider(this.props.events)}
            </Slider>
          <div className="event-mask" /> 
          </span>
       )}
       {this.props.events.length < 1 && (
         <div className="empty-event-div">
          <img src={noEvent} alt="empty-event" className="empty-event-icon"/>
          <h5 className="empty-event-title">No upcoming events!</h5>
          <h6 className="empty-event-subtitle">Check back soon.</h6>
         </div>
       )}
        </div>
      </section>
    )}
}
