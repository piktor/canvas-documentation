import React, { Component } from "react";
import SimpleSliderCarosel from "../carosel";
import website from '../../../images/website.svg';
import "./index.css";

export default class RightSideCarousel extends Component {
  render() {
    return (
      <div>
      {this.props.blogData.length > 0 && (
      <div className="rightSideCarousel">
        <div className="rightSideCarousel-Slider">
          <SimpleSliderCarosel
          itemList={this.props.blogData}
          infinate="true"
          arrows="false"
          dots="true"
          componentName="RightSideCarouselComponent"
        />
        </div>
      </div>
      )}
      {this.props.blogData.length < 1 && (
       <div className="carosal-content">
       <div className="right-carosal-img right-carosal-img-ht">           
          <div className="empty-event-div empty-blog-div"> 
          <div className="gradientt" />
          <img src={website} alt="empty-blog" className="empty-blog-icon" />
            <h5 className="empty-event-title">No current blogs!</h5>
            <h6 className="empty-event-subtitle">Check back soon.</h6>
          </div>
       </div>
       </div>
        )}
        </div>
    );
  }
}
