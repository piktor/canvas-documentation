import React,{ Component } from 'react';
import axios from "axios";
import { Link } from "gatsby";
import './index.css';
import search from '../../images/newicon-search.svg';

class SearchBar extends Component {
    constructor() {
        super();
    
        this.state = {
            suggestions: [],
            activeSuggestion: 0,
            filteredSuggestions: [],
            showSuggestions: false,
            userInput: ""
        };
    }

    componentDidMount() {
        axios.get("https://api.myjson.com/bins/hrk31").then(result => { 
            let resArr = [];       
            const dataApi  = result.data.data;
            const array = [...dataApi.blog.edges, ...dataApi.event.edges, ...dataApi.linked.edges, ...dataApi.toolsDocs.edges];
            for (let index = 0; index < array.length; index++) {
                resArr.push(array[index].node.frontmatter);
            }
            return resArr;
        })
        .then( data => {
            let suggestions = data.map((item) => {
                if(item.blog) {
                    return item.blogTitle;
                }
                else if(item.event || (item.classification === "tools")) {
                    return item.menuTitle;
                }
                else if(item.classification === "documents") {
                    return item.title;
                }
                else{
                    return item.menuTitle;
                }  
            });
            
            this.setState({
                suggestions
            });
        })
        .catch(err => {
            this.setState({ isError: true })
            console.log("----------------------------------------")
            console.log(`The suggestions could not be fetched!\n${err}`)
            console.log("----------------------------------------")
        })

    }

    onChange = e => {
        const userInput = e.currentTarget.value;
        
        const filteredSuggestions = this.state.suggestions.filter(
          suggestion =>
            suggestion.toLowerCase().indexOf(userInput.toLowerCase()) > -1
        );
    
        this.setState({
          activeSuggestion: 0,
          filteredSuggestions,
          showSuggestions: true,
          userInput: e.currentTarget.value
        });
    };
    
    onKeyDown = e => {
        const { activeSuggestion, filteredSuggestions } = this.state;

        if (e.keyCode === 13) {
            document.getElementsByClassName('suggestion-active')[0].click()
        }

        else if (e.keyCode === 38) {
          if (activeSuggestion === 0) {
            return;
          }    
          this.setState({ activeSuggestion: activeSuggestion - 1 });
        }
        
        else if (e.keyCode === 40) {
          if (activeSuggestion - 1 === filteredSuggestions.length) {
            return;
          }    
          this.setState({ activeSuggestion: activeSuggestion + 1 });
        }
    };
     
    enableSearchBar=()=>{
          // document.getElementById('searchBox');
        let searchBox= document.getElementById('searchInput');
        searchBox.focus();
        searchBox.style.width="30rem";
        searchBox.style.background="#242241";
        document.getElementById('rightFlexBox').classList.add('hide');
    }
    
    handleBlur = () =>{
        document.getElementById('searchInput').style.width='';
        document.getElementById('searchInput').style.background='';
        document.getElementById('rightFlexBox').classList.remove('hide');
        this.setState({
            activeSuggestion: 0,
            filteredSuggestions: [],
            showSuggestions: false,
            userInput: ""
        });        
    }

    handleMouseDown = () => {
        document.getElementById('itemLink').click();
    }

    render(){
        const {
            onChange,
            handleBlur,
            onKeyDown,
            state: {
              activeSuggestion,
              filteredSuggestions,
              showSuggestions,
              userInput
            }
          } = this;

        let suggestionsListComponent;

        if (showSuggestions && userInput) {
            if (filteredSuggestions.length) {
                suggestionsListComponent = (
                    <ul class="suggestions">
                    {filteredSuggestions.map((suggestion, index) => {
                        let className;

                        // Flag the active suggestion with a class
                        if (index === activeSuggestion) {
                        className = "suggestion-active";
                        }

                        return (
                            <Link 
                                to='/search'
                                state= {{ searchTerm: suggestion }}
                                id="itemLink" 
                                onMouseDown={this.handleMouseDown}
                            >
                                <li
                                    className={className}
                                    key={suggestion}
                                >
                               
                                    {suggestion}
                                </li>
                            </Link>
                        );
                    })}
                    </ul>
                );
            } else {
                suggestionsListComponent = (
                    <div class="no-suggestions">
                    <em>No suggestions, you're on your own!</em>
                    </div>
                );
            }
        }

        return(
            <section className="search-rectangle" title="Search..">
                <React.Fragment>
                    <input
                    type="text"
                    value={userInput}
                    className="search-text"
                    placeholder="Search..."
                    id='searchInput'
                    onKeyDown={onKeyDown}
                    onChange={onChange}
                    onBlur={handleBlur}
                    autoComplete="off"
                    />
                    {suggestionsListComponent}
                </React.Fragment>
                <div className="search-icon" id="searchBtn" onClick={()=>this.enableSearchBar()}>
                    <img alt="search" src={search} className="search-icon" id="searchBox" />
                </div>
            </section>
        )
    }
}

export default SearchBar;