import React, { Component } from 'react';
import { Provider } from "react-redux";
import store from "../state/createStore";
import SEO from "./seo";
// import store from
import '../scss/custom.scss';
import './layout.css';


// const store = createStore();

export default class Layout extends Component {
  render() {
    return (
      <Provider store={store}>
      <div className="home-container">
      <SEO title="Dev Center" pathname="home" />
        <div>
          {this.props.children}
        </div>
      </div>
      </Provider>
    )
  }
}