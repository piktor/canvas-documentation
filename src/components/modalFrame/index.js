import React, { Component } from "react";
import "./index.css";
import Modal from "react-modal";
import black from "../../images/6B3552.svg";
import brown from "../../images/AB4657.svg";
import orange from "../../images/FA814D.svg";
import grey from "../../images/767F96.svg";
import cross from "../../images/icon-clear@2x.png";
//import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

// Make sure to bind modal to your appElement (http://reactcommunity.org/react-modal/accessibility/)
Modal.setAppElement(`#___gatsby`);

/***
 * class to manage the modal.
 * move all state of modal to parent component
 */
class ModalFrame extends Component {
  spanClass = "";
  nameClass = "";

  iFrameTimeout = null;

  constructor(props) {
    super(props);


    /*let showModal = false;
        if (props.showModal) {
            showModal = true;
            //this.spanClass = "fa-layers fa-fw fa-3x " + this.props.colorClass;
            //this.nameClass = "page-title " + this.props.colorClass;
        }
        this.state = {modalIsOpen: showModal, renderIFrame: false};
*/
    this.state = { renderIFrame: false, colors: [black, brown, orange, grey] };
    this.openModal = this.openModal.bind(this);
    this.afterOpenModal = this.afterOpenModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
  }

  openModal() {
    //this.setState({modalIsOpen: true});
     document.body.style.overflow = 'hidden'
    this.props.changeModalState(true);
  }

  afterOpenModal() {
    // references are now sync'd and can be accessed.
    // this.subtitle.style.color = '#f00';
    //worked try css
    //document.body.style.overflow = 'hidden'
    this.iFrameTimeout = setTimeout(() => {
      this.setState({ renderIFrame: true });
    }, 1500);
  }

  closeModal() {
    //this.setState({modalIsOpen: false, renderIFrame: false});
    this.setState({ renderIFrame: false });
    this.props.changeModalState(false);
    //navigate back to homepage route
    //this.props.history.push('/');
    document.body.style.overflow = 'scroll'
    clearTimeout(this.iFrameTimeout);
  }

  render() {
    return (
      <section>
        <Modal
          isOpen={this.props.showModal}
          onAfterOpen={this.afterOpenModal}
          onRequestClose={this.closeModal}
          className="Modal"
          overlayClassName="Overlay"
          contentLabel="Dev Center Application"
        >
          <div className="commonPageHeader modalHeaderColor">
            {/*<span className={this.spanClass}>
                                        <FontAwesomeIcon icon={['far', 'square']}/>
                                        <FontAwesomeIcon icon={[this.props.iconGroup, this.props.iconName]}
                                                         transform="shrink-8"/>
                                    </span>
                                    */}
            <span>
              <span
                className="modal-image-card-back"
                // style={{
                //     backgroundImage: this.props.image
                //         ? "url(" + white + ")"
                //         : "url(" +
                //         this.state.colors[
                //             Math.floor(
                //                 Math.random() * this.state.colors.length
                //             )
                //             ] +
                //         ")"
                // }}
              >
                <span className="card-img modal-card-img">
                  {this.props.image && (
                    <img
                      src={require("../../pages/" +
                        this.props.imagePath +
                        this.props.image)}
                      alt="Star"
                    />
                  )}
                  {!this.props.image && (
                    <span className="modal-custom-logo">
                      {this.props.name[0]}
                    </span>
                  )}
                </span>
              </span>

              <span className="modal-title">{this.props.name}</span>
            </span>
          </div>
          <button className="closebtn" onClick={this.closeModal}>
            <img src={cross} alt="close-button" className="closebtn-img" />
          </button>

          {/*}
                <div className="iframe_spinner" style={{display: this.state.renderIFrame ? 'none' : 'block'}}>
                    <FontAwesomeIcon icon={['fas', 'spinner']} className="magenta fa-4x fa-pulse"/>
                </div>
            */}
          <iframe
            ref="iframe"
            title="app frame"
            className="iframe-size"
            src={this.props.modalUrl}
            style={{
              visibility: this.state.renderIFrame ? "visible" : "hidden"
            }}
          />
          {/*<div className="modalFooter modalHeaderColor"/>*/}
        </Modal>
      </section>
    );
  }
}

export default ModalFrame;
