import React from 'react';
import './index.css';
import star from '../../../../images/outline-star_border-24px.svg';

const Star = () => (
  <div className="star-div">
    <img alt="star" src={star} className="blog-star" align="right" />
  </div>
);

export default Star;
