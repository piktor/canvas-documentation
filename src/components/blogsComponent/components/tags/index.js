import React from 'react';
import './index.css';
import tags from '../../../../json/tags.json';

const tag = tags.map((item, index) => (
  <span key={parseInt(index.toString(), 10)} className="tags-span">
    {item.name}
  </span>
));

const Tags = () => (
  <div>
    <div className="tag-title">TAGS</div>
    <div className="tags">{tag}</div>
  </div>
);

export default Tags;
