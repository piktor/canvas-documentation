import React, { Component } from 'react';
import './index.css';
import { Link } from 'gatsby';

export default class RecentPosts extends Component {
  constructor(props){
    super(props);
    this.recentPost = this.recentPost.bind(this);
  }
  recentPost(){
    return this.props.recentBlogs.map((item, index) => (
      <Link to={item.node.fields.slug.slice(0, -1)} key={parseInt(index.toString(), 10)} className="section">
        <div  className="recent-post-img">
          <img src={require("../../../../pages/" +
            item.node.frontmatter.image)} alt="blog-page" />
        </div>
        <div className="subsection">
          <div className="ss-category">{item.node.frontmatter.blogTag}</div>
          
          <div className="ss-title">{item.node.frontmatter.blogTitle}</div>
          <div className="ss-details">by {item.node.frontmatter.blogAuthor} / {item.node.frontmatter.blogDate}</div>
        </div>
      </Link>
    ));
  }

  render() {
    return (
      <div className="recent-blogs">
      <div className="title">Recent Posts</div>
      {(this.props.recentBlogs) && (
         <div>{this.recentPost()}</div>
      )}
    </div>
    )
  }
}
