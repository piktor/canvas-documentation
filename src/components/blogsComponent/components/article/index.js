import React, { Component } from 'react';
import './index.css';
import moment from "moment";

export default class Article extends Component {
  constructor(props){
    super(props)
    this.getDate= this.getDate.bind(this);
  }

  getDate(date){
    var d = moment(date);
    return d.format("LL");
  }


  render() {
      return (
        <div>
          <div className="article-sub-title">
            <span>{this.getDate(this.props.blog.frontmatter.blogDate)} &nbsp; | &nbsp; </span>
            <span>{this.props.blog.frontmatter.blogAuthor} &nbsp; | &nbsp; </span>
            <span>{this.props.blog.frontmatter.blogTag}</span>
          </div>
          <div className="article-title">{this.props.blog.frontmatter.blogTitle}</div>
          <div className="article-text" dangerouslySetInnerHTML={{ __html: this.props.blog.html }}></div>
          {/* <div className="article-text">
            {props.blogList.node.frontmatter.blogtitle}
            {' '}
          </div> */}
        </div>
      )
  }
}