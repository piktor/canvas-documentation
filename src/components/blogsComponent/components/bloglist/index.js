import React, {Component} from 'react';
import { Link } from 'gatsby';
import './index.css';
import moment from "moment";

export default class BlogList extends Component {
  constructor(props){
    super(props);
    this.blogs = this.blogs.bind(this);
    this.getDate= this.getDate.bind(this);
  }

  getDate(date){
    var d = moment(date);
    return d.format("LL");
  }

  blogs(){
    return this.props.blogList.map((item, index) => (
      <div key={index} className="section">
        <div className="bloglist-img">
          <img src={require("../../../../pages/" +
            item.node.frontmatter.image)} alt="blog-post"/>
        </div>
        <div className="subsection">
          <div className="bloglist-category">{item.node.frontmatter.blogTag}</div>
          <div className="bloglist-title">{item.node.frontmatter.blogTitle}</div>
          <div className="bloglist-details">by {item.node.frontmatter.blogAuthor} / {this.getDate(item.node.frontmatter.blogDate)}</div>
          <div className="bloglist-descripion" dangerouslySetInnerHTML={{ __html: item.node.html.split(
                      "</a></p>"
                    )[1] }}/>
          <Link to={item.node.fields.slug.slice(0, -1)} className="bloglist-readmore">
            Read More
          </Link>
        </div>
      </div>
    ));
  }

  render() {
    return (
      <div className="blogList">{this.blogs()}</div>
    )
  }
}
