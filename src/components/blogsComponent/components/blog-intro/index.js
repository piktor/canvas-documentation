import React, { Component } from 'react';
import { Link } from 'gatsby';
import moment from "moment";
import './index.css';

export default class BlogIntro extends Component {
  constructor(props) {
    super(props);
    this.getDate= this.getDate.bind(this);
  }

  getDate(date){
    var d = moment(date);
    return d.format("LL");
  }
  
  render() {
    var bgImage = require("../../../../pages/" + this.props.blogdata.node.frontmatter.image ); 
    return (
        <div className="blogintro max-width-blog-upper">
          <div className="blog-intro-left" 
                           style={{
                            backgroundImage: this.props.blogdata.node.frontmatter.image
                              ? " linear-gradient(270deg,transparent,rgba(0,0,0,.72)), url(" + bgImage + ")"
                              : " linear-gradient(270deg,transparent,rgba(0,0,0,.72)), url()"
                          }}>
            <div className="blog-date">{this.getDate(this.props.blogdata.node.frontmatter.blogDate)} | BY {this.props.blogdata.node.frontmatter.blogAuthor} | {this.props.blogdata.node.frontmatter.blogTag}</div>
            <div className="leaderswith">{this.props.blogdata.node.frontmatter.blogTitle}</div>
          </div>
      
          <div className="blog-intro-right">
            <div className="lorem">
              <div className="blog-intro-text" dangerouslySetInnerHTML={{ __html: this.props.blogdata.node.html.split(
                      "</a></p>"
                    )[1] }} />
              <br />
              <br />
              <div>
                <Link to={this.props.blogdata.node.fields.slug.slice(0, -1)} className="Read-More">
                  Read More
                </Link>
              </div>
            </div>
          </div>
        </div>
    )}}