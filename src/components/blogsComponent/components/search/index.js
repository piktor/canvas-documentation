import React, { Component } from 'react';
import './index.css';
import search from "../../../../images/icon-search.png";
import searchClear from '../../../../images/icon-clear.svg';

class Search extends Component {

  constructor() {
    super();
    this.searchList = this.searchList.bind(this);
    this.clickedOnSearch = this.clickedOnSearch.bind(this);
    this.onBlurSearch = this.onBlurSearch.bind(this);
    this.clearSearchBar = this.clearSearchBar.bind(this);

    this.mouseOver = this.mouseOver.bind(this);
    this.mouseOut = this.mouseOut.bind(this);

    this.state = {
      inSearchMode: false,
      searchText: "",
      searchBtnClicked: false,
      onCancelBtn: false,
      pathname: "",
      fitered: []
    }
  }

//   componentDidMount() {
//     this.reCreateNavImage();
//     if (this.state.searchText.length > 0) {
//         this.setState({ inSearchMode: true });
//     }
// }

searchList(data) {
  if (data.length > 0) {
      this.setState({ searchText: data, inSearchMode: true });
      // localStorage.setItem("searchText", data);
  } else {
      this.setState({ searchText: data, inSearchMode: false });
      // localStorage.setItem("searchText", data);
  }
}

renderSearchList() {
  const word = this.state.searchText.toLowerCase();
  this.props.navList.filter((item) => {
      const result = item.node.frontmatter.menuTitle.toLowerCase().includes(word);
      if (result) {
          return item;
      }
      return 0;
  });
}

clickedOnSearch() {
  this.setState({ searchBtnClicked: true });
}

onBlurSearch() {
  this.setState({ searchBtnClicked: false });
}
clearSearchBar() {
  this.setState({ searchText: "", inSearchMode: false, searchBtnClicked: true });
  // localStorage.setItem("searchText", "");
}

mouseOver() {
    document.querySelector(".cross-icon").classList.remove("search-iconn-out");
    document.querySelector(".cross-icon").classList.add("search-iconn-hover");
}

mouseOut() {
    document.querySelector(".cross-icon").classList.remove("search-iconn-hover");
    document.querySelector(".cross-icon").classList.add("search-iconn-out");
}

  render() {
    return (
      <div className={("searchbar" + ((this.state.inSearchMode || this.state.searchBtnClicked) ? " bg-white" : ""))}>
        <input type="text" className="search-blog" placeholder="Search Blog posts" value={this.state.searchText} onBlur={() => this.onBlurSearch()} onFocus={() => this.clickedOnSearch()} onChange={(event) => this.searchList(event.target.value)} />
        {(!this.state.inSearchMode) && (
          <img alt="search" src={search} className="search-iconn" />
        )}
        {(this.state.inSearchMode) && (
          <img alt="search" src={searchClear} onClick={() => this.clearSearchBar()} onMouseEnter={() => this.mouseOver()} onMouseLeave={() => this.mouseOut()} className="search-iconn cross-icon cursor-pointer" />
        )}
      </div>
    )
  }

  // <div className="searchbar">
  //   <input
  //     type="text"
  //     placeholder="Search Blog posts"
  //     className="search-blog"
  //   />
  //   <img src={search} alt="search" className="blog-search-icon"/>
  // </div>
}

export default Search;
