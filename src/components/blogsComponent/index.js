import React, { Component } from 'react';
import Search from './components/search/index';
import BlogIntro from './components/blog-intro';
import BlogList from './components/bloglist/index';
import RecentPosts from './components/recent-posts';
import Header from '../header/expandHeader';
import Footer from "../footer";
import Tags from './components/tags';
import './index.css';

export default class BlogPageHome extends Component {
  render() {
    return (
  <div className="blog-home">
    <Header />
    <section className="blog-home-main-section">
    <BlogIntro blogdata={this.props.blogdetails[0]} />
    
    <section className="maindiv-bgcolor">
    <div className="maindiv max-width-blog">
      <div className="bloghome-left">
        <Tags />
        <RecentPosts recentBlogs = {this.props.blogdetails}/>
      </div>

      <div className="bloghome-right">
        <Search />
        <BlogList blogList = {this.props.blogdetails}/>
      </div>
    </div>
    </section>
    </section>
    <Footer />
  </div>
)}
    }