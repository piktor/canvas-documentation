import React, { Component } from 'react';
import footerTags from '../../json/footer-tags.json';
import "./index.css";


export default class UpperFooter extends Component { 

renderTags(list){
    return list.map((item, index) => {
        return <div className="footer_tag" key={index}>{item}</div>;
    });
}

footerTag(){
    return(
        footerTags.map((item, index) => (
            <article className="fsection" key={index}>
            <div className="footer_title">{item.title}</div>
            <div className="fline" />
            {this.renderTags(item.links)}
            </article>
        ))
    )
}

  render() {
    return (
    <section className="upper_footer">
        <div className="footer_sections  max-width">{this.footerTag()}</div>
    </section>
    )
  }
}
