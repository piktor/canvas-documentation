import React, { Component } from "react";
import "./index.css";
import FeaturedEvents from "./components/featuredEvents";
import UpcomingEvents from "./components/upcomingEvents";
import Header from "../header/expandHeader";
import collapse_icon from '../../images/icon_collapse.svg';
import expand_icon from '../../images/icon_expand.svg';
// import DevJourneyLeft from '../devJourneyComponent/components/devJourneySteps/components/devJourneyNavBar';
import DevJourneyNavBar from '../devJourneyComponent/components/devJourneySteps/components/devJourneyNavBar';

export default class EventsListComponent extends Component {
  constructor() {
    super();
    this.state = {
      expand: false
    }
  }

  toogleClickEvent = () => {

    if (document.getElementById('devJourneyNavb')) {
      document.getElementById('devJourneyNavb').classList.toggle('collapseJourneyEvent');
      // // document.getElementById('collapse-btn').classList.toggle('display-none-devBtn');
      document.getElementById('devJourneyNavb') && document.getElementById('devJourneyNavb').classList.remove('.collapseJourneyEvent');
    }
    if (document.getElementsByClassName('devJourneyNav').length > 0){
      document.getElementsByClassName('devJourneyNav')[0].classList.toggle('nav-expanded');
    }
      document.getElementById('devJourneyNavbarr') && document.getElementById('devJourneyNavbarr').classList.toggle('collapseJourney');
      // document.getElementById('devJourneyNavb') && document.getElementById('devJourneyNavb').classList.toggle('collapseJourney');
      document.getElementById('devJourneyRight-div') && document.getElementById('devJourneyRight-div').classList.toggle('devJourneyRi');
    
    if (window.location.pathname.indexOf(("/toolsdocs") > -1)) {
      document.getElementById('collapse-btn').classList.toggle('rotate-btn');
    }
    this.setState({
      expand: !this.state.expand
    })

    // document.getElementById('devJourneyNavb').classList.toggle('collapseJourneyEvent');
    // // document.getElementById('collapse-btn').classList.toggle('display-none-devBtn');
    // document.getElementById('devJourneyNavb').classList.remove('.collapseJourneyEvent');
    // // document.getElementById('collapse-btn-event').classList.toggle('btn-positn-event')
    // this.setState({
    //   expand: !this.state.expand
    // })
  }
  render() {
    return (
      <div className="eventList-comp">
        <Header />

        <section className="eventsListComponent">
          <div className="eventsListComponent-header">
            <h1 className="eventsListComponent-header-heading max-width">
              Events 2019
            </h1>
          </div>

          <aside className="devJourneyNav">
            <div className="devJourneyNavbarr collapseJourney" id="devJourneyNavbarr" style={(typeof window !== 'undefined' && window.localStorage.getItem('JOURNEY_VALUE')) ? { display: "block" } : { display: "none" }}>
              <DevJourneyNavBar />
            </div>
          </aside>
          <div className="collapse-btn-event toolsdevJourney" title="Dev Journey" id="collapse-btn-event" onClick={this.toogleClickEvent} style={(typeof window !== 'undefined' && window.localStorage.getItem('JOURNEY_VALUE')) ? { display: "block" } : { display: "none" }}>
            <img src={expand_icon} alt="expand-icon" />
          </div>
          <aside className="devJourneyNavEvent devJourneyNavEvent-lr" id="devJourneyNavb" style={(typeof window !== 'undefined' && window.localStorage.getItem('JOURNEY_VALUE')) ? { display: "block" } : { display: "none" }}>
            <DevJourneyNavBar />
          </aside>

          <main className="eventsListComponent-main max-width">
            <div className="event-dev-align">
              {/* <div className="collapse-btn-event" title="Dev Journey" id="collapse-btn-event" onClick={this.toogleClickEvent} style={(typeof window !== 'undefined' && window.localStorage.getItem('JOURNEY_VALUE'))? {display:"block"} : {display: "none"}}>
                <img src={expand_icon} alt="expand-icon" />
              </div>
              <aside id="devJourneyNavb" className="devJourneyNavEvent" >
                <DevJourneyLeft />
              </aside> */}
              {(this.props.eventsList.length > 0) && (  
                <h2 className="eventsListComponent-subsection-heading">
                  Featured Events
                </h2>
              )}
            </div>

            {(this.props.eventsList.length > 0) && (
              <FeaturedEvents eventList={this.props.eventsList} />
            )}
            <h2 className="eventsListComponent-subsection-heading heading-border-bottom">
              Upcoming Events
            </h2>
            {(this.props.eventsList.length > 0) && (<article className="upcomingEvents-table-headings flex">
              <span className="ue-heading ueh1">Date</span>
              <span className="ue-heading ueh2">Event</span>
              <span className="ue-heading ueh3">Location</span>
            </article>)}
            <UpcomingEvents eventList={this.props.eventsList} />
          </main>
        </section>
      </div>
    );
  }
}
