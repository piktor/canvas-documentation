import React, { Component } from "react";
import "./index.css";
import moment from "moment";
import PinkCale from "../../../../images/icon-calendar-magenta.svg";
import cale from "../../../../images/icon-calendar.svg";
import location from "../../../../images/icon-map-pointer.svg";
import Link from "gatsby-link";

export default class FeaturedEvets extends Component {
  constructor(props) {
    super(props);
    this.event = this.event.bind(this);
    this.getDate= this.getDate.bind(this);
  }

  getDate(date){
    var d = moment(date);
    return d.format("LL");
  }

  event() {
    if (this.props.eventList.length !== 0) {
      return this.props.eventList.slice(0, 3).map((item, index) => {
        return (
          <article
            key={parseInt(index.toString(), 10)}
            className="featured-events-div"
          >
            <div className="featured-events-box">
            <Link to={item.node.fields.slug.slice(0, -1)}>
            <img src={PinkCale} alt="Calc" className="event-pink-cal" />
          </Link>
              <Link to={item.node.fields.slug.slice(0, -1)} className="event-btn">
            <span className="event-btn-txt uppercase">
              EVENT
            </span>
            </Link>
            <Link to={item.node.fields.slug.slice(0, -1)}>
            <article className="padding-bottom">
              <div className="event-api">{item.node.frontmatter.title}</div>
              <div>
                <div className="flex flex-align-center margin-top">
                  <img src={cale} alt="Calander" className="event-cal" />
                  <span className="january-21-to-25-20">
                    {this.getDate(item.node.frontmatter.date)}
                  </span>
                </div>
                <div className="flex flex-align-center margin-top">
                  <img
                    src={location}
                    alt="location"
                    className="event-location"
                  />
                  <span className="hyatt">
                    {item.node.frontmatter.location}
                  </span>
                </div>
              </div>
            </article>
          </Link>
            </div>
          </article>
        );
      });
    }
    else {
      return(
        <div>No Events!</div>
      )
    }
  }

  render() {
    return (
      <section className="featured-events-section">{this.event()}</section>
    );
  }
}
