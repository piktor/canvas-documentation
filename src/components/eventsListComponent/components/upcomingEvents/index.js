import React, { Component } from "react";
import "./index.css";
import eventCal from "../../../../images/icon-calendar-el.svg";
import arrow from "../../../../images/arrow-documents.svg";
import noEvent from "../../../../images/events-empty.png";
import moment from "moment";
import Link from "gatsby-link";

export default class UpcomingEvents extends Component {
  constructor(props) {
    super(props);
    this.events = this.events.bind(this);
    this.getDate = this.getDate.bind(this);
    this.getMonth = this.getMonth.bind(this);
  }

  getDate(date) {
    var d = moment(date);
    return d.format("D");
  }

  getMonth(date) {
    var m = moment(date);
    return m.format("MMM");
  }

  events() {    
    if (this.props.eventList.length > 0) {
      return this.props.eventList.map((item, index) => {
        return (
          <Link to={item.node.fields.slug.slice(0, -1)} key={index}>
          <article
            key={parseInt(index.toString(), 10)}
            className="upcoming-events-div flex"
          > 
            <div className="flex flex-between flex-align-center">
              <div className="uevent-date-div">
                <img
                  src={eventCal}
                  alt="calender-date"
                  className="calender-date"
                />
                <span className="uevent-date">
                  <span className="uevent-day">
                    {this.getDate(item.node.frontmatter.date)}
                  </span>
                  <span className="uevent-month">
                    {this.getMonth(item.node.frontmatter.date)}
                  </span>
                </span>
              </div>
              <div className="uevents-details-div">
                <div className="uevent-name">{item.node.frontmatter.title}</div>
                <div
                  className="uevent-detail"
                  dangerouslySetInnerHTML={{
                    __html: item.node.html.split(
                      "</a></p>"
                    )[2]
                  }}
                />
              </div>
            </div>
            <div className="flex flex-between flex-align-center ueh3">
              <div className="uevent-location">
                {item.node.frontmatter.location}
              </div>
              <img src={arrow} alt="arrow" className="uevent-arrow" />
            </div>
          </article>
        </Link>
        );
      });
    }
    else {
      return(
        <div className="empty-event-div">
          <img src={noEvent} alt="empty-event" className="empty-event-icon"/>
          <h5 className="empty-event-title">No upcoming events!</h5>
          <h6 className="empty-event-subtitle">Check back soon.</h6>
        </div>
      )
    }
  }

  render() {
    return (
      <section className="upcoming-events-section">{this.events()}</section>
    );
  }
}
