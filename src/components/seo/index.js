import React, { Component } from "react";
import Helmet from "react-helmet";

export default class SEO extends Component {
  render() {
    return (
      <Helmet
          title={this.props.title}
          meta={[
            { name: 'description', content: 'Sample' },
            { name: 'keywords', content: 'sample, something' },
          ]}
        >
          <html lang="en" />
        </Helmet>
    );
  }
}
