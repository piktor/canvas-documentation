import React, { Component } from 'react';
import cardImg from '../../images/fill-2.png';
import logo from '../../images/dev-center@3x.png';
import beta from '../../images/beta-icon.svg';
import home from '../../images/icon-home.svg';
import blog from '../../images/icon-blog.svg';
import cal from '../../images/icon-eventcalendar.svg';
import { Link } from "gatsby";
import './index.css';
import '../../scss/theme.scss';

export default class BootstrapDemo extends Component {
    burgerClick = () => {
        document.getElementById('collapsibleNavbar').classList.toggle('activee');
        // $(document).ready(function () {

        //     $('#sidebarCollapse').on('click', function () {
        //         $('#sidebar').toggleClass('active');
        //     });

        // })
    }

    clickToggle = () => {
        document.getElementById('sidebar').classList.toggle('activee');
    }
    render() {
        return (
            <div>
                <nav className="navbar fixed-top navbar-expand-md navbar-dark blue-header py-sm-3 px-sm-5">
                    <div className="max-width-header container-fluid justify-content-between mr-4 mx-sm-auto">

                        <button class="navbar-toggler" id="sidebarCollapse" onClick={this.clickToggle} type="button" data-toggle="collapse" data-target="#collapsibleNavbar" aria-controls="collapsibleNavbar" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <Link to='/' className="navbar-brand mx-auto flex flex-start flex-align-start">
                            <img src={logo} alt="logo" className="logo" />
                            <img src={beta} alt="beta" className="beta" />
                        </Link>
                        {/* <div class="collapse navbar-collapse flex-end" id="collapsibleNavbar">
                            <ul className="navbar-nav">
                                <li className="nav-item">
                                    <img src={home} alt="home" />
                                </li>
                                <li className="nav-item">
                                    <img src={blog} alt="blog" />
                                </li>
                                <li className="nav-item">
                                    <img src={cal} alt="cal" />
                                </li>
                            </ul>
                        </div> */}
                    </div>
                </nav>


                <nav id="sidebar" className="sidenavBar">
                    <ul className="list-unstyled components">
                        <li>
                            <img src={home} alt="home" />
                        </li>
                        <li>
                            <img src={blog} alt="blog" />
                        </li>
                        <li>
                            <img src={cal} alt="cal" />
                        </li>
                    </ul>
                </nav>

                <br />
                <hr />
                <br />
                <p>In quis et enim veniam quis ad eu voluptate anim eiusmod. Velit officia in in nulla consequat consectetur aute consequat et fugiat id. Laboris velit consequat pariatur consectetur eiusmod enim. Officia non culpa excepteur qui elit aute nostrud fugiat sint laborum et. Occaecat consequat eiusmod cillum consectetur eu mollit exercitation commodo dolore laboris.Tempor irure veniam et labore mollit aliqua exercitation sint enim adipisicing sit mollit adipisicing. Ut ex Lorem ipsum veniam ea consequat. Sunt proident velit cupidatat adipisicing aute proident nostrud irure. Anim adipisicing cupidatat velit dolore mollit anim sint occaecat minim cupidatat. Labore amet mollit voluptate cupidatat pariatur qui ullamco. Commodo consequat consequat magna laborum ipsum ex nulla voluptate non sint veniam veniam nisi.Consectetur laborum duis ut occaecat eu. Aliqua excepteur ipsum ullamco ullamco ea non magna culpa esse tempor in et mollit. Irure enim ex consectetur eiusmod. Ea minim et ea aute et fugiat proident. Lorem amet ut non aute veniam ut irure veniam et. Esse nulla dolor Lorem exercitation magna. Est ut ipsum ea ex ipsum deserunt deserunt adipisicing ad irure qui ipsum.Amet dolore Lorem irure ex ullamco velit pariatur. Amet esse eu irure quis voluptate nostrud culpa pariatur enim laboris elit. Fugiat eiusmod labore sit dolore laboris Lorem laborum. In laborum nulla nisi excepteur qui quis nostrud. Exercitation duis mollit adipisicing minim. Officia ut officia et in ex sit tempor veniam eiusmod esse nostrud ea pariatur.Dolore aliquip labore ea fugiat pariatur magna ea laboris commodo elit culpa esse aute. Sunt aliqua culpa sit sint incididunt et incididunt. Esse tempor ullamco et voluptate. Sit id nisi laborum aliqua mollit reprehenderit quis amet in sunt pariatur.</p>


                <div className="card-cont card-wrapper to-margin">
                    <div className="card doc-card">
                        <div className=".card-img-top">
                            <img src={cardImg} alt="card-img" />
                        </div>
                        <div className="card-body">
                            <div className="card-title">Card</div>
                            <div className="card-text">card detail</div>
                        </div>
                    </div>
                    <div className="card doc-card">
                        <div className=".card-img-top">
                            <img src={cardImg} alt="card-img" />
                        </div>
                        <div className="card-body">
                            <div className="card-title">Card</div>
                            <div className="card-text">card detail</div>
                        </div>
                    </div>
                    <div className="card doc-card">
                        <div className=".card-img-top">
                            <img src={cardImg} alt="card-img" />
                        </div>
                        <div className="card-body">
                            <div className="card-title">Card</div>
                            <div className="card-text">card detail</div>
                        </div>
                    </div>
                    <div className="card doc-card">
                        <div className=".card-img-top">
                            <img src={cardImg} alt="card-img" />
                        </div>
                        <div className="card-body">
                            <div className="card-title">Card</div>
                            <div className="card-text">card detail</div>
                        </div>
                    </div>
                    <div className="card doc-card">
                        <div className=".card-img-top">
                            <img src={cardImg} alt="card-img" />
                        </div>
                        <div className="card-body">
                            <div className="card-title">Card</div>
                            <div className="card-text">card detail</div>
                        </div>
                    </div>
                    <div className="card doc-card">
                        <div className=".card-img-top">
                            <img src={cardImg} alt="card-img" />
                        </div>
                        <div className="card-body">
                            <div className="card-title">Card</div>
                            <div className="card-text">card detail</div>
                        </div>
                    </div>
                </div>

                <nav aria-label="breadcrumb">
                    <ol className="breadcrumb">
                        <div className="breadcrumb-flex">
                            <li className="breadcrumb-item">
                                <Link to="/" className="breadcrumb-li text-style-1">Home</Link>
                            </li>
                            <li className="breadcrumb-item">
                                <Link to="/" className="breadcrumb-li text-style-1">Blog</Link>
                            </li>
                            <li className="breadcrumb-item active" aria-current="page">
                                <Link to="/" className="breadcrumb-li text-style-1">Page</Link>
                            </li>
                        </div>
                    </ol>
                </nav>

                <footer className="footer">
                    <div className="content">
                        <div className="copyright">© 2019 All rights reserved</div>
                    </div>
                </footer>
            </div>
        )
    }
}