import React from "react"
import PropTypes from "prop-types"

export default function HTML(props) {
  return (
    <html id="main-html" {...props.htmlAttributes}>
      <head>
        <meta charSet="utf-8" />
        <meta httpEquiv="x-ua-compatible" content="ie=edge" />
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1, shrink-to-fit=no"
        />
        {props.headComponents}
      </head>
      <body {...props.bodyAttributes}>
        {props.preBodyComponents}
        <noscript key="noscript" id="gatsby-noscript">
qwertyuioperghuiop[vbhnjmk]        </noscript>
        <div
          key={`body`}
          id="___gatsby"
          dangerouslySetInnerHTML={{ __html: props.body }}
        />

        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-135690304-1"/>

<script
  dangerouslySetInnerHTML={{
    __html: `
        if((window.location.hostname.includes("beta.devcenter.t-mobile.com") > -1) || (window.location.hostname.includes("devcenter.corporate.t-mobile.com") > -1) || (window.location.hostname.includes("devcenter.t-mobile.com") > -1)){
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', 'UA-135690304-1');   
        }`,
  }}
/>
        {props.postBodyComponents}

      </body>
    </html>
  )
}

HTML.propTypes = {
  htmlAttributes: PropTypes.object,
  headComponents: PropTypes.array,
  bodyAttributes: PropTypes.object,
  preBodyComponents: PropTypes.array,
  body: PropTypes.string,
  postBodyComponents: PropTypes.array,
}
