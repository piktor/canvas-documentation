import React from 'react';
import NotFound from "../components/notFound";

const NotFoundPage = () => (
  <NotFound/>
)

export default NotFoundPage
