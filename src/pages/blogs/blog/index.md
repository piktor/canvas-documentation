---
blogPath: /blog/leaders-with-social-heart
blogDate: '2019-03-15'
blogAuthor: John Doe 
blogTag:  API DEVELOPMENT
blogTitle: 'Leaders With Social Heart: Three Companies on a Mission to Better The World'
image: "blogs/blog/assets/rectanglebp2.png"
draft: true
blog: true
---


![Blog-image](./assets/rectanglebp2.png)



Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris nec sapien pellentesque nulla elementum molestie. Donec facilisis nulla et dignissim varius. Proin at iaculis purus. Suspendisse sodales sodales orci, efficitur dictum ligula lacinia vitae. Vivamus vel fermentum velit, ac venenatis est. Quisque facilisis risus sed quam molestie gravida. Praesent egestas neque eu imperdiet varius. Nam tellus dui, malesuada id luctus a, semper vel magna. Etiam efficitur, odio sed efficitur lobortis, neque orci porta augue, eu elementum nisi mi ac felis. 

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris nec sapien pellentesque nulla elementum molestie. Donec facilisis nulla et dignissim varius. Proin at iaculis purus. Suspendisse sodales sodales Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris nec sapien pellentesque nulla elementum molestie. Donec facilisis nulla et dignissim varius. Proin at iaculis purus. Suspendisse sodales sodales orci, efficitur dictum ligula lacinia vitae. Vivamus vel fermentum velit, ac venenatis est. Quisque facilisis risus sed quam molestie gravida. Praesent egestas neque eu imperdiet varius. Nam tellus dui, malesuada id luctus a, semper vel magna. Etiam efficitur, odio sed efficitur lobortis, neque orci porta augue, eu elementum nisi mi ac felis. 

<br/>

![rectangle-copy-4](./assets/rectangle-copy-4.png)    |  ![](./assets/rectangle-copy-5.png)
:-----------------------------------------:|:--------------------------------------:
                                         |      

<br/>                                         

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris nec sapien pellentesque nulla elementum molestie. Donec facilisis nulla et dignissim varius. Proin at iaculis purus. Suspendisse sodales sodales orci, efficitur dictum ligula lacinia vitae. Vivamus vel fermentum velit, ac venenatis est. Quisque facilisis risus sed quam molestie gravida. Praesent egestas neque eu imperdiet varius. Nam tellus dui, malesuada id luctus a, semper vel magna. Etiam efficitur, odio sed efficitur lobortis, neque orci porta augue, eu elementum nisi mi ac felis.