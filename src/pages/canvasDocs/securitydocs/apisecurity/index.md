---
path: canvasDocs/documentationspace/eat-2-csdm
title: "EAL to CSDM"
menuTitle: "EAL to CSDM"
draft: true
order: 1
tags: API Security, 
      Security

---


## EAL to CSDM

This is a sample space for EAL to CSDM

<!-- |Working Group | Slack Channel  |
|------     | ------      |
|Himanshu Kumar | \#apicoe|
|Doug McDorman  |       &nbsp;  |
|Sharma Devanand |      &nbsp;   |


To contribute, add content (markdown) go to: <a href="https://bitbucket.service.edp.t-mobile.com/projects/DIAPIC/repos/api-security-governance-site/browse" target="_blank">https://bitbucket.service.edp.t-mobile.com/projects/DIAPIC/repos/api-security-governance-site/browse</a>  

For more details see the [CONTRIBUTING.md](CONTRIBUTING.md) file.

Please go to: Slack channel <a href="https://t-mo.slack.com/messages/C5JL97XJ6" target="_blank">#apicoe</a> and comment and give feedback. We will love to hear from you!


## Why API Security?

APIs express domain capability and provide building blocks for unlocking various business use cases. API creation requires many thoughtful decisions to be made along its life-cycle phases like Design, Build, Expose/Grant Access, and Operation. While focusing on the "functional" aspect of an API is an obvious and most focused goal, the full benefits of an API cannot be realized until the API is Secure, Operationally robust & observable. \#DevSecOps

This section will try to highlight the key areas that an API must be capable of:

1. Confidentiality 
1. Integrity 
1. Authentication 
1. Authorization 
1. Attack Mitigation (prevention / detection / response)
1. Protection against overuse / replay / inappropriate / malicious use
1. Telemetry / Logging / Auditing / Monitoring / Observability 


More generally the API needs to have the following characteristics:

* Mutual Authentication
* Confidentiality
* Integrity
* Auditability – An ability of a system to conduct persistent, nonbypassable monitoring of all actions performed by humans or machines within the system
* Replay Attack Prevention
* Binding authentication information to transaction
* Protection from Denial of Service attacks (DoS)
* Changing authentication credentials without downtime
* Load balancing, redundancy
* Non-repudiation - An ability of a system to prove (with legal validity) occurrence/non-occurrence of an event or participation/non-participation of a party in an event
* Not passing secret authentication information through channel. For example, passwords and Oauth tokens typically pass through the channel. But x.509 certificate authentication or digital signing passes only the information derived from the secret, not the secret itself
* Secured transactions preferred over just secured pipes (message protection vs. transmission protection)
* Propagate the principal identity
* Attack detection, alerting and response
* Basic attack prevention (e.g. cross-site scripting (XSS) and SQL injection)
* Rate limiting
* Attack filtering and mitigation, often sometimes described as “dynamic patching” where newly discovered attacks or vulnerabilities are mitigated before reaching the API
* Security Logging and Monitoring

### Replay Attack Prevention

Replay attack detection and prevention prevents an attacker from sending the same request more than once, or recording a valid request and sending it again. An example would be to record a $10 payment to an account. The attacker might record the valid transaction and replay it again to add additional payments that did not actually happen.

Replay attacks are normally detected by caching unique pre-request values (nonce) or timestamps and identifying and rejecting the ones with identical values. -->