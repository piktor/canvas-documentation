---
path: canvasDocs/samplespace/samplepage
title: Sample Documentation space
menuTitle: Sample Space
draft: true
order: 2
tags: "Api Security"
---

## Sample documentation space

This is a sample documentation space

<!-- [[mdImage]]
| ![API Security](assets/apisecurity.jpg) -->



<!-- The table given below shows the broad categories of APIs and the minimum security measures needed.

 

|API Classification | Mandatory Security Measures|
|------ | ------ |
|*All APIs* | 1. Shield itself from threats & script ingestion to avoid malicious attack <p>2. Telemetry/Monitoring/Observability (quota, spike arrest, etc)|
|Public content or API intented to be open (eg: Product catalog, Store locator, forgot password) | 1. Transport level integrity and confidentiality <p>Exceptions: Third-party, GSM/Network APIs|
|APIs that deal with CPNI, PII & PCI data (eg: GET /customers, GET /Payments) |1. Transport level integrity and confidentiality<p>2. Access must require authentication <p>3. Data authorization must be enforced by Domain|
|User/Customer Submission APIs (eg: Coupon redeem, PIN verification)	| 1. Transport level integrity and confidentiality <p>2. Access must require authentication <p>3. Data authorization must be enforced by Domain <p>4. Systematic & Schematic Lock on overuse or failed attempt|


These security measures cannot be achieved unless we build them in our API SDLC from the very beginning. API designers must follow design guidelines at <a href="http://tm/apis" target="_blank">http://tm/apis</a> and other best practices at DSO Lighthouse.

## API Security Considerations: Design phase
1.	T-Mobile REST guidelines recommend that sensitive data must not be in URI; follow <a href="http://tm/apis" target="_blank">http://tm/apis</a> (section 1.3.1: /uri/uri-sensitive/#1-3-1-the-security-issue).
1.	Review data classification document: TISS-310 INFORMATION CLASSIFICATION STANDARD.
1.	PCI card data must be tokenized at the input layer before transmitting to APIs.
1.	Consider similar tokenization/encryption using HP Voltage, for other highly sensitive PII data like SSN.
1.	Data encryption at REST: any micro-services data persistence and file systems must consider data encryption for classified data per TISS before storing.
1.	Credential information should not be a part of API response.
1.	Swagger must define specific data formats, patterns and lengths, and enforce these during runtime.
1.	Design field level authorization per client: For example, assume that an API supports certain element *SSN*. But, if API consumer does not need this information since it is classified sensitive, either create a wrapper API or apply data filtering as appropriate to exclude this element for this client.
1.  Propagate initial identity: Authentication/identityPropagation - IDToken
1.  The system should be designed so that any authentication credentials can be rotated without downtime. <p>Examples : <p>
 * Using two service accounts to facilitate service account password changes.<p>
 * Configured to trust a small list of signing certificates or Root CAs instead of just one so that expiring certificates can be rotated out.

## API Security Considerations: Build phase

We need to think about access patterns that will be supported by the APIs at the run time.

* Will it be partners, mobile device/tablet & apps, customers, rep & dealers, third-party apps, direct browser? 
* Will it run on SaaS, OnPrem or on which cloud platform(s)? 
* What security capabilities does the API & micro-services implementation platform provide/enable/support?

Here are the key aspects that should be addressed in the Build phase:

#### Data Authorization (must be enforced by Domain): 
API providers cannot assume that UI is taking care of appropriate data access; they must have authentication and authorization in place. This is important as your API consumer types may change over a period of time and cause security issues if it was started as an internal API but later got used by an App that exposes it to the public.

1. Product Level AuthZ
1. RBAC
1. TAAP -JWT: API Security Link . Review TAAP to conclude on IDToken and/or access token. Header param names, ABF & authorization content in token, various token scopes, unified token process & format for various IDP.
1. Domain Data Access

#### Shield from threats and script ingestion:
Depending on API implementation and deployment platform, many of the below aspects will be supported by the platform. Apigee platform supports the below and needs API developers to use standard build pipelines, which automatically add these policies:

#### DDoS Protection

1. Threat Mitigation: Rate Limit, Spike Arrest, Quota, concurrent rate limits.
1. Script or SQL Injection, XSS: CORS headers, Java callouts for RegEx checks.
1. Build Pipeline : SecurityFaultCallout,TrafficManagementFault,ThreatFaultHandling,DefaultFaultCallout,SpikeArrestCallout,VerifyAccessToken,JSONThreatValidation,ThreatProtection,QuotaAtProductLevel,getCORSDomainList,CORSValidation,cf_FaultNotacceptable,cf_FaultUnsupportedMediaType,cf_Unknown_Resource,BasicAuthentication, VerifyAPIkey, OAuthV2

#### Message Masking and Encryption
API developers should implement payload field encryption/decryption as needed and recommended by DSO. They also need to work with platform teams for log masking configuration.

#### Systematic and Schematic Lock on overuse or failed attempt
Developers need to implement max failure/velocity check (certain count in a certain time period on a given key) – type of enforcement for their API as per the data classification and sensitivity. ID/account validations, password/pin validations and similar things must require these considerations .However, these are only examples and NOT a complete applicable scenario.

#### Code Scans: Fortify and security code scans/review.
This needs to be added to build pipeline but developers should address any reported issues.

#### Vulnerability Test: Pen Test in QA & PROD
This already exist, but has to be made more structured and mandatory. Developers should address any reported issues.

## API Security Considerations: API Exposure phase

Define approved access patterns – (partners, device, customer, rep, third-party) SaaS, OnPrem

Work with DSO to get finalized security pattern which can be implemented on the platform as Transport level integrity and confidentiality using Transport (TLS1.2) 1way or mTLS (2 way)

#### Access must require authentication:

     Types of token to be used : TAAP – JWT with PoP Token

     Opaque Tokens (OAuth2) are not recommended and require explicit exception approval from DSO and platform team.

#### API Product : What do API products do and why to pay attention to it?
It is the bundling of your APIs which allows you to configure invocation Quota, Spike and Rate limits. It allows monetization in case of external exposure. You must choose carefully what is added in it, as attaching this product will result in the Application (Dev app) having access to all the APIs that the product has bundled.

#### Client Application’s access to APIs:
To allow access to APIs, a product (API bundle) needs to be attached to the application id (Dev app) for that client app. We need to evolve this to handle scenarios of API exposure from multiple domains and decision/approval workflow for the same.

#### Areas being further refined:
API Product and Dev app governance:  Review current exposed APIs and consumers, create approval process and pipeline for on-boarding, maintenance and de-provisioning.
 


## API Security Considerations: Operations phase

1. Telemetry/Monitoring/Observability (quota, spike arrest, etc)
1. Define Dashboards and Alerts for specific monitoring and business use cases
1. Splunk Logs
1. API Reports
1. Security Audit – Sense, Shape
1. Certificate / Key management : rotation/renew/revoke -->

<!-- **Continuous Improvement - the cycle repeats, with any change to the API!** -->