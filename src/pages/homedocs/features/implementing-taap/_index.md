---
label: "feature"
title: "Implementing TAAP"
menuTitle: "Implementing TAAP"
image: "assets" 
link: "/sites/linked/securitydocs/taap"
draft: false
---

The T-Mobile Authentication and Authorization Process (TAAP) is designed to address several limitations and security issues with previous approaches of two-way SSL or OAuth 2.0 bearer tokens. 