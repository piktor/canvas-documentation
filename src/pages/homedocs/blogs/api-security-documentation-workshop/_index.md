---
label: "blog"    [comment]: # (this is classification tag for "blog/event/feature/upcomming")
title: "API Security"
menuTitle: "API Security"
image: "assets/blog.5f0a808f.png" 
link: "/securitydocs/api-security-intro"
draft: true
---

Secure your APIs with the T-Mobile Authentication and Authorization Process (TAAP). Introduction to Apigee and Microservice data security and guidance through IDP and token handling system.
