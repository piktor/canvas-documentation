---
eventPath: /events/apigee-cert-prep-sessions
title: Apigee Certification Prep Sessions
menuTitle: Apigee Certification Prep Sessions
location: Webex
date: 2019-04-29T14:00
eventTimeZone: PST
address: Webex, 2:00pm - 3:00pm PST
draft: true
event: true
---
###Apigee Ninja’s in Training:

You are a member of an elite group of T-Mobile employees who completed two or more Coursera API Developer courses and T-Mobile wants to inject a jolt of caffeine into our commitment to your career development. So, we’ve struck up a friendly competition with the Starbucks technology team to see which company can earn more Google Apigee API Engineer certifications by the end of May. 

“It’s a friendly competition,” said Komes Subramaniam, a Senior Manager in Software Development in the API Center for Enablement (API CoE).  “This is a valuable certification that isn’t easy to get, and we want to push ourselves against another company.”

The losing side will wear the winning company’s colors for a day, so let’s turn Starbucks from green to Magenta! 

To help you go the distance and become a Google certified Apigee API Engineer we’re letting you and your manager know that the API CoE will host a series of “Apigee Ninja 1-hour prep sessions” to help you prepare for the certification exam and set us up for success at beating our technology partners from Starbucks. 

Please join your colleagues for this series of API Certification Prep sessions. You can join in person in Canyon Point CPS 3A or via WebEx. We’ll record all sessions for replay.

<a href="https://t-mobile.webex.com/meet/aclamou" target="_blank">When it's time, join the session.</a>

For more information on this certification, take a look at the <a href="https://t-mo.slack.com/messages/CAGFNN2CQ/" target="_blank">Apigee Certification Training</a> Slack channel.

###Session Topics:

April 15 - 2 pm PT:	Prep Session 2 	Proxy development and configurations.

April 22 - 2 pm PT:	Prep Session 3 	Product access & developer apps.

April 29 - 2 pm PT:	Prep Session 4 	Shared flows.


