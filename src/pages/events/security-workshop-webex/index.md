---
eventPath: /events/security-workshop-webex
title: API Security Workshop
menuTitle: API Security Workshop
location: Webex
date: 2019-03-27T10:00
eventTimeZone: PST
address: Webex, 10:00am - 1:00pm PST
draft: false
event: false
---

<a href="https://t-mobile.webex.com/webappng/sites/t-mobile/meeting/info/123198415645445795?MTID=m339a99620f412253320bf13a1ab3ec24" target="_blank">When it's time, join the meeting.</a>

![API Security](./assets/workshop.jpg)

API Center for Enablement (ACE) is hosting several API Security Workshops including the introduction to TAAP integration.

API security has been audited heavily in the past few quarters with the intent to build and organize better solutions and dynamics with cross domain compliance and DevOps support including host providers and IDPs. The technical development of TAAP has been brought forth with extensive library development in order to help developers and supporting teams to develop creatively and freely without interference in development time with new methods in securing our clients.

ACE welcomes you to join in the workshops to learn and join in the security and methodology developed to improve experience and support of our customer’s data. This is a great time to share your experiences, knowledge, learn, and teach through an interactive experience.
