--- 
path: /API-Security-Workshop/Implement-the-API-proxy-with-TAAP-Security
title: 3.1 Implement-the-API-proxy-with-TAAP-Security
menuTitle: 3.1 Implement-the-API-proxy-with-TAAP-Security
draft: true
order: 1
---


## Implement the API proxy with TAAP Security 

In this section we will implement the TAAP security in the proxy.

#### Step 1: Create a new proxy

Perform the following steps

* Download the swagger file **[here](https://api-security-workshop-site.apps.px-npe01.cf.t-mobile.com/1.0_api_swagger)** into Postman.
* Update the basepath suffix with your NTID - Search the downloaded swagger file with keyword "basepath" and append your NTID after the version "v1" (Ex. "basePath": "/jwttoken-demo/v1/kkundur")
* Login to "groove-api" UI using your ntid (https://devcenter.t-mobile.com/createApi)
* In the Groove UI, drag-and-drop or Browse to choose the Swagger file which you downloaded in the previous step.
* Some of the fields will be auto populated from the swagger file, and you have to update the below mandatory fields.

   * "Proxy Description"- Add meaningful information about API Proxy.
   * "Description"(in Target Server section)- Add meaningful information about the Target.
   * "Target Server Name*" - Choose the target server "PCF_JWTTokenDemoService_Target" from the drop down.
   * "Target Server URI*" -  Update it to following - "jwttoken-demo/v1" 
   * "Support DL" - Add your team Distribution email or your own emailID.  
* After providing the required info, choose "Begin API Creation".
* The Groove UI starts a "Jenkins Job" to store your API files in a Bitbucket project.

#### Step 2: (SKIP this Step) Add the Dependency Clearwater API Specification Path to project.yaml.
   * Login in to the BitBucket - https://bitbucket.service.edp.t-mobile.com/projects/JZREP/repos/.
   * Search for your newley created repository with your NTID.
   * Update Project.yaml with the following info (add this content below the build: as a new entry) and commit the change - 
               
        clearwater:
            folder: ce.dsg.ace/demo
            swaggerName: api_security_workshop_demo_swagger.json
            
            
#### Step 3: Deploy the proxy

* Login to Jenkins using your ntid (https://jenkins.service.edp.t-mobile.com/job/JZREP/)
* Click on the "Scan Organization Folder Now" option to sync the bitbucket source code with jenkins pipeline. After few minutes it will show your proxy in the list
* Search and navigate to your proxy > master > Build Now. (e.g. jwtdemo-proxy > master > Build Now)
* (Skip this step) Once build is complete,Approve(manual) the deployment to sb02 environment. 
* This process will deploy the proxy into tmobilea (saas) Organization   

#### Step 4: Publish APIs Proxy

* Register your proxy to JWT-Training product in tmobilea organization.
* Click the Publish tab, then Products.
* Search the product name with "JWT-Training" and click the name of the API product.
* Click Edit on the API product page.
* Click "+API Proxy" button, it will show one drop down list and select your proxy from the drop down list
* Click Save  

#### Step 5: Test the rest endpoints

Download and import the following postman collection(s) to Postman tool. These postman collection(s) are used throughout this workshop.

> **Import the apigee postman collection from [here](https://api-security-workshop-site.apps.px-npe01.cf.t-mobile.com/4.0_postman_collections) into Postman (if it's not already imported in previous** section).

Generate the new JWT access token for all below test cases using `TAAP - Generate Token` postman request.

#### Test case 1: Test "Create Order" rest endpoint

Update the downloaded Postman collection to match your proxy basepath. e.g - https://tmobilea-sb02.apigee.net/jwttoken-demo/v1/kkundur/orders
Execute `TAAP - Create Order` postman request and verify that it returns 201 HTTP status code with TAAP JWT access token in request headers.

#### Test case 2: Test "Get Order" rest endpoint

Update the downloaded Postman collection to match your proxy basepath. e.g - https://tmobilea-sb02.apigee.net/jwttoken-demo/v1/kkundur/orders/Order-1000
Execute `TAAP - Get Order` postman request and verify that it returns 200 HTTP status code with TAAP JWT access token in request headers.

#### Test case 3: Test "Get Orders By Account Number" rest endpoint

Update the downloaded Postman collection to match your proxy basepath. e.g - https://tmobilea-sb02.apigee.net/jwttoken-demo/v1/kkundur/orders?account-number=1111111111
Execute `TAAP - Get Orders By Account Number` postman request and verify that it returns 200 HTTP status code with TAAP JWT access token in request headers.

