---
path: /API-Security-Workshop/IMPLEMENT-TAAP-SECURITY-FOR-APIGEE
title: 3.0 IMPLEMENT-TAAP-SECURITY-FOR-APIGEE
menuTitle: 3.0 IMPLEMENT-TAAP-SECURITY-FOR-APIGEE
draft: true
order: 3
---

## Implement TAAP security for APIGEE

In order to implement security for the Apigee proxy, we will perform the following activities.

- 3.1 Implement the Apigee proxy with TAAP Security
- 3.2 Testing the TAAP Security Behavior
   
## Prerequisites for Apigee Workshop:

- Postman is installed
- Apigee access to tmobilea organization
- Groove UI
- Access to EDP Jenkins
