---
path: /API-Security-Workshop/Testing-the-TAAP-Security-Behavior
title: 3.2 Testing-the-TAAP-Security-Behavior
menuTitle: 3.2 Testing-the-TAAP-Security-Behavior
draft: true
order: 2
---


## Testing the TAAP Security Behavior

### Step 1: Import Postman Collection

Download and import the following postman collection(s) to Postman tool. These postman collection(s) are used throughout this workshop.

> **Import the apigee postman collection from [here](https://api-security-workshop-site.apps.px-npe01.cf.t-mobile.com/4.0_postman_collections) into Postman (if it's not already imported in previous section).**
