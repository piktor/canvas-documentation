---
path: /API-Security-Workshop/Testing-Access-Token-&-ID-Token-Security
title: 3.2.2 Testing Access Token & ID Token Security
menuTitle: 3.2.2 Testing Access Token & ID Token Security
draft: true
order: 2
---

## 3.2.2 Testing Access Token & ID Token Security

### Step 1: Testing the Apigee Security

Execute the following test cases using Postman.

#### Test case 1: Use valid JWT tokens for both access token (Authorization header) and ID token (X-Auth-Originator header)

* Generate the new Apigee access token using `TAAP - Generate Token` postman request.
* Generate the new IAM access token and ID token using following steps:
  * Execute postman request `TAAP Generate IAM Authorization Code (Step 1)` copy the code from response.
  * Modify the body of `TAAP Generate IAM Token (Step 2)` postman request to set the code received from step above.
  * Execute the `TAAP Generate IAM Token (Step 2)` postman request and get the access token and ID token from response.
* Execute the `TAAP - Create Order - APIGEE IAM` postman request with the newly generated Apigee access token and IAM ID token.
* Verify that the API returns 201 HTTP status code.

#### Additional test cases to verify TAAP security behavior:

* Pass expired access token and verify that it returns HTTP status code 401
* Pass expired ID token and verify that it returns HTTP status code 401
* Pass invalid JWT for access token and verify that it returns HTTP status code 401
* Pass invalid JWT for ID token and verify that it returns HTTP status code 401
