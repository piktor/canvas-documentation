---
path: /API-Security-Workshop/Testing-PoP-Token-Security
title: 3.2.1 Testing-PoP-Token-Security
menuTitle: 3.2.1 Testing-PoP-Token-Security
draft: true
order: 1
---


## Testing PoP Token Security

### Step 1: Testing the PoP token validation

#### Step 1.1: Get the new access token with PoP token in the Request

Follow the following steps to get the new access token:

* Execute `PoP token test cases\TAAP - POP GENERATOR - GET METHOD` postman, the response will contain Pop token.
* Execute `PoP token test cases\TAAP - Generate Token (with PoP token)` by setting PoP token fetched in above step as "X-Authorization" header in the request, the response will contain the access token and ID token. 

#### Step 1.2: Invoke 'Create Order' REST endpoint with access token, ID token and PoP token

* Execute `TAAP - POP GENERATOR - POST METHOD` postman request, the response will contains the PoP token.
* Execute `TAAP- Create Order (with PoP token)` postman request by setting the PoP token value fetched in above step as "X-Authorization" header in the request header.This should Create an Order by validating the PoP Token.
