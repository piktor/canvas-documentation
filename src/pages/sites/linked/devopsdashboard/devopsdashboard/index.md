---
path: /devopsdashboard/intro
title: 1.0 Self Service DevOps Dashboard
menuTitle: 1.0 Self Service DevOps Dashboard
draft: true
order: 1
---


#Self Service DevOps Dashboard

Brief Overview of Self Service DevOps Dashboards

There are two separate Self Service DevOps dashboards. One dashboard is
for engineers and thus offers metrics, among other details, in an
extremely drilled down manner. These dashboards are as follows:

-   Team Dashboard

-   Product Dashboard

The other dashboard is intended for executives who, if they so choose,
can either read at a higher level or can opt to also dig into additional
details.

Sign Ups and Logins
-------------------

DevOps dashboards provides two types of users.

1.  Admin User

2.  Other Users

About Admin Users
-----------------

DevOps dashboards provide administrator and user access through various
views. An admin user can:

-   Select a theme for the dashboard

-   Manage user and admin accounts for the dashboard

-   Set up API tokens for authentication

-   Create and manage custom dashboard templates

About Other Users
-----------------

User Signup and Login Instructions
----------------------------------

To create an account for a new user:

1.  Click **Signup** in the login page to create a user account.

2.  Enter a username, password, confirm the password, and then
    click **Signup**.

If you already have your login credentials, enter the username and
password, and then click **Login**.

**Note:** If SSO authentication is enabled, then on successful
authentication, you are automatically logged-in to DevOps Dashboard.

A Note on User Authentication
-----------------------------

DevOps dashboards provide the following options for user authentication:

-   LDAP allows you to use your LDAP server to authenticate users. To
    enable SSO (Single Sign On) authentication, you must configure
    DevOps Dashboard with your LDAP server to authenticate users.

-   Standard authentication uses an internal database of users and
    passwords. If you choose to create an internal database, the user
    names and passwords will not be in sync with the LDAP server.

To modify the user authentication type for the dashboard, see the <a href="http://hygieia.github.io/Hygieia/api.html#api-properties-file" target="_blank">API Properties</a> file.

Before you login to the DevOps dashboard, choose a login type:

Standard Login

LDAP Login

If **Single Sign On (SSO)** authentication is enabled, then the user is
automatically authenticated and logged-in to DevOps Dashboard.

Select Your Dashboard
---------------------

Once you log in to DevOps, you can view the existing dashboards
categorized as follows:

-   All dashboards tab

-   Team dashboard tab

-   Product dashboard tab

The list of all dashboards you create in DevOps will appear in the **My
Dashboards** in the screen.

#### Search Dashboards

To find a dashboard, filter the list by entering all or part of the
dashboard name in the **Search** field.

#### All Dashboards Tab

In this tab, you can view a list of all the dashboards created by all
dashboard users.

#### Team Dashboards Tab

This tab displays a list of all product dashboards created in DevOps.

To create a new team dashboard, click **Create a new dashboard**.

#### Product Dashboards Tab

This tab displays all the products for a team.

To create a new product dashboard, click **Create a new dashboard.**

![](assets/image1.png)

![](assets/image2.png)

The Team Dashboard - Introduction
---------------------------------

The DevOps Team Dashboard offers a single, streamlined view of the
DevOps Pipeline. It tells a clean story in visual form of all the
complex parts that makeup the CI/CD pipeline. The DevOps Team Dashboard
offers a comprehensive summary with the following views:

-   The Widget View

-   The Pipeline View

-   The Cloud View

Create a Team Dashboard
-----------------------

To create a new team dashboard:

1.  In the **Team Dashboards** tab, click **Create a new dashboard**.
    This invokes the **Create a New Dashboard** screen.

![](assets/image3.png)

1.  Enter the following details:

    -   Select the dashboard type as Team Dashboard from the dropdown
        list.

    -   Choose Select Widgets radio button to customize the dashboard
        layout while creating a new dashboard.

    -   Choose Select Template radio button to select a dashboard
        template from the dropdown list. For the team dashboard, select
        one of the following options:

        -   Cap One

        -   Cap One ChatOps

        -   Cloud Dashboard

        -   Split View

        -   Any custom template created by the dashboard administrator

    <!-- -->

    -   Enter the dashboard title. The title should have a minimum of 6
        characters and should not contain any special characters (for
        example, !, \#, &, \_).

    -   Enter a business service name. This is an optional value.

    -   Enter a Business Application Type to support the business
        function. This is an optional value.

    -   Enable Score -- Check this box to indicate that you want to
        enable the scoring feature for an existing team dashboard.

> Select one of the following radio-buttons to choose how the score
> appears on the dashboard:

-   Display in Header -- Choose this option to display the overall
    dashboard score at the top of the team dashboard. Click on the star
    rating to view the score details.

-   Display in Widget -- Choose this option to display the overall score
    as a widget on the dashboard. Click View Details on the widget to
    see the score details.

1.  Click Create.

If you have chosen Select Widgets radio button, then this option invokes
the Widget Management screen.

![Image](assets/image4.png)

In this screen, choose widgets from the list of widgets, and then
click **Create**. The team dashboard is created.

If you have chosen **Select Template** radio button, then the team
dashboard is created based on the template you have selected.

Widget View
-----------

The widget view enables you to configure widgets with various DevOps
tools that are used to manage your CI/CD pipeline. The widgets are
highly flexible, meaning you can choose to configure the necessary
widgets to view your DevOps pipeline on the DevOps dashboard. These
widgets are integrated with Collectors to showcase information,
collected from the CI/CD tools, on the DevOps dashboard.

As the following screenshot shows, the widget view enables you to
configure and visualize the data of your DevOps pipeline:

![](assets/image5.png)

Configure Widgets - Common Procedure
------------------------------------

To configure a widget in the dashboard:

1.  Click **Configure Widget** under the widget name for which you want
    to configure your DevOps Dashboard.

> The **Configure \[Widget Name\] Widget** dialog box is invoked.

2.  In the **Configure \[Widget Name\] Widget** screen, enter
    configuration values for the fields, and then click **Save**.

> The Dashboard widget displays values based on your configuration.

To change widget settings:

-   Click the Settings icon, make any configuration changes, and then
    click **Save**.

> The widget displays details based on the new configuration.

To clear a configured widget:

-   Click the Delete icon beside the template name. System prompts a
    message to confirm or cancel deletion. Click Delete to confirm
    deletion. The widget configuration is cleared.

Pipeline View
-------------

The pipeline view showcases the code progression through each
component's lifecycle in the software delivery process, from initial
development to the final, product deployment.

As the following screenshot shows, the screen offers immediate analysis
of code progression:

![](assets/image6.png)

In addition, you can evaluate the code progression through its various
stages through the delivery pipeline. As shown above, there is a list of
executable packages with the corresponding version deployed at each
stage in the delivery pipeline. Finally, the date under the version
number indicates the package deployment date.

**Configure the Pipeline View**
-------------------------------

To configure your delivery pipeline through the pipeline stages:

1.  In the Pipeline tab of your team dashboard, click **Configure
    widget**. This invokes the **Configure Delivery Pipeline** pop-up
    window.

2.  In the **Configure Delivery Pipeline** pop-up window, enter the
    following details:

    -   Click Add to enter the pipeline stage and then select the
        environment.

    -   Once you have selected an environment for each for the pipeline
        stage you want to configure, choose the radio button to indicate
        the production stage, and then click **Save**.

The Pipeline View is displayed for the configured environments.

The following screenshot shows the **Configure Delivery
Pipeline** pop-up window:

![](assets/image7.png)

The Product Dashboard -- Introduction
-------------------------------------

The Product dashboard displays multiple applications in a single
illustration. Here, you see the progression of an application from its
inception (commit) to its final release (prod) for multiple teams.

As the following screenshot shows, you can view how the code is
progressing for multiple applications:

![](assets/image8.png)

**Create a Product Dashboard**
------------------------------

To configure the product dashboard:

To create a product dashboard:

1.  Click Create a new dashboard. The Create a New Dashboard screen is
    invoked.

2.  Enter the following details:

    -   Select the dashboard type as Product Dashboard from the dropdown
        list.

    -   Enter the dashboard title. The title should have a minimum of 6
        characters and should not contain any special characters (for
        example, !, \#, &, \_).

    -   Enter the Application Name.

    -   Enter a business service name. This is an optional value.

<!-- -->

1.  Click Create.

**Prerequisites**:
------------------

Before creating a product dashboard, ensure that the following
prerequisites are met:

1.  A Team Dashboard is created

2.  **Build** and **Code Repo** widgets are configured on the team
    dashboard

The process to create a Team Dashboard and configure the widgets is
explained in the following sections.

**Note**: The metrics displayed on the product dashboard are based on
the configurations in the team dashboard. If the team dashboard is not
configured for a metric, it is not displayed on the product dashboard.

### **Configure the Product Dashboard**

To configure a Product Dashboard:

-   In the product dashboard screen, click **Add a Team**. Enter the
    following details in the **Add team** pop-up window:

    -   Select a team dashboard from the drop-down list. The list
        displays all team dashboards created in DevOps.

    -   Optionally, specify a custom team name, and then click **Add
        this team**.

**Note**: Configure the Build and Code Repository before adding the team
to the Product Dashboard.

### Information on the Product Dashboard

The product dashboard displays averages and trends calculated over a
90-day period.

-   **DevOps Maturity Score** -- Quantitative score that measures the
    application's DevOps maturity level.

-   **90-day Pipeline Health** - This segment displays trends to enable
    easy access to important DevOps metrics over a 90-day period. The
    various trends displayed include the number of unit and functional
    tests passed as well as code coverage per day. These metrics also
    enable you to measure code and security issues, build success, and
    time taken to fix builds.

At each of the different development stages, the Product view shows the
number of commits waiting to move to the next stage, thus amplifying the
wait-times between stages. Thus, the Product View enables teams to
reduce the wait-time and speed up the continuous delivery pipeline.

The product dashboard gives in-depth details by highlighting the
standard code deviations at each stage:

-   The green boxes at a development stage indicate the number of
    commits that do not exceed 2 standard deviations.

-   The red boxes indicate that have commits have exceeded over 2
    standard deviations.

**Screenshots:**

The product dashboard displays averages and trends calculated over a
90-day period. For details more details on the calculations displayed on
the Product Dashboard, see the following screenshots:

Commit Stage:

![Image](./assets/image9.png)

Build Stage:

![Image](./assets/image10.png)

Commit Details at every stage:

![Image](./assets/image11.png)

Deployment Stage:

![Image](./assets/image12.png)

Production Stage:

![Image](./assets/image13.png)

Pipeline Health Details:

![Image](./assets/image14.png)
