---
stagename: "manage"
level: 1
refid: 15
devJourney: false
draft: false
---

## Summary:  
Now that your Apigee Proxy is up and running, you should monitor it to maintain your tools.

<br/>

## Process:
Monitor Apigee Proxy