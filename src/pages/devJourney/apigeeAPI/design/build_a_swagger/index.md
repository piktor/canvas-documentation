---
processTitle: "Build a Swagger"
processOrder: 1
refId: 112
stage: "Design"
level: 2
devJourney: true
draft: true
modal: false
---

## Step 2: Build a Swagger Using Recite

[[external | Reference:]]
| [https://tmobileusa.sharepoint.com/teams/ServiceDesignOffice/RECITE%20Wiki/Home.aspx](https://tmobileusa.sharepoint.com/teams/ServiceDesignOffice/RECITE%20Wiki/Home.aspx)


1. [[external | Download.]]
    | [![](../../assets/icon-external-link.png) Website](https://tmobileusa.sharepoint.com/teams/ServiceDesignOffice/Shared%20Documents/Recite/Install)

2. [[external | Install and configure.]]
    | [![](../../assets/icon-external-link.png) Website](https://tmobileusa.sharepoint.com/teams/ServiceDesignOffice/RECITE%20Wiki/Installation%20and%20setup.aspx)

3. [[external | How to use.]]
    | [![](../../assets/icon-external-link.png) Video](https://tmobileusa.sharepoint.com/teams/ServiceDesignOffice/RECITE%20Wiki/A%205%20Minute%20Demo.aspx)

4. [[external | Create a simple Swagger file using an Object from a Model.]]
    | [![](../../assets/icon-external-link.png) Video](https://tmobileusa.sharepoint.com/teams/ServiceDesignOffice/RECITE%20Wiki/Creating%20a%20simple%20Swagger%20Document%20Using%20an%20Object%20From%20a%20Model.aspx)


[[notes | NOTES]]
| Recite is only Windows compatible. Make sure you click the 'Check Your Swagger' option to generate your Swagger.