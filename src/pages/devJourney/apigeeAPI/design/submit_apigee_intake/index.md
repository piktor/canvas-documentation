---
processTitle: "Submit Apigee Intake"
processOrder: 4
refId: 115
stage: "Design"
level: 2
devJourney: true
draft: true
modal: false
---

## Step 5: Submit Apigee Intake


1. [[external | Create an intake form by selecting \"Create\" button (at top blue color bar in the header), type \"Apigee\" in the search field and select “Apigee Infrastructure/Environment Intake” template.]]
    | [![](../../assets/icon-external-link.png) Website](https://qwiki.internal.t-mobile.com/display/CSDCM/Apigee+Environment+Intake)
2. Make sure you submit everything.
3. Save the form.
4. Contact Apigee CM <ApigeeCM@T-Mobile.com> with your intake link to get it reviewed. [![](../../assets/icon-email.png) Email](mailto:ApigeeCM@T-Mobile.com)


[[notes | NOTES]]
| Apigee intake needs to be reviewed by the ApigeeCM team so they can understand the project requirements and suggest which Apigee platform (on-prem/saas) and which environments (core or experience) need to be used for that project. There are also other details like backend system and consumer (devapp) which need to be reviewed by Apigee CM.