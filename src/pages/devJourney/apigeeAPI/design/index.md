---
stageName: "design"
level: 1
refid: 11
devJourney: false
draft: false
---

## Summary:  
Designing your API in Apigee is crucial for the ApigeeCM@T-Mobile.com team to review and allow access. Please use the following steps to design your API correctly in order to save time and have a quick production.  

<br/>

## Process:
Review Guidelines -> Build Your Swagger -> Submit to Clear Water -> Submit an Intake Form -> Review the Guidelines