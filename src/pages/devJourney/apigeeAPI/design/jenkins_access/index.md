---
processTitle: "Jenkins Access"
processOrder: 3
refId: 114
stage: "Design"
level: 2
devJourney: true
draft: true
modal: false
---

## Step 4: Jenkins Access


1. Submit a request in the #edp-support Slack channel for EDP Jenkins access. You will need it to view your proxy build status in later stages.