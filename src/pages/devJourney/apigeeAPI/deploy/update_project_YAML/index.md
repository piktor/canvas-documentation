---
processTitle: "Update Project YAML"
processOrder: 8
refId: 132
stage: "Deploy"
level: 2
devJourney: true
draft: true
modal: true
---

## Step 2: Update the Project YAML File in the BitBucket Apigee Repository

[[iLink | Reference:]]
| [/static/Apigee-EDP-Jenkins-Sample-Files/Apigee-EDP-Jenkins-Sample-Files/](/sites/static/apigee-journey/Deploy/Step2-Update_the_Project_YAML_File/Apigee-EDP-Jenkins-Sample-Files-CSD-CM-Qwiki)


1. [[external | Clone your Apigee proxy repository to your local machine and update the project.yaml file with the environment entries where you want to deploy your proxy to and commit the changes. ]]
    | [![](../../assets/icon-external-link.png) Video](https://tmobileusa.sharepoint.com/teams/ServiceDesignOffice/RECITE%20Wiki/A%205%20Minute%20Demo.aspx)

2. Update the environment details in the project.yaml file with the test environment details (org, env, host, Type). You can login to Apigee UI console and get the environment details.


[[notes | NOTES]]
| Test environment entries need to be updated in the project.yaml file for the Apigee bitbucket proxy repository by the developer. Once committed, the jenkins pipeline build will kickoff and deploy the proxy to the environments listed in project.yaml file. You can refer to the sample files in the qwiki page link. You can also login to the Apigee org and get the environment details.