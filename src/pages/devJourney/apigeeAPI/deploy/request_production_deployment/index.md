---
processTitle: "Request Production Deployment"
processOrder: 11
refId: 135
stage: "Deploy"
level: 2
devJourney: true
draft: true
modal: false
---

## Step 5: Request Production Deployment


1. For a major release, provide your change details (any/all of the below items) at least by 3rd week of a major release date. (If major release is on March 30th, provide changes at least by March 20th-21st)
2. Contact Apigee Production Admins with the following information in your email: Proxy name, Proxy Version, Test environment name where your proxy has been tested along with test results, Clarity project name and PR# along with RCM# which is committed for Apigee for a given release, any config changes (Products, devapps, kvm’s, caches, sharedflows etc), production target server url’s for the proxy confirmation on whether API Inventory has been updated. [![](../../assets/icon-email.png) Email](mailto:Apigeeproductionadmins@T-Mobile.com)
3. For an off release, Domain teams need to provide all the below changes 48 hrs in advance before the production release date.
4. Contact Apigee Production Admins with the following information in your email: Proxy name, Proxy Version, test environment name where proxy has been tested along with test results, any config changes (Products, devapps, kvm’s, caches, sharedflows etc), and production target server url’s for the proxy. [![](../../assets/icon-email.png) Email](mailto:Apigeeproductionadmins@T-Mobile.com)


[[notes | NOTES]]
| Please follow the format based upon if you are doing a major release or an off release.