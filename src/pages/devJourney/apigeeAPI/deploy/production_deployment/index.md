---
processTitle: "Production Deployment"
processOrder: 14
refId: 138
stage: "Deploy"
level: 2
devJourney: true
draft: true
modal: false
---

## Step 8: Production Deployment


1. Contact Apigee Production team if you don't see your deployment by the scheduled date. [![](../../assets/icon-email.png) Email](mailto:Apigeeproductionadmins@T-Mobile.com)


[[notes | NOTES]]
| Apigee Production Admins <Apigeeproductionadmins@T-Mobile.com> will be deploying changes to Apigee production env's based on the changes mentioned in Apigee release notes.