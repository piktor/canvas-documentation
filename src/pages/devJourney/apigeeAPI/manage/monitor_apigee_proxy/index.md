---
processTitle: "Monitor Apigee Proxy"
processOrder: 17
refId: 151
stage: "Manage"
level: 2
devJourney: true
draft: true
modal: false
---

## Step 1: Monitor Apigee Proxy


1. Contact Apigee CM for custom reports. [![](../../assets/icon-email.png) Email](mailto:ApigeeCM@T-Mobile.com)


[[notes | NOTES]]
| Domain teams can reach out to Apigee CM <ApigeeCM@T-Mobile.com> for NPE and Apigee Production Admins for Production if they want to have any custom reports created for monitoring proxy performance, errors etc..