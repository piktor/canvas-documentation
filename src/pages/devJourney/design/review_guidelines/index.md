---
processTitle: "Review Guidelines"
processOrder: 0
refId: 111
stage: "Design"
level: 2
devJourney: true
draft: true
modal: false
---

## Step 1: Review Guidelines


1. [[external | Review these API design standards to make sure your Apigee API is designed as per the defined standards.]]
    | [![](../../assets/icon-external-link.png) Website](http://tm/apis)
