---
processTitle: "Apigee UI Access"
processOrder: 5
refId: 116
stage: "Design"
level: 2
devJourney: true
draft: true
modal: false
---

## Step 6: Apigee UI Access


1. An Apigee user creation bot is available in the #apigee Slack channel with the wake word “/apigee access <email_id>” (without the quotes and actual T-Mobile email id). You can request access for any existing or new team members joining the team using the bot. Default role choices are ‘debugger’ with trace access to debug API issues and ‘user’ which is only provided on the development environments. Please Let us know in case you have any further queries on the same.
2. [[external | If you haven’t signed up yet in [https://enterprise.apigee.com](https://enterprise.apigee.com) , please do this onetime signup before using with Slack process. If you have any issues with the Slack bot, you can always reach out to Apigee CM for help with access. If you not sure about which Apigee organization you need to get access for, contact Apigee CM <ApigeeCM@T-Mobile.com>. Usually, the organization information for which you need to get access is provided as part of the intake review.]]
    | [![](../../assets/icon-external-link.png) Website](https://groove-api.apps.px-prd04.cf.t-mobile.com/login)


[[notes | NOTES]]
| Get the Apigee UI access to view your proxy deployment status, environment details, product mapping, and proxy trace.