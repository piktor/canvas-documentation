---
processTitle: "Deposit to Clearwater"
processOrder: 2
refId: 113
stage: "Design"
level: 2
devJourney: true
draft: true
modal: true
---

## Step 3: Deposit to Clearwater

[[external | Reference:]]
| [https://tmobileusa.sharepoint.com/sites/eit/clearwater-resource-page](https://tmobileusa.sharepoint.com/sites/eit/clearwater-resource-page)

1. [[iLink | Get Clearwater repo project access]]
   | [![](../../assets/icon-external-link.png) Website](/sites/static/apigee-journey/Design/Step3-Deposit_to_Clearwater/Accessing-the-Clearwater-Bitbucket-repo)

2. [[external | Create your Clearwater repository in EITCODEDOC project.]]
    | [![](../../assets/icon-external-link.png) Website](https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse)

3. Checkin/Commit your Swagger file to Clearwater.

[[notes | NOTES]]
| For questions related to Clearwater or making your APIs Clearwater compliant, please reach out to Clearwater2@T-Mobile.com or ClearwaterDevOps@T-Mobile.com.
