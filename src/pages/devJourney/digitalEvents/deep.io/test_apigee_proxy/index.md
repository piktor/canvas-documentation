---
processTitle: "Test Apigee Proxy"
processOrder: 10
refId: 243
stage: "Deep.io"
level: 2
devJourney: true
draft: true
modal: false
---

## Step 2: Test Apigee Proxy


1. Using the access token received by calling the oauth url’s, dev/consumer need to test/validate their Apigee proxy.