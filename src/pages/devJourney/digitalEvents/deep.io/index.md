---
stageName: "Deep.io"
level: 1
refid: 24
devJourney: false
draft: false
---

## Summary:  
Summary: Once, Apigee proxy deployment is done, below steps need to be followed to test the proxy.

<br/>

## Process:
General Access Token -> Test Apigee Proxy