---
stageName: "Test2"
level: 1
refid: 25
devJourney: false
draft: false
---

## Summary:  
Now that your Apigee Proxy is up and running, you should monitor it to maintain your tools.

<br/>

## Process:
Monitor Apigee Proxy