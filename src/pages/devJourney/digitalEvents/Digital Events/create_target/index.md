---
processTitle: "Digital Events Test 0"
processOrder: 1
refId: 231
stage: "Digital Events"
level: 2
devJourney: true
draft: true
modal: true
---

## Step 1: Digital Events Test

1. [[iLink | Complete the 4 steps to configure your target servers, KVM’s or Caches at the environment level here.]]
   | [![](../../assets/icon-external-link.png) Website](</sites/static/apigee-journey/Deploy/Step1-Create_Target_Servers_KVMs_Caches/Apigee-API-Proxy-TargetServer(s)-Configuration-CSD-CM-Qwiki>)
2. Contact Apigee CM <ApigeeCM@T-Mobile.com> with your pull request link to get it completed. [![](../../assets/icon-email.png) Email](mailto:ApigeeCM@T-Mobile.com)
3. Repeat Step 1 and 2 whenever you have a pull request for target Server, KVM, and or Cache.

[[notes | NOTES]]
| You need to update the targetServer.json file of the respective test environment with the target server details used by the Apigee proxy. You then need to create a pull request which will be reviewed, approved and merged by Apigee CM Team. The same process needs to be followed to update KVM's or Caches for the test environments.
