---
processTitle: "Jenkins Build Steps"
processOrder: 3
refId: 233
stage: "Digital Events"
level: 2
devJourney: true
draft: true
modal: true
---

## Step 3: Update the Project YAML File in the BitBucket Apigee Repository

1. If you do not see your Jenkins build for Apigee repository, click on Scan Organization Folder Now and wait for the Scan to finish. You can check the status of the scan here: Scan Organization Folder Log. You should be able to see Jenkins build after the Scan is completed.
2. [[iLink | Depending on the value you provided for ‘Approval’ tag (more details here) in your Project.yaml file, you might need to Approve the deployment to test environment in the Jenkins log (Go to your jenkins build and click on the build number to view the console log). Make sure your Jenkins build is successful. Contact ApigeeCM if build fails and you are not sure about the issue. ]]
   | [![](../../assets/icon-external-link.png) Website](/sites/static/apigee/Apigee-EDP-Environments-and-Deploy-Approvals-Process-CM)
3. Login to Apigee UI console and confirm Apigee proxy is deployed in the Environments that are listed in project.yaml file for your Apigee repository in edp-bitbucket.
