---
processTitle: "Release Notes Preparation"
processOrder: 7
refId: 237
stage: "Digital Events"
level: 2
devJourney: true
draft: true
modal: false
---

## Step 7: Release Notes Preparation


1. Apigee CM will prepare production release notes for Major/Minor releases based on the changes received from domain teams.