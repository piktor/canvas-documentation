---
stageName: "Digital Events"
level: 1
refid: 23
devJourney: false
draft: false
---

## Summary:  
Follow below steps to have your Apigee proxy deployed to the test environments and how to request for Apigee production deployment.

<br/>

## Process:
Create Target Servers, KVM’s, Caches -> Update Project YAML in BitBucket Apigee Repository -> Contact ApigeeCM@T-Mobile.com -> Steps For Request -> API Inventory Update -> Release Notes Prep -> Production Deployment