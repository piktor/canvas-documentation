---
processTitle: "Build Apigee Proxy"
processOrder: 0
refId: 121
stage: "Develop"
level: 2
devJourney: true
draft: true
modal: true
---

## Step 1: Build Apigee Proxy

1. [[iLink | Read the instructions for using the Apigee Proxy Generator tool in order to create your Apigee proxy.]]
   | [![](../../assets/icon-external-link.png) Website](/sites/static/apigee-journey/Develop/Step1-Build_Apigee_Proxy/SelfService-Proxy-Generation-CSD-CM-Qwiki)
2. [[external | Use the Apigee Proxy Generator tool in order to build your Apigee API Proxy.]]
    | [![](../../assets/icon-external-link.png) Website](https://proxygen.apps.px-prd04.cf.t-mobile.com)
