---
processTitle: "Generate Access Token"
processOrder: 0
refId: 142
stage: "Test"
level: 2
devJourney: true
draft: true
modal: false
---

## Step 1: Generate Access Token


1. OAuth url = https://<Apigeehosturl>/v1/oauth2/accesstoken?grant_type=client_credentials.
2. [[external | Replace <Apigeehosturl> with apigee environment url in above urls. Ex for dlab02-core env is in the reference.]]
    | [![](../../assets/icon-external-link.png) Website](https://dlab02.core.op.api.t-mobile.com/v1/oauth2/accesstoken?grant_type=client_credentials)


[[notes | NOTES]]
| You need to call Apigee authentication proxy(oauth or OAUTH2_TOKEN if using JWT) to get access token. Using the access token, you can access your apigee proxy. Below are the links for authentication proxies in Apigee.