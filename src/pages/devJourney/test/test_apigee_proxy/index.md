---
processTitle: "Test Apigee Proxy"
processOrder: 1
refId: 143
stage: "Test"
level: 2
devJourney: true
draft: true
modal: false
---

## Step 2: Test Apigee Proxy


1. Using the access token received by calling the oauth url’s, dev/consumer need to test/validate their Apigee proxy.