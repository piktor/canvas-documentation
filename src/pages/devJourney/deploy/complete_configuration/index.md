---
processTitle: "Complete Configuration"
processOrder: 3
refId: 134
stage: "Deploy"
level: 2
devJourney: true
draft: true
modal: false
---

## Step 4: Contact Apigee CM to Complete Configuration (Product Mapping, Devapps, KVM’s, etc)


1. Once the Apigee proxy deployment is verified from Apigee UI, please reach out to Apigee CM to do the following steps. [![](../../assets/icon-email.png) Email](mailto:ApigeeCM@T-Mobile.com)
2. Create a new Apigee product and add the required proxies to it or add the proxy to an existing Apigee product.
3. Create a new Apigee devapp and add required products to it or add a product to an existing devapp.


[[notes | NOTES]]
| Once the proxy is deployed to the test environment, you need to contact Apigee CM to get help with mapping your proxy to an Apigee product and grant access to a devapp or any other org level configuration changes.