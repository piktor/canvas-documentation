---
processTitle: "Release Notes Preparation"
processOrder: 6
refId: 137
stage: "Deploy"
level: 2
devJourney: true
draft: true
modal: false
---

## Step 7: Release Notes Preparation


1. Apigee CM will prepare production release notes for Major/Minor releases based on the changes received from domain teams.