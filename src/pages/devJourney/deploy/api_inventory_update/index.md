---
processTitle: "API Inventory Update"
processOrder: 5
refId: 136
stage: "Deploy"
level: 2
devJourney: true
draft: true
modal: true
---

## Step 6: API Inventory Update

[[iLink | Reference:]]
| [/sites/static/apigee-journey/Deploy/Step6-API_Inventory_Update/API-Security-Inventory-collection](/sites/static/apigee-journey/Deploy/Step6-API_Inventory_Update/API-Security-Inventory-collection)


1. [[external | Use the sample format as your template.]]
    |[![](../../assets/icon-external-link.png) Website](https://tmobileusa.sharepoint.com/:x:/r/sites/APIAudit/_layouts/15/Doc.aspx?sourcedoc=%7BD4F608E5-468B-4AC4-AE22-7C05A55FEF99%7D&file=Sample%20Mandatory%20API%20Inventory%20Document.xlsx&action=default&mobileredirect=true)

2. [[external | Copy your file to]]
    |[![](../../assets/icon-external-link.png) Website](https://tmobileusa.sharepoint.com/sites/APIAudit/Shared%20Documents/Forms/AllItems.aspx?newTargetListUrl=/sites/APIAudit/Shared%20Documents&viewpath=/sites/APIAudit/Shared%20Documents/Forms/AllItems.aspx&id=/sites/APIAudit/Shared%20Documents/API%20Input%20Datasheets)

3. [[external | Request your domain auditor/security champion to audit and copy to this folder]]
    |[![](../../assets/icon-external-link.png) Website](https://tmobileusa.sharepoint.com/sites/APIAudit/Shared%20Documents/Forms/AllItems.aspx?newTargetListUrl=/sites/APIAudit/Shared%20Documents&viewpath=/sites/APIAudit/Shared%20Documents/Forms/AllItems.aspx&id=/sites/APIAudit/Shared%20Documents/ReadyForUpload-AuditorOnly)

4. [[external | An auditor can leverage Import functionality in the link to upload the content.]]
    |[![](../../assets/icon-external-link.png) Website](https://tmobileusa.sharepoint.com/teams/ServiceDesignOffice/RECITE%20Wiki/A%205%20Minute%20Demo.aspx)


[[notes | NOTES]]
| Domain team need to make sure to update API Inventory for their Apigee proxies if not done already. ApigeeCM will check API Inventory for any Apigee proxy that need to go to production.