import { createStore as reduxCreateStore } from "redux"

const reducer = (state, action) => {
  if (action.type === `NAVBARIMAGE`) {
    return Object.assign({}, state, {
      storedActiveNavImage: action.data
    })
  }
  if (action.type === `PATH`) {
    return Object.assign({}, state, {
      rootPath: action.data
    })
  }

  if (action.type === `TOOLS`) {
    return Object.assign({}, state, {
      toolsData: action.data
    })
  }
  return state
}

const initialState = { storedActiveNavImage : [], rootPath : "", toolsData:[] }

const store = reduxCreateStore(reducer, initialState)
export default store
